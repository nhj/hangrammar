/**
 * Created by zenchyon on 2017-04-14.
 */
window.addEventListener("message", function(event) {
    // We only accept messages from ourselves
    if (event.source != window)
        return;

    if (event.data.type && (event.data.type == "FROM_PAGE")) {
        console.log("Content script received: " + event.data.text);
        port.postMessage(event.data.text);
    }
}, false);

// var s = '$(document).ajaxComplete(function( event, request, settings ) {console.log("injectedScript  ajaxComplete" , event,request,settings)})'
//
//
// $( document ).ajaxStart(function() {
//     console.log("ajaxStarted")
// })'