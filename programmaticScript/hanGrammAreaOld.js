class HanGrammarAreaCEOld extends HanGrammarArea{
    constructor(targetAreaId){
        super(targetAreaId)
        this.mirrorDiv = this.makeHanGrammMirror()
        this.registeringHandlerCE()
    }

    registeringHandlerCE(){
        let that = this;
        let targetAreaElement = this.targetAreaElement;

        // CE
        targetAreaElement.addEventListener("keyup",function(evt){

            that.mirrorDiv.textContent = that.targetAreaElement.innerText
            //errZone 의 단어를 수정할 경우 해당 구역은 errZone이 해제 되어야 한
            let typedNode = document.getSelection().anchorNode;
            //일단 text node 라면 parent node를 선택하고 아니라면 해당노드를 선택 closest를 사용하기위해선
            // elementNode 여야 한다
            let node = typedNode.nodeType === 3 ? typedNode.parentNode : typedNode;
            let errZone;


            if (node.classList.contains("han_gramm_errZone")){
                errZone = node
            }else{
                errZone = node.closest(".han_gramm_errZone")
            }

            if(errZone){
                // console.log(errZone);면
                //만약 errZone Word에 엔터를 쳤다면
                let errZoneNum
                // console.log(evt.keyCode)

                //만약 엔터를 눌러서 gtag가 두개로 갈렸으면 id는 없이 gTag가 복제가 된다
                // 따라서 errZoneNum을 뽑아내려면 위상황에 대비하기 위해서 class 에서 찾아야함
                for (let i = 0; i < errZone.classList.length; i++) {
                    let className = errZone.classList[i];
                    let splited = className.split("han_gramm_errZone_");
                    // han_gramm_errZone_n을 찾기위한 과점
                    if (splited.length === 2) {
                        errZoneNum = splited[1];
                    } else {
                        // "han_gramm_errZone".split("han_gramm_errZone_") 해도 length1짜리 array가 나옴
                    }
                }
                let allErrZone = document.getElementsByClassName("han_gramm_errZone_" + errZoneNum)
                let errWordOnly = that.candidateContents[errZoneNum].errorWord;
                //엔터를 쳤을때는 errzone이 두개다로 갈리므로 그 상황을 대비해야 한
                // console.log(allErrZone.length)
                let allErrZoneLength = allErrZone.length
                let currentPosition = positionUtil.getCurrentCursorPosition(that.targetAreaElement.id)
                let isWordDiscordance = false
                for (let i=0; i < allErrZoneLength; i++){
                    //하나씩 삭제 되므로 무조건 0번째임
                    let detectedErrZone = allErrZone[0];
                    //errZone의 내용이 바뀌면 errZone을 해제한다
                    // console.log(i)
                    // console.log(detectedErrZone)
                    isWordDiscordance = errWordOnly !== detectedErrZone.innerText.trim();
                    if (isWordDiscordance){
                        //만약 두개로 나눠진것 안에 <br> 이 있으면 <br>까지 없애버리므로 innerHTML을 넣는
                        // console.log(detectedErrZone.innerHTML)
                        detectedErrZone.outerHTML = detectedErrZone.innerHTML
                        // console.log(evt.keyCode)
                        if(evt.keyCode !== 9 &&evt.keyCode !== 13 ){
                            positionUtil.setCurrentCursorPosition(that.targetAreaElement,currentPosition )
                        }
                    }
                }
            }
        });

        //errZone에 마우스를 올려놨을때
        targetAreaElement.addEventListener("mouseover", function(evt){

            if (evt.target.classList.contains("han_gramm_errZone")){
                let target = evt.target;
                let errZoneNum = target.id.split("han_gramm_errZone_")[1];
                let rect = target.getBoundingClientRect();
                // console.log(rect);
                let errZonePosX, errZonePosY;
                let errZoneWidth = target.offsetWidth;
                let errZoneHeight = target.offsetHeight;
                // hanGrammarScript가 top Frame에 삽입되었을경우 그냥 getBoundingClinetRect에서 구하면 됨

                if (window.top === window.self) {
                    errZonePosX = rect.x;
                    errZonePosY = rect.y;


                    //hanGrammarScript가
                }else {
                    //현재 frame의 절대 위치
                    let currentFrameAbsolutePosition = getCurrentFrameAbsolutePosition();
                    // console.log(currentFrameAbsolutePosition)
                    //현재 프레임의 절대 위치에서  프레임에서 errZone 까지의 거리를 더하면 전체 x y 가 도출됨
                    errZonePosX = currentFrameAbsolutePosition.x + rect.x;
                    errZonePosY = currentFrameAbsolutePosition.y + rect.y;
                }
                //해당 iframe 내의 위치

                // console.log(errZonePosX, errZonePosY, errZoneHeight, errZoneWidth, that.candidateContents[errZoneNum]);
                window.top.postMessage({
                        type: "errZoneMouseIn",
                        data : [
                            errZonePosX,
                            errZonePosY,
                            errZoneWidth,
                            errZoneHeight,
                            that.candidateContents[errZoneNum],
                            errZoneNum,
                            that.targetAreaId]
                    },
                    window.top.location.href)
            }

        });

        // 이건 mouseout을 원하는 주체가 targetAreaElement가
        // 아니고 children 이기 때문에 mouseleave(최외곽을 빠져나왔을때 적용되기를 원할때)가
        // 아닌 mouseOut 을 사용
        targetAreaElement.addEventListener("mouseout", function(evt){
            if (evt.target.classList.contains("han_gramm_errZone")){
                window.top.postMessage({
                        type: "errZoneMouseOut",
                        data : ""
                    },
                    window.top.location.href)
            }
        });

        //common
        targetAreaElement.addEventListener("paste", function(evt){
            // console.log(evt.target.innerHTML);
            // let clipboardData = evt.clipboardData;
            // console.log(evt.clipboardData)
            // that.rea(evt.target);
            //trim된게 빈것이라면 애초에 빈것을 붙여넣기 한것임 그건 진행X
            let pastedData = evt.clipboardData.getData("text/html") || evt.clipboardData.getData("text/plain");
            let pastedDataTextOnly = evt.clipboardData.getData("text/plain");
            let pastedDataTrimed = pastedData.trim();
            // console.log(pastedDataTrimed.length, pastedDataTrimed === " ");
            // console.log(pastedDataTextOnly)
            // console.log(pastedData)
            if (pastedDataTrimed === "" || pastedDataTrimed.length === 0){
                console.log("returned");
                return
            }else{
                //errZone 작업을 하다보면 이상하게 붙여넣기후 caret이 최초의 errZone에 위치하기 때문에 복사 상황일때

                let currentPosition = positionUtil.getCurrentCursorPosition(that.targetAreaElement.id)
                let textOnlyLength = pastedDataTextOnly.length;
                // console.log(currentPosition, textOnlyLength);
                that.pasteInfo.currentPosition = currentPosition;
                that.pasteInfo.textOnlyLength = textOnlyLength;
            }
            //setTimeout을 안하면 붙여넣기가 완료되기전에 observeArea가 호출 되기 대문에
            //붙여넣기를 완료시킨'후' observeArea가 호출되기 위해 setTimeout을 사용
            //즉 afterPaste 효과를 줌
            setTimeout(function(){
                that.observeArea(that.targetAreaElement);
            },0)
        });
    }

    getContentInRange (){
        // caret 위치까지의 모든 String을 수집 만약 시작위치와 종료위치가 있다면
        //setStart와 setEnd를 적적히 사용할 것 createRange를 사용해야 할
        // for contentedit field
        this.targetAreaElement.focus();
        let _range = document.getSelection().getRangeAt(0);
        let range = _range.cloneRange();
        range.selectNodeContents(this.targetAreaElement);
        range.setEnd(_range.endContainer, _range.endOffset);
        // console.log(range)

        let content = range.toString();
        console.log(content)

        //함부러 sel.addRange를 쓰면 범위선택되는게 보여 버림
        // var sel = window.getSelection();
        // sel.removeAllRanges()
        // sel.addRange(range)
        // console.log(sel.toString(), sel.toString().length)

        //일단 해당 범위 안에 있는 content의 newline여부를 알아야 하기 때문에 documentFragment를
        //호출하여 첫번째 textnode를 추적
        var documentFragment = range.cloneContents();
        // console.log(documentFragment)
        // console.log(documentFragment,documentFragment.childNodes.length);

        let contentConsideringLine = "";
        for (let childNum = 0; childNum < documentFragment.childNodes.length; childNum++){
            let childNode = documentFragment.childNodes[childNum];

            if (childNode.nodeType === Node.TEXT_NODE){
                contentConsideringLine += childNode.nodeValue;
            }else{
                if (childNode.tagName.toLowerCase() === "p" || childNode.tagName.toLowerCase() === "div"){
                    if (childNum !== 0){
                        contentConsideringLine += "\n"
                    }
                    contentConsideringLine += childNode.innerText
                }else if (childNode.tagName.toLowerCase() === "br" || childNode.tagName.toLowerCase() === "br/"){
                    contentConsideringLine += "\n"
                }else{
                    contentConsideringLine += childNode.innerText
                }
            }
        }
        //nbsp 처리를 안하면 검사기 엔진이 제대로 작동을 안하는 듯 보임
        // let contentWithoutNbsp = content.replace(/\u00a0/g, " ");
        let contentConsideringLineWithoutNbsp = contentConsideringLine.replace(/\u00a0/g, " ");
        let contentConsideringLineWithout8203 = contentConsideringLine.replace(/\u200B/g, "");

        console.log(contentConsideringLineWithout8203, contentConsideringLineWithout8203.length)
        // return contentWithoutNbsp;
        return contentConsideringLineWithout8203;
    }


    //잡코리아 위주의 response data 처리를 하지만 되도록 common이 될 수 있도록
    processJobKoreaSpellCheckedData(receivedData){
        let that = this
        let originalContentRightTrimed = receivedData.originalContent.trimRight();
        let contentRange = receivedData.contentRange;
        // console.log(receivedData.checkedData.WordInfo.mcontent)
        //jobKorea만 이 작업을 시킴
        // console.log(receivedData.checkedData.WordInfo.mcontent);
        let checkedData = this.preProcessJobKoreaSpellCheckedData(receivedData.checkedData);
        //candidateContent처리
        this.preProcessJobKoreaCandidateContents(checkedData.WordCandidateList);

        let mcontent = checkedData.WordInfo.mcontent;
        //checked된 데이터에 <br> 이 string으로 밖혀서 올수 있음
        mcontent = mcontent.replace(/<br>/g, '')
        mcontent = mcontent.replace(/<br\/>/g, '')
        // 두칸 이상뛴 내용에 nbsp가 섞여서 올 수 있음
        mcontent = mcontent.replace(/\&nbsp\;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp/g, ' ')

        //jobKorea에서 String으로 받아오
        let dummyElem = document.createElement(("div"));
        dummyElem.innerHTML = mcontent;

        dummyElem.innerHTML = dummyElem.innerHTML.replace(/<br>/g, '');
        dummyElem.innerHTML = dummyElem.innerHTML.replace(/<br\/>/g, '');
        console.log(mcontent)
        // console.log(dummyElem)
        let modifiedContentInfoArray = [];
        // console.log(dummyElem,dummyElem.childElementCount, dummyElem.children, dummyElem.childNodes)
        //jobKorea 형식(태그안에 errWord가 있는 전체문장)으로 오는것을 파싱하여 정보를 갖춘 array로 만든다.
        for (let i = 0; i < dummyElem.childElementCount ; i++){
            //html string으로 받아지는 mcontent내용을 parsing하기 위한 작업
            let childrenMcontent = dummyElem.children[i];
            //여기있는 id 값이 spell checked 된 반환내용중 candidate words의 몇번째 내용인지를 결정한다
            // 이건 jobKorea기준이므로 달라질 수 있음
            let childrenMcontentId = childrenMcontent.id;

            let childOuterHtml =  childrenMcontent.outerHTML;
            let childOuterHtmlLength = childOuterHtml.length;

            let childInnerText = childrenMcontent.innerText;
            let childInnerTextLength = childInnerText.length;


            //errWordWithTag로 위치 파악하고 errWordOnlyLength로 범위를 구한다
            let errWordOnly = childInnerText;
            let errWordOnlyLength = childInnerTextLength;

            let errWordWithTag = childOuterHtml;

            //id 부분이 숫자가 다 다르기 때문에 동일한 errword를 가지고 있다해도 무방함
            let errWordStartPos = mcontent.indexOf(errWordWithTag);
            let errWordEndPos = errWordStartPos + errWordOnlyLength;

            //errWordOnly로 치환 함으로서 다음 순회에서 위치를 뽑아낼수 있음
            mcontent = mcontent.replace(errWordWithTag, errWordOnly);
            // 앞에서부터 치환된 문장을 기준으로 위치를 기록

            let errWordZoneNumber = this.errWordZoneNumber++;
            // console.log(errWordZoneNumber)
            let errWordZoneElem = this.makeErrWordZoneElem(errWordOnly, errWordZoneNumber);
            // console.log(checkedData.WordCandidateList)
            // console.log(childrenMcontentId, checkedData.WordCandidateList[childrenMcontentId])
            this.candidateContents[errWordZoneNumber] = checkedData.WordCandidateList[childrenMcontentId]

            // this.candidateWords = {}
            modifiedContentInfoArray.push({"errWordZoneElem" : errWordZoneElem, "errWordOnly" : errWordOnly,
                "errWordWithTag" : errWordWithTag, "errWordStartPos" : errWordStartPos,
                "errWordEndPos" : errWordEndPos})
        }

        //최초에 맞춤법 검사를 통째로 보낸 범위
        let startPos = receivedData.contentRange.startPos;
        //endPos는 사실상 어느정도의 범위가  검사로 넘어갔는지 확인용도 외에는 필요가 없을듯
        let endPos = receivedData.contentRange.endPos;
        for (let i = 0; i < modifiedContentInfoArray.length; i++){
            console.log(modifiedContentInfoArray)
            //통째로 보낸 범위 부터 시작한 errWord의 시작위치
            let modifiedContentInfo = modifiedContentInfoArray[i]
            let errWordStartPos = modifiedContentInfo.errWordStartPos;
            let errWordEndPos = modifiedContentInfo.errWordEndPos;
            //이게 적합한 위치에 있는 errWord와 바꿔치기 된다
            let errWordZoneElem = modifiedContentInfo.errWordZoneElem;
            let errWordOnly = modifiedContentInfo.errWordOnly

            //체커가 잡코리아나 사람인 체커를 것을 파싱해서 사용할때 그쪽에서 보내주는 errWord 를 추린것임 테스트용으로사용했음
            let errWordWithTagFromChecker = modifiedContentInfoArray[i].errWordWithTag;

            // console.log(startPos, errWordStartPos, errWordEndPos)
            let finalRange = this.createFinalRangeHG(this.targetAreaElement, startPos + errWordStartPos, startPos + errWordEndPos)

            let clone = finalRange.cloneRange();
            let rangeLength = finalRange.toString().length;
            // console.log(finalRange, finalRange.toString(), rangeLength);
            // console.log(clone)
            // let endContainer = finalRange.endContainer;
            let endContainer = clone.endContainer;
            // console.log(endContainer);


            // 일단 해다당 위치에 err_zone이 있다면
            // 옛날 id값만 현재의 id로 바꿔준
            if (endContainer.parentElement.classList.contains("han_gramm_errZone")){
                // console.log(endContainer, errWordZoneElem)
                let oldErrZone = endContainer.parentNode;
                oldErrZone.id = errWordZoneElem.id;
                let oldErrZoneNum;
                for (let i = 0; i < oldErrZone.classList.length; i++) {
                    let className = oldErrZone.classList[i];
                    let splited = className.split("han_gramm_errZone_");
                    // han_gramm_errZone_n을 찾기위한 과점
                    if (splited.length === 2) {
                        oldErrZoneNum = splited[1];
                        oldErrZone.classList.remove("han_gramm_errZone_" + oldErrZoneNum)
                        //id 자체가 "han_gramm_errzone_" 이 포함되어 있다.
                        oldErrZone.classList.add(errWordZoneElem.id)
                    }
                }
                // console.log(endContainer, endContainer.id);
                continue;
            }

            clone.collapse()

            clone.insertNode(errWordZoneElem);
            // errWordZoneElem.classList.add("han_gramm_line")

            finalRange.deleteContents()
            // }, (i + 1) * 3000)

        }
        //paste 상황일때 errZone을 만들면서 붙여넣기가 완료됐을경우 caret 의 위치가 붙여넣은 내용의 끝이아닌
        //임의의 errZone으로 가있음 따라서 paste 상황일때만 caret의 위치를 끝으로 이동시켜줌
        if(this.pasteInfo.hasOwnProperty("textOnlyLength")){
            let currentPosition = this.pasteInfo["currentPosition"];
            let textOnlyLength = this.pasteInfo["textOnlyLength"];

            positionUtil.setCurrentCursorPosition(this.targetAreaElement,currentPosition + textOnlyLength)
            //paste정보를 다시 없애
            this.pasteInfo = {}
        }else{
            document.getSelection().getRangeAt(0).collapse()
        }
    }


    // g태그 안의 errWordZone을 만듦
    makeErrWordZoneElem (errWordOnly, number){
        // console.log(number, typeof(number))
        let errWordZoneElem = document.createElement("g");
        errWordZoneElem.id = "han_gramm_errZone_" + number;
        errWordZoneElem.innerText = errWordOnly;

        errWordZoneElem.setAttribute("class", "han_gramm_errZone");
        errWordZoneElem.classList.add("han_gramm_errZone_" + number);
        errWordZoneElem.style.borderBottom = "thin solid red";
        //이걸 안해놓으면  html상에서 errWordZoneElem 을 해석할때
        // 칸 두개이상을 실제 보이는 rendering 화면이 반영을 못하고 한칸으로 취급
        errWordZoneElem.style.whiteSpace = "pre";
        // console.log(errWordZoneElem)
        return errWordZoneElem
    }

    //대체어를 쿨릭했을때 postMessage로 받아서 수정
    processCorrectionBySelectedWord(errWord, selectedWord, errZoneNum){
        let foundedErrZoneElem = document.querySelector("g.han_gramm_errZone.han_gramm_errZone_" + errZoneNum)

        // console.log(foundedErrZoneElem)
        if (foundedErrZoneElem.innerText === errWord){
            foundedErrZoneElem.outerHTML = selectedWord
        }else{
            //일단 이것도 교정하는걸로
            foundedErrZoneElem.outerHTML = selectedWord
            throw Error("incorrect ErrWord");
        }
    }

    makeHanGrammMirror(){

        var properties = [
            'boxSizing',
            'width',  // on Chrome and IE, exclude the scrollbar, so the mirror div wraps exactly as the textarea does
            'height',
            'overflowX',
            'overflowY',  // copy the scrollbar for IE

            'borderTopWidth',
            'borderRightWidth',
            'borderBottomWidth',
            'borderLeftWidth',

            'paddingTop',
            'paddingRight',
            'paddingBottom',
            'paddingLeft',

            // https://developer.mozilla.org/en-US/docs/Web/CSS/font
            'fontStyle',
            'fontVariant',
            'fontWeight',
            'fontStretch',
            'fontSize',
            'lineHeight',
            'fontFamily',

            'textAlign',
            'textTransform',
            'textIndent',
            'textDecoration',  // might not make a difference, but better be safe

            'letterSpacing',
            'wordSpacing'
        ];

        let hanGrammMirror = document.createElement("hanGramm-mirror")
        let mirrorDiv = document.createElement("div")
        hanGrammMirror.appendChild(mirrorDiv)
        // document.getElementsByTagName("html")[0].append(hanGrammMirror)
        window.top.document.getElementsByTagName("html")[0].append(hanGrammMirror)

        let mirrorDivStyle = mirrorDiv.style;
        let computed = window.getComputedStyle(this.targetAreaElement)

        //copy all style to hanGramm mirror
        // mirrorDivStyle.cssText = document.defaultView.getComputedStyle(this.targetAreaElement, "").cssText

        mirrorDivStyle.whiteSpace = 'pre-wrap';
        mirrorDivStyle.wordWrap = 'break-word';

        // position off-screen
        mirrorDivStyle.position = 'fixed' //required to return coordinates properly
        mirrorDivStyle.top = "0px"
        mirrorDivStyle.left = "0px"

        mirrorDivStyle.visibility = 'hidden';

        mirrorDivStyle.overflow = 'hidden'

        properties.forEach(function(prop){
            mirrorDivStyle[prop] = computed[prop]
        })


        //textarea 창 크기를 sync 해줌
        let observer = new MutationObserver(function(mutation) {
            console.log(mutation)
            mirrorDiv.style.width = mutation[0].target.offsetWidth + "px"
            mirrorDiv.style.height = mutation[0].target.offsetHeight +"px"
        })
        observer.observe(this.targetAreaElement,{attributes : true, attributeFilter : ['style']})

        return mirrorDiv
    }
}