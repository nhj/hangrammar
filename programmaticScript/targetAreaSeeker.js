'use strict';

console.log("i'm in");
//나는 누구냐 너는 누구냐 이땅에 태어나 이땅에 신토불이 신토부울이 신토불이야 규안아
// console.log(window.location.href)
// console.log(window.name)
// let frameURL = document.location.href;
let os;
let correctionServer = ""
let processActivated = true
// 하나의 frame 안에서 복수의 targetArea가 존재할 수 있으므로 seeker는 계속 작동된다.
// frameNotified 의 여부는 해당 frame에 hanGrammarScript를 injection 시켰는지 여부만을 따지고 1회만 통보되면 그이상은
// 통보할 필요 없음
let frameNotified = false;

let framePort;

let activation = false;

let seekerMainMethod = {

    handleRecognizingFrame : function(evt){
        //processActivated 가 꺼져 있으면 무조건 활성화 시키지 않는
        // console.log(processActivated)
        if(!processActivated){
            return
        }

        // console.log(evt.target)
        let target = evt.target;

        let hanGrammArea = false;
        let areaType = "";
        if ((target.tagName.toLocaleLowerCase() === "input" && target.type === "textarea") ||
            target.tagName.toLowerCase() === "textarea"){
            hanGrammArea = true;
            areaType = "textarea"
        }else{
            //click event의 경우 caret 이 위치한 element 가 아니라 그 상위의 element가 target이 되는 경우가
            //있으므로 getSelection의 anchor Node 로 추적해 들어간다
            if (evt.type === "click"){
                var node = document.getSelection().anchorNode;
                for (let i=0; i < 3; i++){
                    // console.log(i, node)
                    //링크를 클릭해서 넘어갔을때 node가 null 로 뜬다 따라서 처리해 줘야
                    if(node === null){
                        break
                    }
                    if (node.nodeType === 3){
                        node = node.parentNode
                    }else{
                        if (node.contentEditable === "true"){
                            target = node;
                            break
                        }else{
                            node = node.parentNode
                        }
                    }
                }
                // target = node.nodeType == 3 ? node.parentNode : node;
            }else{
                target = evt.target
            }
            // console.log(target.isContentEditable)
            if (target.contentEditable === "true" ||
                //frame 자체에 designMode = "on" 으로 된것들을 가려내기 위함임
                (target.contentEditable === "inherit" && document.designMode === "on" && target.tagName.toLowerCase() === "body")){
                hanGrammArea = true;
                areaType = "contenteditable";
            }
        }
        // console.log(target);

        if (hanGrammArea) {
            // frameNotified  가 아닐 경우 어떠한 area도 표시되지 않은 상태 이므로 makeHanGrammAttribute 수행
            // console.log("start");
            if (!frameNotified){
                seekerMainMethod.makeHanGrammAttribute(target, areaType);
                seekerMainMethod.notifyTargetFrame(target);
            }else{
                let isRecognizedArea = target.getAttribute("_han_gramm");
                if (!isRecognizedArea){
                    seekerMainMethod.makeHanGrammAttribute(target, areaType)
                }
            }
        }
    },

    // 조건에 맞는 frame을 background로 전달하는데 쓰임
    notifyTargetFrame : function(target){
        //frameId는 background 측의 onMessage event에서 알 수 있으므로 굳이 넘기지 않아도 됨
        let port = chrome.runtime.connect({name : "properFrameFounded"});
        // chrome.runtime.sendMessage({fromSeeker : true});
        framePort = port;
        frameNotified = true;
    },

    // 조건에 맞는 element에 attribute를 표시한다.
    makeHanGrammAttribute : function(target, areaType){
        console.log(target);
        let hanGrammId = new Date().getTime().toString();
        target.setAttribute("_han_gramm", true);
        target.setAttribute("_han_gramm_id", hanGrammId);

        let errCount = 0;
        // hanGrammJsInjected 여부를 확인하기 위해 undefined error Catch
        (function confirmHanGrammJsInjected(){
            try {
                if (hanGrammJsInjected == true) {
                    // console.log(hanGrammJsInjected);
                    if(areaType ==="contenteditable"){
                        areaObjs[hanGrammId] = new HanGrammarAreaCE(hanGrammId);
                    }else if(areaType ==="textarea"){
                        areaObjs[hanGrammId] = new HanGrammarAreaTA(hanGrammId);
                    }
                    return true
                }
            }catch (e) {
                if (errCount < 3){
                    setTimeout(confirmHanGrammJsInjected, 100);
                    errCount++
                }else{
                    console.log(e);
                    return true
                }

            }
        })();

    },


};

(function processEventListener(){
    document.addEventListener("input", seekerMainMethod.handleRecognizingFrame);
    document.addEventListener("click", seekerMainMethod.handleRecognizingFrame);
    document.addEventListener("paste", seekerMainMethod.handleRecognizingFrame);
})();

//일부러 한번지우고 다시 등록  이유는 모르겠는데 이렇게 안하면 안되는 경우가 있음
setTimeout(function(){
    document.removeEventListener("select", seekerMainMethod.handleRecognizingFrame);
    document.removeEventListener("click", seekerMainMethod.handleRecognizingFrame);
    document.removeEventListener("input", seekerMainMethod.handleRecognizingFrame);
    (function processEventListener(){
        document.addEventListener("click", seekerMainMethod.handleRecognizingFrame);
        document.addEventListener("input", seekerMainMethod.handleRecognizingFrame);
        document.addEventListener("paste", seekerMainMethod.handleRecognizingFrame)
    })();
});
//-----------------------------------------------------------------------------------------
//hanGramm Card Controll
function registeringGrammCardTemplateHandler(grammCard){
    grammCard.addEventListener("mouseover", function(evt){
        // console.log(evt.target)
        //han_gramm_card 최외곽
        let curTarget = evt.currentTarget

        //candidate or ignore 등등
        let target = evt.target
        if(target.parentElement.parentElement.classList.contains("han_gramm_card_candidates") ||
            target.parentElement.parentElement.classList.contains("han_gramm_card_ignore")){
            target.classList.add("mouseOver")
        }
        // target.classList.add("mouseOver");
        // console.log(target.parentElement.parentElement)
        // console.log(target.parentElement.parentElement.classList)
        curTarget.classList.add("han_gramm_card_mouseinCard");

    });


    //mouseout으로 할 경우 내부에 있는 children에도 event가 적용도되기 때문에 짜증나짐
    // 따라서  최 외곽만을 다룰때에는 mouseleave로 함
    grammCard.addEventListener("mouseleave", function(evt){
        let target = evt.target
        // console.log(target, target)
        target.classList.remove("han_gramm_card_mouseinCard")

        if(target === evt.target){
            let hanGrammCard = target
            //만약 잠깐 mouse가 나갔다가 곧바로 들어오면 없애지 card를 없애지 않는다
           setTimeout(function(){
                if (!hanGrammCard.classList.contains("han_gramm_card_mouseinCard")){
                    hanGrammCard.remove()
                }
            }, 500)
        }
        // console.log(evt.target)

    })

    grammCard.addEventListener("mouseout", function(evt){
        //card 내부 요소
        let target = evt.target
        //card 최외곽
        let curTarget = evt.currentTarget

        if(target.classList.contains("mouseOver")){
            target.classList.remove("mouseOver")
        }

    })
}

function createHanGrammCardTemplate(candidateContent, errZoneNum, areaObjId, sourceElem){
    // console.log(candidateContent, errZoneNum, areaObjId);
    let outerHanGrammCard = document.createElement("hanGramm-card");
    registeringGrammCardTemplateHandler(outerHanGrammCard);

    let cardMain = document.createElement("div");
    cardMain.setAttribute("class", "hanGramm_card_main");

    let checkedReason = document.createElement("div");
    checkedReason.classList.add("han_gramm_card_checkedReason");

    let candidates = document.createElement("div");
    candidates.classList.add("han_gramm_card_candidates");
    candidates.setAttribute('style', "width:100%");

    //candidate word의 갯수에 따라서 div tag를 만든다
    for (let i=0; i < candidateContent.candidateWordList.length; i++){
        let candidateWord = candidateContent.candidateWordList[i];

        let candidateElem = document.createElement("div");
        candidateElem.errWord  = candidateContent.errorWord;
        candidateElem.errZoneNum = errZoneNum;
        candidateElem.areaObjId = areaObjId;
        candidateElem.innerText = candidateWord;
        candidateElem.setAttribute("style", "width : 100%; height : auto;");

        let aTag = document.createElement("a");

        aTag.onclick = function(evt){

            let hanGrammCardCurrentlyExisted = document.getElementsByTagName("hanGramm-card")[0]
            if (hanGrammCardCurrentlyExisted !== undefined){
                hanGrammCardCurrentlyExisted.remove()
            }

            let target = evt.target;
            // console.log(evt.currentTarget);
            // console.log(evt.target, evt.target.errZoneNum, evt.target.areaObjId, evt.target.innerText)

            sourceElem.postMessage({
                type : "candidateWordSelected",
                data:[
                    target.errWord,
                    target.innerText,
                    target.errZoneNum,
                    target.areaObjId
                    ]
            })
        };
        aTag.appendChild(candidateElem);
        candidates.appendChild(aTag);
    }

    let ignore = document.createElement("div");
    ignore.classList.add("han_gramm_card_ignore");
    ignore.setAttribute('style', "width:100%");
    let ignoreInner = document.createElement("div")
    ignoreInner.setAttribute("style", "width : 100%; height : 40px;");
    ignoreInner.innerText = "IGNORE"
    let ignoreAtag = document.createElement("a");
    ignoreAtag.onclick = function(evt){
        let hanGrammCardCurrentlyExisted = document.getElementsByTagName("hanGramm-card")[0]
        if (hanGrammCardCurrentlyExisted !== undefined){
            hanGrammCardCurrentlyExisted.remove()
        }

        sourceElem.postMessage({
            type : "ignoreSelected",
            data:[errZoneNum, areaObjId]
        })
    }
    ignoreAtag.appendChild(ignoreInner)
    ignore.appendChild(ignoreAtag)

    //helpMessage 최외곽
    let helpMeassageDiv = document.createElement("div");
    helpMeassageDiv.classList.add("han_gramm_card_help");
    let helpMessageDivided = candidateContent.helpMessageDivided;
    //거의 checkedReason이라고 봐도 무방
    let mainHelpMsgDiv = document.createElement("div");
    mainHelpMsgDiv.style.fontSize = "10px";
    mainHelpMsgDiv.innerText = helpMessageDivided[0];
    //mainHelgMsgDiv를 클릭하면 적용되는 동작
    mainHelpMsgDiv.onclick = function(evt){
        this.classList.toggle("active");
        let addedMsgDiv = this.nextElementSibling;
        if (addedMsgDiv.style.display === "block") {
            addedMsgDiv.style.display = "none";
        } else {
            addedMsgDiv.style.display = "block";
        }
    };

    //mainHelpMsg를 클릭하면 튀어나오는 부가설명
    let addedHelpMsgDiv = document.createElement("div");
    addedHelpMsgDiv.innerHTML = helpMessageDivided[1];
    addedHelpMsgDiv.style.display = "none";
    addedHelpMsgDiv.style.fontSize = "8px";
    helpMeassageDiv.appendChild(mainHelpMsgDiv);
    helpMeassageDiv.appendChild(addedHelpMsgDiv);

    cardMain.appendChild(checkedReason);
    cardMain.appendChild(candidates);
    cardMain.appendChild(ignore)
    cardMain.appendChild(helpMeassageDiv);

    outerHanGrammCard.appendChild(cardMain);
    return outerHanGrammCard
}

function locateHanGrammCardOnPage(data, cardTemplate){
    let errZonePosX = data[0];
    let errZonePosY = data[1];
    let errZoneWidth = data[2];
    let errZoneHeight = data[3];

    var vpWH = [];
    var vpW, vpH;
    var intCoordX = errZonePosX;
    var intCoordY = errZonePosY;

    var intXOffset = intCoordX;
    var intYOffset = intCoordY;

    //화면 전체의 너비 높이 픽셀값\
    //getViewPortWidthHeight() 과 동일한
    let viewPortWidth = window.innerWidth;
    let viewPortHeight = window.innerHeight;

    // popup.style.position = 'fixed';
    // // if not display: block, .offsetWidth & .offsetHeight === 0
    // popup.style.display = 'block';
    // popup.style.zIndex = '10100';
    cardTemplate.style.position = 'fixed';
    // if not display: block, .offsetWidth & .offsetHeight === 0
    // cardTemplate.style.display = 'block';
    cardTemplate.style.zIndex = '9999999';

    //popupclick 부분의 위치가 전체 width의 절반보다 크다면 popup의 너비만큼 offset에서 빼줌
    // 결국 원래 offset보다 좌측에 위치하게 된다
    // if ( intCoordX > vpW/2 ) { intXOffset -= cardTemplate.offsetWidth; }
    // if ( intCoordY > vpH/2 ) { intYOffset -= cardTemplate.offsetHeight; }
    //
    // if ( vpW <= 500 ) { intXOffset = ( vpW - cardTemplate.offsetWidth ) / 2;}
    // if ( vpH <= 500 ) { intYOffset = (vpH - cardTemplate.offsetHeight ) / 2;}

    //firstElement 로 한번 내려가야 진짜 Height가 나옴
    let cardTemplateHeight = parseFloat(window.getComputedStyle(cardTemplate.firstElementChild).height.replace("px", ""))
    console.log(cardTemplateHeight)

    //일반적인 상황일때는 errZone 바로 아래에 cardpopup이 위치할수 있게
    if(viewPortHeight - intYOffset >= cardTemplateHeight){
        // console.log(errZoneHeight);
        intYOffset += errZoneHeight
    //bottom 부터 errZone까지의 길이가 card의 높이보다 좁을때는 errZone의 위쪽으로 위치시킬수 있도록
    }else{
        intYOffset -= (cardTemplateHeight + errZoneHeight * 1.5)
    }
    // errZone보다 10px 정도 뒤에 위치
   intXOffset -= 10;

    // popup.style.top = intYOffset + 'px';
    // popup.style.left = intXOffset + 'px';
    // popup.style.visibility = 'visible';

    cardTemplate.style.top = intYOffset + 'px';
    cardTemplate.style.left = intXOffset + 'px';
    // cardTemplate.style.visibility = 'visible';
    // cardTemplate.style.display = "block";


}



//이건 extension간의 소통임
//상단의 framePort는 개별 area가 발견된 뒤에 hanGrammarScript.js가 inject된 frame에 할당된 것이고
//이것은 모든 것에 영향을 미친다
chrome.runtime.onMessage.addListener(function(message, sender){
    // console.log(message)
    switch(message.type){
        case "processActivated":
            processActivated = true;
            // console.log(processActivated)
            break;

        case "processDeactivated":
            processActivated = false;
            // console.log(processActivated)
            break;

        case "osInfo":
            // console.log(message.data)
            os = message.data
            break;

        case "correctionServerInfo":
            // console.log(message)
            correctionServer = message.data
            break;
    }

});



//extension간의 소통이 아닌 iframe간의 소통임
if (window.top == window.self){

    window.addEventListener("message",function(msg){
        let source = msg.source;
        let type = msg.data.type;
        let data = msg.data.data;
        switch (type){
            case "errZoneMouseIn":
                let hanGrammCardCurrentlyExisted = document.getElementsByTagName("hanGramm-card")[0]
                // console.log(hanGrammCardCurrentlyExisted , hanGrammCardCurrentlyExisted !== undefined, hanGrammCardCurrentlyExisted !== null)
                if (hanGrammCardCurrentlyExisted !== undefined){
                    hanGrammCardCurrentlyExisted.remove()
                }

                let candidateContent = data[4];
                let candidateContentNum = data[5];
                let targetAreaId = data[6];
                let cardTemplate = createHanGrammCardTemplate(candidateContent, candidateContentNum, targetAreaId, source);

                //insert card popup
                let parentNode = document.body;
                let targetNode = parentNode.children[0]
                parentNode.insertBefore(cardTemplate, targetNode);
                // cardTemplate.style.visibility = "hidden";
                // cardTemplate.style.display = "none";
                locateHanGrammCardOnPage(data, cardTemplate);
                break;

            //errZone에서 Mouseout이 바깥으로 나갈
            case "errZoneMouseOut":
                // console.log("errzoneMouseOut received")
                let hanGrammCard = document.getElementsByTagName("hanGramm-card")[0]
                setTimeout(function(){
                    if (!hanGrammCard.classList.contains("han_gramm_card_mouseinCard")){
                        hanGrammCard.remove()
                    }
                }, 500)
        }
    })
}


// <body class="tx-content-container"
// style="height: 375px; background-color: rgb(255, 255, 255); padding: 8px;"
// contenteditable="true"
// data-gramm_id="7ce85f0f-5ee1-ec5f-5935-ce6c8f2e056e"
// data-gramm="true"
// spellcheck="false"
// data-gramm_editor="true"><p><br></p></body>

// let iframes = document.getElementsByTagName("iframe");

// for (let iframeElem in iframes){
//     iframeElem.addEventListener("input", function(target){
//         console.log('ive found input area, in the iframe')
//     })
// }

// //네이버 블로그는 최초 페이지가 불러진후 frame 으로 해당 게시불을 불러오고,
// // 댓글 frame도 추후에 불러온다
// // 2017년 Top mainFrame hiddenFrame commnetFrame(댓글창 여기) 으로 구성
// // 2018년 top mainFrame(댓글창 여기) hiddenFrame 으로 구성
//
// var frameURL = document.location.href
// var frameName = "";
// var scriptResult = {};
// //allFrame으로 모든 frmae에  contentScript 가 injection  되었으므로  각 injection 된 context에 frameName을  지정한다
// //나중에  각 port에 이름을 넣어 background에서 각자 관리하기 위해 쓰임
// console.log(frameURL);
// if(frameURL.match("PostList")|| frameURL.match("PostView")){
//     frameName = "contentFrame";
// }else if(frameURL.match("Hidden")){  //쓸모없지만 일단 네이밍만 해둠
//     frameName = "hiddenFrame"
//     // 2017년 당시에는 commentFrame이 따로 있어서 댓글
//     // 적는곳이 comment전용 Iframe 안에 따로 있었음 2018 현재는 contentFrame에 통합된 것으로 보임
// // }else if(window.frameElement.id.match("CommentFrm")){
// //     frameName = "commentFrame"
// }
//
// console.log(frameName);
// var injectedTabId = -1;
//
// //굳이 생성자 쓸일없고 생성자에 function넣을 필요도 없지만 심심해서 그냥했음 깊게 생각하지 말것
// function ScriptMainMethods(){
//     this.realClick = function(element){
//         var event = document.createEvent("MouseEvents");
//         event.initEvent("click", true, true);
//         element.dispatchEvent(event);
//     }
// }
//
// //contentFrame에 inject된 script만 port활성화
// if(frameName == "contentFrame") {
//
//     var addedMethods = {
//         openCommentArea: function () {
//             // var isCommentFrameOpened = ($("iframe[id^=CommentFrm]").css("display") !== 'none');
//             var isCommentAreaOpened = ($("div#naverComment_201_220955760032_ct").css("display") != 'none')
//             console.log(isCommentAreaOpened);
//             //바로 열어준다
//             if (!isCommentAreaOpened) {
//                 // scriptMainMethods.realClick($("a#Comi220955760032")[0]);
//                 console.log(this);
//                 this.realClick($("a#Comi220955760032")[0]);
//
//             }
//             //안열려서 열었더라도 안열려있었다는 사실을 return함
//             return isCommentAreaOpened;
//         }
//     };
//
//     //생성자 패턴으로 바꾸면서 없앴음 필요없음 지울것
//     //각 addedMethods를 scriptMainMethods에 추가
//     //     for (var i in addedMethods) {
//     //         if (addedMethods.hasOwnProperty(i)) {
//     //             scriptMainMethods[i] = addedMethods[i]
//     //         }
//     //     }
//
//     ScriptMainMethods.prototype = addedMethods;
//
//     var scriptMainMethods = new ScriptMainMethods();
//
//     // #여기서 port를 여는 순간 background측의 chrome.runtime.onConnect.addListener가 반응함
//
//     var port = chrome.runtime.sendMessage();

//
//     port.onMessage.addListener(function (msg) {
//         switch (msg.msgType) {
//             case "openCommentList":
//                 scriptMainMethods.realClick($("strong>a[href='#']")[0]);
//                 break;
//             case "killScript":
//                 port.disconnect()
//                 break;
//
//             case "beacon":
//                 console.log("i am alive");
//                 alert("asdf")
//                 port.postMessage({answer: "fuck!! im not dead"});
//                 break;
//             case "alert":
//                 alert(msg.message);
//         }
//
//     });
//     // console.log($("iframe[id^=CommentFrm").css("display"));
//     var isCommentAreaOpened = scriptMainMethods.openCommentArea()
//
//     // console.log(isCommentAreaOpened);
//     scriptResult = {frameName: frameName, isCommentAreaOpened: isCommentAreaOpened, frameURI: window.location.href};
//
// }
//

//     ScriptMainMethod`s.prototype = addedMethods
//
//     var scriptMainMethods = new ScriptMainMethods();
//
//     var port = chrome.runtime.connect({name: frameName});
//
//     port.onMessage.addListener(function(msg) {
//         switch (msg.msgType){
//             case "injectedTabId":
//                 injectedTabId = msg.data;
//                 break;
//             case "killScript":
//                 port.disconnect()
//                 break;
//
//             case "checkPutReady": //필요없음 지울것
//                 console.log("i am alive");
//                 alert("asdf");
//                 var $commentTextArea = $("#commentTextArea");
//                 var $masterOnly = $("#master_only");
//                 $commentTextArea.val() == "" && $masterOnly;
//                 var $putBtn = $("input[alt='댓글입력']");
//
//                 break;
//             case "putComment":
//                 scriptMainMethods.putComment(msg.data)
//                 // port.postMessage({answer : "fuck!! im not dead"});
//                 break;
//
//         }
//     });
//
//     scriptResult = {frameName :frameName,  frameURI: window.location.href };
//     //executeScript 의 callback 함수의 인자로 넘어간다
// }
//
// if(frameName == "commentFrame" || frameName == "contentFrame"){
//     //이게 chrome.tabs.executeScript의 callback 파라미터로 나온다
//     scriptResult  //가장 마지막에 있는것만 반환시킴
// }