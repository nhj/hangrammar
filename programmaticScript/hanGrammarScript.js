'use strict';
const SARAMIN ="SARAMIN"
const JOBKOREA = "JOBKOREA"
let hanGrammJsInjected = true;
//background 에서는 tabControlObjs 롤 tabController 객체를 관리하고 areaObjs는 개별 탭 안의 frame 안의
framePort = framePort

let areaObjs = {};

function getCurrentFrameAbsolutePosition() {
    let currentWindow = window;
    let currentParentWindow;
    let positions = [];
    let rect;

    while (currentWindow !== window.top) {
        currentParentWindow = currentWindow.parent;
        for (let idx = 0; idx < currentParentWindow.frames.length; idx++)
            if (currentParentWindow.frames[idx] === currentWindow) {
                for (let frameElement of currentParentWindow.document.getElementsByTagName('iframe')) {
                    if (frameElement.contentWindow === currentWindow) {
                        rect = frameElement.getBoundingClientRect();
                        positions.push({x: rect.x, y: rect.y});
                    }
                }
                currentWindow = currentParentWindow;
                break;
            }
    }
    return positions.reduce((accumulator, currentValue) => {
        return {
            x: accumulator.x + currentValue.x,
            y: accumulator.y + currentValue.y
        };
    }, { x: 0, y: 0 });
}

function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    // Change this to div.childNodes to support multiple top-level nodes
    return div.firstChild;
}

function generateQuickGuid() {
    return Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
}

//---------------------------------------------------------------------------------------

let positionUtil = {

    createRange : function createRange(node, chars, range) {
        if (!range) {
            range = document.createRange()
            range.selectNode(node);
            range.setStart(node, 0);
        }

        if (chars.count === 0) {
            range.setEnd(node, chars.count);
        } else if (node && chars.count >0) {
            if (node.nodeType === Node.TEXT_NODE) {
                if (node.textContent.length < chars.count) {
                    chars.count -= node.textContent.length;
                } else {
                    range.setEnd(node, chars.count);
                    chars.count = 0;
                }
            } else {
                for (var lp = 0; lp < node.childNodes.length; lp++) {
                    range = createRange(node.childNodes[lp], chars, range);

                    if (chars.count === 0) {
                        break;
                    }
                }
            }
        }
        return range;
    },

    setCurrentCursorPosition : function setCurrentCursorPosition(baseNode,chars) {
        // console.log(chars)
        if (chars >= 0) {
            var selection = window.getSelection();

            let range = this.createRange(baseNode, { count: chars });

            if (range) {
                range.collapse(false);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    },


    isChildOf : function isChildOf(node, parentNode) {
        while (node !== null) {
            if (node === parentNode) {
                return true;
            }
            node = node.parentNode;
        }

        return false;
    },

    getCurrentCursorPosition : function getCurrentCursorPosition(parentNode) {
        var selection = window.getSelection(),
            charCount = -1,
            node;

        if (selection.focusNode) {
            if (this.isChildOf(selection.focusNode, parentNode)) {
                node = selection.focusNode;
                charCount = selection.focusOffset;

                while (node) {
                    if (node === parentNode) {
                        break;
                    }

                    if (node.previousSibling) {
                        node = node.previousSibling;
                        charCount += node.textContent.length;
                    } else {
                        node = node.parentNode;
                        if (node === null) {
                            break
                        }
                    }
                }
            }
        }

        return charCount;
    },

    getCurrentPositionBasedOnRangeObj : function(rangeObj){

    },

    findFirstTextNode : function(node){
        let firstTextNode;
        for(let i = 0; i < node.childNodes.length; i ++){
            if(node.childNodes[i].nodeType === 3){
                return node.childNodes[i]
            }else{
                // console.log(node.childNodes.length)
                if (node.childNodes.length >= 1){
                    firstTextNode = this.findFirstTextNode(node.childNodes[i])
                    if (firstTextNode !== undefined){
                        break;
                    }
                }
            }
        }

        return firstTextNode
    }
}

//rangeObj를 감싸는 wrapper 개념임
class RangeInfo{
    constructor(areaObj, rangeObj, absoluteStartPos, absoluteEndPos, errWordOnly, errWordZoneNumber, oneWideRangeString){
        this.areaObj = areaObj
        this.absoluteStartPos = absoluteStartPos
        this.absoluteEndPos = absoluteEndPos
        this.errWordOnly = errWordOnly
        this.errWordZoneNumber = errWordZoneNumber
        // CE에서 종종 endOffset이나 container가 원인을 모르는 이유로 바뀔때가 있다 그러나 spcing scrolling으로 조정되면
        //올바른 range 값으로 돌아오되 교정을 눌렀을때 가져오면 안되는 경우가 생김 따라서 교정때는
        // 여기 저장된 값을 쓰는게 아니라 absoluteStartPos와 absoluteEndPos로 새로 만들어서 교정을 함
        this.rangeObj = rangeObj
        this.oneWideRangeString = oneWideRangeString
        this.localCoord
        //객체 생성시에만 한번 호출
        this.ghostErrWordZone;
        this.originTop = 0
        this.originLeft =0
        this.ignored = false
    }

    set rangeObj(rangeObj){
        this._rangeObj = rangeObj
        if(!this.ignored){
            this.setRangeLocalCoord()
            //CE가 최외곽 프레임에 있을때는 최외곽 스크롤에 의해서 영향을 받으므로 최외곽에 있는 CE는
            //scrollTop을 반영하지 않는다.
            if(this.areaObj.areaType == this.areaObj.CE && window.top !== window.self){
                // 이걸 하지 않으면 스크롤이 올라갔을때 ghostErrZone의 정확한 위치를 잡을수 없다
                let scrollTop = document.documentElement.scrollTop
                let scrollLeft = document.documentElement.scrollLeft
                // this.makeGhostErrZone()
                this.makeGhostErrZone(scrollTop, scrollLeft)
            }else if(this.areaObj.areaType === this.areaObj.TA){
                let scrollTop = this.areaObj.targetAreaElement.scrollTop
                let scrollLeft = this.areaObj.targetAreaElement.scrollLeft
                this.makeGhostErrZone(scrollTop, scrollLeft)
            }else{
                this.makeGhostErrZone()
            }
        }
    }

    get rangeObj(){
        return this._rangeObj
    }

    setRangeLocalCoord(){
        //어차피 mirrorDiv를 position fixed로 맨 좌측 끝에 붙여놨기 때문에
        // 내부에 있는 range들은 바로 local coord가 구해진
        var range = this._rangeObj
        //getBoundingClinetRect로 하면 줄의 마지막과 다음줄 시작까지 이어진 단어를 엄청 크게 표현해버린다
        //따라서 getClientRects를 사용(일반적인 상황에선 getClientRects()[0] = getBoundingClientRect())
        // var rect = range.getBoundingClientRect()
        var rect = range.getClientRects()[0]
        if (this.areaObj.areaType === this.areaObj.TA){
            this.localCoord = rect
            // console.log(rect)
        //CE는 mirror Div가 존재하지 않으며 targetArea 내부의 local coord를 구해야 한
        }else{

            var targetAreaStyle = window.getComputedStyle(this.areaObj.targetAreaElement)

            let targetAreaRect = this.areaObj.targetAreaElement.getBoundingClientRect()

            let customRect = {top : 0, left: 0, x:0, y : 0}
            let marginTop = parseInt(targetAreaStyle.marginTop.replace("px", ""))
            let marginLeft = parseInt(targetAreaStyle.marginLeft.replace("px", ""))
            customRect.top = rect.top + marginTop  - targetAreaRect.top
            customRect.left = rect.left + marginLeft - targetAreaRect.left
            customRect.x = rect.x - targetAreaRect.x
            customRect.y = rect.y - targetAreaRect.y
            customRect.width = rect.width
            customRect.height = rect.height

            this.localCoord = customRect
        }
    }


    //hanGramm this.areaObj.ghostPanel 위에서 존재하는 range Obj 의 상대위치에 존재하는 zone들임
    //최초 ghostErrZone의 생성과 이후에 일어나는 모든 갱신을 담당한다.
    makeGhostErrZone (scrollTop, scrollLeft){
        let that = this
        // console.log(this.areaObj)
        let rangeObj = this._rangeObj
        let basePanel = this.areaObj.ghostPanel
        let localCoord = this.localCoord
        let targetArea = this.areaObj.targetAreaElement
        let scrollHeight = targetArea.scrollHeight
        let clientHeight = targetArea.clientHeight

        scrollTop = scrollTop !== undefined ? scrollTop : targetArea.scrollTop
        scrollLeft = scrollLeft !== undefined ? scrollLeft : targetArea.scrollTop
        // console.log(scrollHeight, clientHeight, scrollTop)

        this.ghostErrWordZone = this.ghostErrWordZone ? this.ghostErrWordZone : document.createElement("div")
        // console.log(this.ghostErrWordZone,localCoord)
        this.ghostErrWordZone.classList.add("han_gramm_errZone_gst")
        this.ghostErrWordZone.classList.add("han_gramm_errZone_3F-Wk")
        this.ghostErrWordZone.classList.add("han_gramm_errZone_3mEyK")
        //아랫줄 두개임 다른것들과 차별화를 위
        this.ghostErrWordZone.classList.add("han_gramm_errZone__" + this.errWordZoneNumber)

        this.ghostErrWordZone.id = "han_gramm_errZone__" + this.errWordZoneNumber
        //scroll Top 만큼을 빼주면 scroll 이 있어도 위치를 바꿀수 있다
        this.ghostErrWordZone.style.top = localCoord.top - scrollTop + "px"
        this.ghostErrWordZone.style.left = localCoord.left - scrollLeft + "px"
        this.ghostErrWordZone.style.width = localCoord.width + "px"
        this.ghostErrWordZone.style.height = localCoord.height + "px"

        // console.log(this.ghostErrWordZone.parentElement, basePanel)
        if(this.ghostErrWordZone.parentElement !== basePanel){
            basePanel.append(this.ghostErrWordZone)
        }

        // errZone:before 를 클릭하면 안에있는 caret이 선택이 안되므로 caret을 선택할수 있게하는 이벤트
        this.ghostErrWordZone.addEventListener("mousedown", function(evt){
            //마우스나 엔터를 꾹 누르고 있다가 땠을경우 마지막 땟을때 absoluteStartPos를 반영 못하게 된다
            //따라서 startPos는 여기서 새로 불러옴
            let absoluteStartPos = that.absoluteStartPos
            let absoluteEndPos = that.absoluteEndPos

            //errZone 자체를 클릭하는건 prevent defaut를 해줘야 그 뒤에있는 단어 사이를 클릭할수 있
            evt.preventDefault()

            if(that.areaObj.areaType === that.areaObj.TA){
                let targetAreaElemPosition = that.areaObj.targetAreaElement.getBoundingClientRect()
                //TA에서는 targetAreaElem의 위치를 빼줘서 상대 위치를 구한
                let clickLeftLocal = evt.clientX - targetAreaElemPosition.left
                let clickTopLocal = evt.clientY - targetAreaElemPosition.top

                console.log(clickLeftLocal, clickTopLocal, localCoord)

                let clickLeftCoordInErrZone = clickLeftLocal - (localCoord.left - scrollLeft)

                //한글자여도 caret 위치는 2개가 된다 즉 글자수 + 1
                let possibleCaretNum = absoluteEndPos - absoluteStartPos

                let clickedLocalCaretPosNum = Math.round((clickLeftCoordInErrZone / localCoord.width) * possibleCaretNum)
                // console.log(absoluteStartPos , clickedLocalCaretPosNum, absoluteStartPos + clickedLocalCaretPosNum)

                // console.log(that.areaObj.targetAreaElement)
                that.areaObj.targetAreaElement.focus()
                that.areaObj.targetAreaElement.selectionStart = absoluteStartPos + clickedLocalCaretPosNum;
                that.areaObj.targetAreaElement.selectionEnd = absoluteStartPos + clickedLocalCaretPosNum;

            }else if(that.areaObj.areaType === that.areaObj.CE){
                let currentFrameAbsolutePosition = getCurrentFrameAbsolutePosition()

                let clickLeftLocal = evt.pageX - currentFrameAbsolutePosition.x
                let clickTopLocal = evt.pageY - currentFrameAbsolutePosition.y
                console.log(clickLeftLocal, clickTopLocal, localCoord)

                let clickLeftCoordInErrZone = clickLeftLocal - (localCoord.left - scrollLeft)

                //한글자여도 caret 위치는 2개가 된다 즉 글자수 + 1
                let possibleCaretNum = absoluteEndPos - absoluteStartPos

                // console.log(clickLeftCoordInErrZone, localCoord.width, absoluteEndPos - absoluteStartPos)
                // console.log((clickLeftCoordInErrZone / localCoord.width) * possibleCaretNum)

                let clickedLocalCaretPosNum = Math.round((clickLeftCoordInErrZone / localCoord.width) * possibleCaretNum)
                console.log(absoluteStartPos , clickedLocalCaretPosNum, absoluteStartPos + clickedLocalCaretPosNum)

                // console.log(that.areaObj.targetAreaElement)
                that.areaObj.targetAreaElement.focus()
                positionUtil.setCurrentCursorPosition(that.areaObj.targetAreaElement, absoluteStartPos + clickedLocalCaretPosNum)
            }
        })
    }

    //동일한 영역을 spellCheck로 보내면 range가 중복되는데 이를 막기위해
    // 3개의 parameter로 중복여부를 확인함
    checkDuplicated(commingStartPos, commingEndPos, commingErrWordOnly){
        // console.log(commingStartPos, commingEndPos, commingErrWordOnly, this)
        if(commingEndPos === this.absoluteEndPos &&
            commingStartPos === this.absoluteStartPos && commingErrWordOnly === this.errWordOnly){
            return true
        }else{
            return false
        }
    }
}

class CorrectionPart{
    static get MAKING(){
        return "making"
    }
    static get PARTCOMPLETED(){
        return "partCompleted"
    }
    static get MODIFYING(){
        return "modifying"
    }
    static get JOINNING(){
        return "joinning"
    }
    static get PROCESSING(){
        return "processing"
    }
    //복붙이 들어올 때를 대비
    constructor(start, end){
        this.start = start
        this._end = end
        this.partLength = this._end - this.start
        this.status = CorrectionPart.MAKING;
        //고유키 같은 개념으로 교정서버를 갔다온 값과 비교되는 값
        this.uuid = generateQuickGuid()
        //서버 송수신 문제가 발생했을때 이를 알아채기 위해 얼마나 처리시간이 지났는지 비교해보려는 시간
        this.processingStartTime = null
    }

    get end(){
        return this._end
    }

    set end(end){
        this._end = end
        // console.log(this._end, this.start)
        this.partLength = this._end - this.start
    }

    isUpperPartThanOldPosStart(oldPosStart){
        console.log(oldPosStart, this.end)
        // if (oldPosStart <=  this.end ){
        if (oldPosStart <=  this.end ){
            return this
        }
    }

    isLowerPartThanOldPosEnd(oldPosEnd){
        console.log(oldPosEnd, this.start)
        if(this.start <= oldPosEnd){
            return this
        }
    }
}

class HanGrammarArea{
    //그냥 constant 임 obfuscator가 static 을 인식못해서 이렇게 했음
    get CE(){
        return "contenteditable"
    }
    get TA(){
        return "textarea"
    }

    constructor(targetAreaId){

        // frame 하나가 등록되고 그 frame안에 복수의 area 를 등록할 수 있다 이것이 area를 특정하는 id임
        this.targetAreaId = targetAreaId;
        this.targetAreaElement = document.querySelector('[_han_gramm_id="' +this.targetAreaId + '"]' );
        this.areaType = this.targetAreaElement &&
        (this.targetAreaElement.tagName.toLowerCase() === "textarea" ||
            this.targetAreaElement.type === "textArea") ? this.TA : this.CE

        //이걸 하나씩 늘리면서 errZone의 id값으로 저장이 된다.
        this.errWordZoneNumber = 0
        // errZoneElem이 지정되면 해당 element에 id값으로 number가 들어가는데 candidateWords 안에 이에 동일한 key 를 생성하고
        // candidate 내용이 value로 들어간다
        this.candidateContents= {};
        this.lengthPosInfo = {oldPosStart : 0, oldPosEnd : 0, newPosStart : 0, newPosEnd : 0, oldWholeLength: 0, newWholeLength : 0 ,lastKeyCode: 0, modifying : false, lastModifiedTime: 0, ctrlKey : false, altKey: false};
        this.rangeInfoStorage = {};
        this.registeringCommonHandler();
        this.pointedErrZone = null
        this.correctionPartInfoStorage = []
        this.initCorrectionTimeWorker()
        // this.manageCorrectionPart()
    }

    //correctionPart와 init~ Worker 를 도입한 뒤로 필요없어졌음 필요없으면 지울 것
    registeringCommonHandler() {
        let that = this;
        let targetAreaElement = this.targetAreaElement;
        //common
        targetAreaElement.addEventListener("keyup", function(evt){
            // #일단 엔터키에 반응하도록
            if(evt.keyCode ===13 && evt.target === that.targetAreaElement){
                // console.log(evt.keyCode);
                // console.log(evt.target);
                let correctionPartInfoStorage = that.correctionPartInfoStorage
                // for (let part= 0 ;part < correctionPartInfoStorage.length ; part++){
                //     let correctionPart = correctionPartInfoStorage[part]
                //     if(correctionPart.status === CorrectionPart.MODIFYING ||
                //         correctionPart.status === CorrectionPart.MAKING){
                //         console.log("observeArea")
                //         that.observeArea(correctionPart)
                //     }
                // }
                // that.observeArea(evt.target)
            }
        });
    }

    observeArea(correctionPart){
        if (!processActivated) return
        let correctionStart = correctionPart.start
        let correctionEnd = correctionPart.end
        let correctionPartUuid = correctionPart.uuid
        let that = this;

        let contentOnly = this.getContentInRange(correctionStart, correctionEnd)
        // console.log(contentOnly)
        //CE의 경우 newLine이 고려되지 않은 start와 end 이다
        let contentRange = {startPos : correctionStart, endPos :correctionEnd};

        // console.log(contentOnly, contentOnly.length);
        setTimeout(function(){
            framePort.postMessage({type : "areaContent" ,targetAreaId : that.targetAreaId,
                content : contentOnly , "contentRange" : contentRange, correctionPartUuid : correctionPartUuid});
            that.collecting = false
        }, 0);
    }


    getContentInRange (){

        if (this.areaType === this.CE ) {


        }else if(this.areaType === this.TA){

        }
    }

    //영어로된 것은 무시한다.
    checkAlphabetOnlyWord(errWordOnly, errWordOnlyLength){
        let  matched = errWordOnly.match(/[A-Za-z ]+/)
        // console.log(matched)
        if(matched === null){
            return false
        }else{
            let matchedLength = matched[0].length
            if (matchedLength === errWordOnlyLength) {
                return true
            }else{
                return false
            }
        }

    }
    //잡코리아 위주의 response data 처리를 하지만 되도록 common이 될 수 있도록
    //CE와 TA 가 각각 overriding
    processJobKoreaSpellCheckedData(receivedData){

    }

    preProcessSaramInSpellCheckedData(receivedData){
        let checkedData = receivedData.checkedData
        //jobKorea와 맞추려고 이렇게 key 값을 바꿈
        checkedData["WordCandidateList"] = checkedData["word_list"]
        delete checkedData["word_list"]

        let candidateList = checkedData.WordCandidateList

        for(let i = 0; i < candidateList.length; i++){
            let candidateWordList = {}
            let helpMessageDivided = []
            let childrenMcontentId = 0

            //jobKorea 형에 맞게 이름을 변경
            candidateList[i]["candidateWordList"] = candidateList[i].candWordList.split(",")
            delete candidateList[i].candWordList

            let helpMessage = candidateList[i].helpMessage
            helpMessage = helpMessage.replace(/\n/ig, "<br>")

            helpMessageDivided = helpMessage.split("<br>")

            let helpMessageSubList = helpMessageDivided.splice(1)
            let helpMessageSubListContent = helpMessageSubList.join("\n")
            // console.log([helpMessageDivided[0]], helpMessageSubListContent)
            let initial = [helpMessageDivided[0]]
            candidateList[i]["helpMessageDivided"] = initial.concat(helpMessageSubListContent)
                // .push(helpMessageSubListContent)
        }
        // console.log(checkedData)
    }

    //jobKorea 에서 맞춤법 검사가 된 데이터중 WordInfo의 mcontent 중 중복해서 span으로 감싼부분이 중복되어 있는 경우가 있음
    //그럴대에만 쓰는 용도로 다른 checker의 경우 쓸 필요는 없을듯
    preProcessJobKoreaSpellCheckedData(checkedData){
        // console.log(checkedData)
        let mcontent = checkedData.WordInfo.mcontent;
        let elem = document.createElement("div");
        elem.innerHTML = mcontent;
        // console.log(elem)
        for (let i = 0; i < elem.childElementCount ; i++){
            let child = elem.children[i];
            // console.log(child)
            child.innerHTML = child.innerText
        }
        checkedData.WordInfo.mcontent = elem.innerHTML;
        return checkedData
    }

    //jobKorea의 candidate Content가 html string으로 되어있으니 이를 나중에 쓸 수있게 candidateWord부분을 list로 바꿈
    preProcessJobKoreaCandidateContents(candidateContent){

        for (let i=0 ; i < candidateContent.length ; i++) {
            let candidateWordList = []

            // <span id="0_0">규암아</span><br/><span id="0_1">규산아</span> <- 이런식으로 되어 있는
            // candidate word 들을 list화함
            let dummy = document.createElement("div");
            dummy.innerHTML = candidateContent[i].candidateWord;

            for (let j = 0; j < dummy.childElementCount; j++) {
                if (dummy.children[j].tagName.toLowerCase() === "span") {
                    candidateWordList.push(dummy.children[j].innerText)
                }
            }
            candidateContent[i].candidateWordList = candidateWordList;

            //helpMessage가  "메인 문장ex(철자 검사를 해 보니 이 어절은 분석할 수 없으므로 틀린 말로 판단하였습니다.)"
            // 이 나오고 <br><br> 이후에 세부내용이 옴 메인문장 만을 노출시키고 나무지는 클릭하면 펼쳐지는걸로
            let helpMessage = candidateContent[i].helpMessage;
            let helpMessageDivided = []

            //br2개씩 있는거 없애주기
            helpMessage = helpMessage.replace(/<br><br>/g, "<br>");
            helpMessage = helpMessage.replace(/<br\/><br\/>/, "<br>");

            dummy = document.createElement("div");
            dummy.innerHTML = helpMessage;

            //메인문장과 부가문장을 따로 array로 넣어줌
            helpMessageDivided.push(dummy.firstChild.data);
            helpMessageDivided.push(helpMessage.split(dummy.firstChild.data)[1])

            // console.log(helpMessageDivided)

            candidateContent[i].helpMessageDivided = helpMessageDivided;


        }
    }


    //단순히 createRangeHG warapper 함수임
    createFinalRangeHG(node, startCount, endCount){
        // console.log(startCount,endCount)
        let chars = {count : startCount, end: false }
        let range = this.createRangeHG(node, chars, true);
        chars = {count : endCount, end: false }
        let finalRange = this.createRangeHG(node, chars, false, range);

        return finalRange
    }

    //errWord의 위치와 길이에 맞는 range객체를 만듬
    //chars.count는 무조건 reference값이 들어가야 되 때문에 Object를 받아야 함
    createRangeHG (node, chars, isSettingStart ,range) {

        //range가 들어오지 않을때는 최초의 range를 하나 만듬
        if (!range) {
            range = document.createRange();
            range.selectNode(node);
            // range.setStart(node, 0);
        }

        if (chars.count === 0) {
            if (isSettingStart){

                //첫줄부터 글을 쓰는게 아니라 시작부터 아랫줄로 몇칸 내리고 시작하여 첫단어가 errWord일경우
                //errZone이 만들어 지고 원래 errWord가 지워질때 아랫줄로 내린 칸까지의 모든 빈칸까지 range에 포함된다
                // 따라서 아래 내린칸전의 모든 칸이 지행워짐
                //이러한 상황에는 이처럼 range의 start node를 thia.targetAreaElement가 아니라 내린칸의 errWord가
                //속한 element로 해줘야 한다 즉  errWord text의 parent
                // console.log(node, chars.count)
                if (node === this.targetAreaElement){
                    let firstTextNode = positionUtil.findFirstTextNode(node)
                    node = firstTextNode.parentNode
                    // console.log(firstTextNode, node)
                    range.setStart(node, chars.count)
                    chars.end = true

                }else{
                    if (node.nodeType === Node.TEXT_NODE ) {

                        if (node.nodeValue == ""){
                            return
                        }

                        // console.log(node,node.nodeValue,chars.count)
                        range.setStart(node, chars.count)

                        chars.end = true

                    }else{
                        // console.log(node,chars.count)
                        for (var lp = 0; lp < node.childNodes.length; lp++) {
                            range = this.createRangeHG(node.childNodes[lp], chars, isSettingStart, range);

                            if (chars.end === true) {
                                break;
                            }
                        }
                    }
                    // console.log(node,chars.count)
                    // if (node.nodeType === Node.TEXT_NODE) {
                    //     range.setStart(node, chars.count)
                    //
                    // }else{
                    //     return
                    // }
                }

                // chars.end = true
                // return

                // let suspectedTextNode = positionUtil.findFirstTextNode(node)
                // console.log(suspectedTextNode)
                //
                // if (suspectedTextNode !== undefined && suspectedTextNode.nodeType === Node.TEXT_NODE){
                //
                //     node = suspectedTextNode
                //     range.setStart(node, chars.count)
                // }else{
                //     return
                // }


            }else{
                range.setEnd(node, chars.count);
                chars.end = true
            }

        } else if (node && chars.count >0) {
            // console.log(node, chars.count)
            if (node.nodeType === Node.TEXT_NODE) {
                //여기가 핵심임 재귀로 순회된 node의 성향이 text라면 count를 깎거나 setStart나 end로 마킹함
                if (node.textContent.length < chars.count) {
                    chars.count -= node.textContent.length;
                } else {
                    // console.log(isSettingStart,node,chars.count)
                    if (isSettingStart){
                        //만약 해당 노드의 마지막으로 range start가 되어 버리면  다음 빈칸이 모두 지워지는 상황이 발생한다
                        //따라서 count를 0으로 만들고 다음node의 첫번째를 찾기위해 진행
                        if (node.nodeValue.length === chars.count){
                            // console.log(node.nodeValue , chars.count);
                            chars.count = 0;
                            return
                        }else{
                            range.setStart(node, chars.count);
                        }
                    }else{
                        // console.log(node,chars.count);
                        range.setEnd(node, chars.count);
                    }
                    chars.end = true;
                }
                //텍스트 노드가 아니면 자식순회
            } else {
                for (var lp = 0; lp < node.childNodes.length; lp++) {
                    range = this.createRangeHG(node.childNodes[lp], chars, isSettingStart, range);

                    if (chars.end === true) {
                        break;
                    }
                }
            }
        }
        return range;
    }
}

class HanGrammarAreaCE extends HanGrammarArea{
    constructor(targetAreaId){
        super(targetAreaId)
        //CE에만 있는 Iframe을 찾아서 넣는다 이는 ghostMirror의 크기를 정할때 쓰임
        //contentEditable targetArea는  실제 입력창의 크기를 반영 못할때가 종종 있음 (ex. daum메일)
        this.CEclosestIframe = window.frameElement
        //CE의경우 CEclosestIframe을 일단 찾아보고 ghostPanel을만들어야 함.
        this.ghostPanel = this.makeHanGrammGhost()
        //attribute는 소문자화 됨
        this.hanGrammarButton = this.ghostPanel.closest("hangrammar-ghost").querySelector("[hangrammar-part=button]").firstElementChild
        this.registeringHandlerCE()
        this.initButtonWorker()
        //correction 요청을 한 마지막 문장
        this.lastRequestedContent = this.targetAreaElement.innerText
    }

    initButtonWorker(){
        let that = this
        let button = this.hanGrammarButton
        //CE든 TA 든 실제 영향을 미치는 것만 lastModifiedTime에 들어가 있다
        let lastModifiedTime = this.lengthPosInfo.lastModifiedTime;
        // button.classList.add("han-gramm-loader")


        setTimeout(function doButtonWork(){
            var count = Object.keys(that.candidateContents).length
            if (that.targetAreaElement.textContent.length ===0 ){
                button.innerText = new String(count)
                if (button.classList.contains("han_gramm_loader")){
                    button.classList.remove("han_gramm_loader")
                }
            }else{
                button.innerText = new String(count)
                let currentTime = new Date().getTime()
                if(currentTime - that.lengthPosInfo.lastModifiedTime > 2000){

                    if (button.classList.contains("han_gramm_loader")) {
                        // console.log(currentTime - that.lengthPosInfo.lastModifiedTime)
                        button.classList.remove("han_gramm_loader")
                    }

                }else{
                    if(!button.classList.contains("han_gramm_loader")){
                        button.classList.add("han_gramm_loader")
                    }
                }
            }
            setTimeout(doButtonWork, 1000)
        }, 1000 )
    }

    initCorrectionTimeWorker(){
        let that = this;
        let caretLengthPos = this.lengthPosInfo

        setTimeout(function timeCorrector(){
            let currentTime = new Date().getTime()
            if((currentTime - caretLengthPos.lastModifiedTime > 3000 &&
                Math.abs(that.targetAreaElement.innerText.trim().length - that.lastRequestedContent.trim().length) > 5)){
                //끝을 지웠을때 마지막 request에 포함된 내용이면 request를 하지 않는다
                // if(that.targetAreaElement.innerText.trim().length - that.lastRequestedContent.trim().length < 0 &&
                //     that.lastRequestedContent.trim().indexOf(that.targetAreaElement.innerText.trim()) !== -1){
                //     console.log("return! timeworker")
                //     return
                // }
                let correctionPart = that.manageCorrectionPart()
                that.observeArea(correctionPart)
                that.lastRequestedContent = that.targetAreaElement.innerText
            }
            setTimeout(timeCorrector, 1000)
        },  1000)
    }

    initCorrectionTypingWorker(){
        let that = this;
        let caretLengthPos = this.lengthPosInfo

        let delayingVal = 0
        if(Math.abs(that.targetAreaElement.innerText.trim().length > 5 &&
            that.targetAreaElement.innerText.trim().length - that.lastRequestedContent.trim().length) > 10){
            // if(that.targetAreaElement.innerText.trim().length - that.lastRequestedContent.trim().length < 0 &&
            //     that.lastRequestedContent.trim().indexOf(that.targetAreaElement.innerText.trim()) !== -1){
            //     console.log("return! typingworker")
            //     return
            // }
            let correctionPart = that.manageCorrectionPart()
            that.observeArea(correctionPart)
            that.lastRequestedContent = that.targetAreaElement.innerText
        }
    }

    //correctionPart를 자잘하게 나누는걸 포기한 이후로 주석 처리함
    // initCorrectionTimeWorker(){
    //     let correctionPartInfoStorage = this.correctionPartInfoStorage
    //     let that = this;
    //     let caretLengthPos = this.lengthPosInfo
    //
    //
    //     setTimeout(function timeCorrector(){
    //         let delayingVal = 0
    //         for (let part = 0; part < correctionPartInfoStorage.length; part++){
    //             let correctionPart = correctionPartInfoStorage[part]
    //             let currentTime = new Date().getTime()
    //             if(correctionPart.status === CorrectionPart.MAKING || correctionPart.status === CorrectionPart.MODIFYING ){
    //
    //                 //partlength 가 10 이상인 상황에서 3초 이상입력이 없으면 processing 진행
    //                 // console.log(currentTime - caretLengthPos.lastModifiedTime)
    //                 if((currentTime - caretLengthPos.lastModifiedTime > 3000 && correctionPart.partLength > 10)){
    //                     //너무많은 요청이 있을때 각 요청을 1초씩 차이를 둔다
    //                     setTimeout(function(){
    //                         that.observeArea(correctionPart)
    //                     }, 1000 * delayingVal)
    //                     delayingVal++
    //                 }
    //             }
    //             //아주 잠깐의 시간동안 MODIFYING으로 바뀐뒤에 다음 setTimeout call 때 곧바로 다시 요청이 들어간다
    //             if(correctionPart.status === CorrectionPart.PROCESSING &&
    //                 correctionPart.processingStartTime !== null &&
    //                 currentTime - correctionPart.processingStartTime > 9000){
    //                 correctionPart.status = CorrectionPart.MODIFYING
    //                 correctionPart.processingStartTime = null
    //             }
    //         }
    //         setTimeout(timeCorrector, 1000)
    //     }, 1000)
    // }
    //
    // initCorrectionTypingWorker(){
    //     let correctionPartInfoStorage = this.correctionPartInfoStorage
    //     let that = this;
    //     let caretLengthPos = this.lengthPosInfo
    //
    //
    //     let delayingVal = 0
    //     for (let part = 0; part < correctionPartInfoStorage.length; part++){
    //         let correctionPart = correctionPartInfoStorage[part]
    //         if(correctionPart.status === CorrectionPart.MAKING || correctionPart.status === CorrectionPart.MODIFYING ){
    //             // console.log(currentTime - caretLengthPos.lastModifiedTime)
    //             if(correctionPart.partLength > 15){
    //                 //너무많은 요청이 있을때 각 요청을 0.1초씩 차이를 둔다
    //                 setTimeout(function(){
    //                     that.observeArea(correctionPart)
    //                 }, 500 * delayingVal)
    //                 delayingVal++
    //
    //             }
    //         }
    //     }
    // }

    killAll() {
        //rangeInfoStorage에서 있을지 모를임 ghostErrZone과 candidate rangeObj들을 모두 삭제한다
        // 기타 ghost mirror 존 등등은 나중에 켰을때 사용하기 위해 삭제하지 않는다.
        for (let i in this.rangeInfoStorage) {

            if (this.rangeInfoStorage.hasOwnProperty(i)) {
                this.rangeInfoStorage[i].ghostErrWordZone.remove()
                delete this.rangeInfoStorage[i]
                delete this.candidateContents[i]
            }
        }
        //button을 삭제해 준다.
        this.hanGrammarButton.style.display= "none";
    }

    reviveAll(){
        this.hanGrammarButton.style.display = ""
    }

    //기존에 세부적으로 관리하던 correctionPart를 폐기
    manageCorrectionPart(){
        //newLine이 고려되지 않은 length임
        return new CorrectionPart(0, this.targetAreaElement.textContent.length)
    }


    //correctionPart를 자잘하게 나누는걸 포기한 이후로 주석 처리
    //여기서 문장의 길이나 진행에 따라 correction Part를 만들고  init~Worker 들이 이를 활용하여 observeArea를
    //호춣한다
    //교정을 했을때는 변화의 정도를 추적하는 differencePos 를 인자로 넣어준다
   // manageCorrectionPart(differencePos){
   //      let that = this
   //      let caretLengthPosInfo =this.lengthPosInfo
   //      let correctionPartInfoStorage = this.correctionPartInfoStorage
   //
   //      let oldPosStart = caretLengthPosInfo.oldPosStart
   //      let oldPosEnd = caretLengthPosInfo.oldPosEnd
   //      let newPosStart = caretLengthPosInfo.newPosStart
   //      let newPosEnd = caretLengthPosInfo.newPosEnd
   //      let oldWholeLength = caretLengthPosInfo.oldWholeLength
   //      let newWholeLength = caretLengthPosInfo.newWholeLength
   //      // console.log(caretLengthPosInfo)
   //      differencePos = differencePos !== undefined ? differencePos : caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength
   //
   //      console.log(differencePos)
   //      //현재 typing이나 드래그 등이 어느 part에 결부되어있는지 찾아낸다 하나 또는 그 이상(드래그, 중간)일수 있음
   //      function findCorrectionPart(){
   //          let correctionParts = []
   //          // console.log(correctionPartInfoStorage.length)
   //          for (let part= 0 ;part < correctionPartInfoStorage.length ; part++){
   //              let correctionPart = correctionPartInfoStorage[part]
   //
   //              if(correctionParts.length === 0 && correctionPart.status  === CorrectionPart.MAKING &&
   //                  correctionPartInfoStorage.length > 1){
   //                  correctionParts.push(correctionPart)
   //              }else{
   //                  //개별 파트를 oldPosStart와 oldPosEnd로 측량해서  범위안에(붙여넣기 일때)
   //                  let upper = correctionPart.isUpperPartThanOldPosStart(oldPosStart)
   //                  // console.log(upper)
   //                  let lower = correctionPart.isLowerPartThanOldPosEnd(oldPosEnd)
   //                  // console.log(lower)
   //                  if(upper === lower){
   //                      correctionParts.push(upper)
   //                  }
   //              }
   //          }
   //          return correctionParts
   //      }
   //
   //      function adjustCorrectionParts(){
   //          // console.log(caretLengthPosInfo)
   //          for(let part= 0 ; part < correctionPartInfoStorage.length ; part++){
   //              let correctionPart = correctionPartInfoStorage[part]
   //              // console.log(correctionPart,caretLengthPosInfo ,differencePos)
   //              //part 내부에서 일어난 일
   //              if (correctionPart.start <= oldPosStart && oldPosEnd <= correctionPart.end){
   //                  //part 사이의 공통공간 조작시 오른쪽은 start도 영향을 받아야함 첫번째 part시작은 제외
   //                  if(correctionPart.start === oldPosEnd && correctionPart.start !== 0){
   //                      correctionPart.start += differencePos
   //                  }
   //                  //이 조건은 사실 필요없을것 같은데 확실해지면 지울것
   //                  if(correctionPart.status === CorrectionPart.PROCESSING && false){
   //                      continue
   //                  }else if(correctionPart.status === CorrectionPart.PARTCOMPLETED){
   //                      correctionPart.status = CorrectionPart.MODIFYING
   //                      correctionPart.end += differencePos
   //                  }else{
   //                      //making 상황일때는 오직 PROCESSING을 거쳐 PARTCOMPLETED로 갈 뿐이다
   //                      correctionPart.end += differencePos
   //                  }
   //                  //oldPosEnd가 임의의 part보다 앞에 있을때는 part 전체 통째로 움직인다
   //              }else if(oldPosEnd <= correctionPart.start) {
   //                  correctionPart.start += differencePos
   //                  correctionPart.end += differencePos
   //
   //                  //part내부에서 일어난 일과 정반대의 상황으로 3개 이상의 part가 결부되어 있을경우
   //                  //가운데에 낀 part상황임 혹은 절묘하게 part의 start와 end만 select 된후 일어나는 상황
   //              }
   //              //위의 과정을 거친후 해당 part의 partLength가 0이되는 순간 지워준다
   //              if(correctionPart.partLength === 0 ){
   //                  // console.log("partLength = 0")
   //                  //만약 맨끝 part를 지우는게 아니라 처음 또는 중간을 지워버리면 for문의 다음part를 건너뛰어버림
   //                  //그것 조정
   //                  let needPartNumAdjust =false
   //                  if(part !== correctionPartInfoStorage.length - 1) needPartNumAdjust = true
   //                  correctionPartInfoStorage.splice(part,1)
   //
   //                  if (needPartNumAdjust) part--
   //
   //                  continue
   //              }
   //              // console.log(correctionPart,caretLengthPosInfo ,differencePos)
   //          }
   //      }
   //
   //     function sliceLongCorrectionPart(correctionPart, partNum){
   //         let wholeContent = that.targetAreaElement.textContent
   //         let miniPartLength = 50
   //         let longCorrectionPartAbsoluteStart = correctionPart.start
   //         let longCorrectionPartAbsoluteEnd = correctionPart.end
   //
   //         let longCorrectionPartLength = correctionPart.partLength
   //
   //         let dividedPartLength = Math.ceil(longCorrectionPartLength / miniPartLength)
   //
   //         let dividedPartArray = []
   //         while(true){
   //             //애초에 length 100 이상인 것들만 넘어오므로 최초에는 무조건 50이 넘는다
   //             if(longCorrectionPartLength >= 50){
   //                 if(dividedPartArray.length ===0){
   //                     //최초 miniPart의 absoluteStart는 longCorrectionPartAbsoluteStart의 start와 같
   //                     let miniPartAbsoluteStart = longCorrectionPartAbsoluteStart
   //                     let tempMiniPartAbsoluteEnd = longCorrectionPartAbsoluteStart + miniPartLength
   //                     //일단 50까지 잘라낸것
   //                     let tempMiniPartContent = wholeContent.substring(miniPartAbsoluteStart, tempMiniPartAbsoluteEnd)
   //                     //50까지 끊은것에서 뒤로 계속 가서 " " 이나 \n 이 나오는 lastindex를 구함
   //                     let blankLastIndex = tempMiniPartContent.lastIndexOf(" ", miniPartLength)
   //                     let newLineLastIndex = tempMiniPartContent.lastIndexOf("\n", miniPartLength)
   //
   //                     let miniPartAbsoluteEnd = 0
   //                     if(blankLastIndex === -1 && newLineLastIndex === -1){
   //                         miniPartAbsoluteEnd = tempMiniPartAbsoluteEnd
   //                     }else{
   //                         miniPartAbsoluteEnd = blankLastIndex >= newLineLastIndex ?
   //                             miniPartAbsoluteStart + blankLastIndex : miniPartAbsoluteStart +newLineLastIndex
   //                     }
   //                     let miniCorrectionPart = new CorrectionPart(miniPartAbsoluteStart, miniPartAbsoluteEnd)
   //                     dividedPartArray.push(miniCorrectionPart)
   //                     longCorrectionPartLength -= miniCorrectionPart.partLength
   //
   //                 }else{
   //                     //두번째 miniPart의 start는 이전 miniPart의 end와 같다.
   //                     let miniPartAbsoluteStart = dividedPartArray[dividedPartArray.length - 1].end
   //                     let tempMiniPartAbsoluteEnd = miniPartAbsoluteStart + miniPartLength
   //                     //일단 이전 end 부터 50까지 끊는다.
   //                     let tempMiniPartContent = wholeContent.substring(miniPartAbsoluteStart, tempMiniPartAbsoluteEnd)
   //                     //50까지 끊은것에서 뒤로 계속 가서 " " 이나 \n 이 나오는 lastindex를 구함
   //                     let blankLastIndex = tempMiniPartContent.lastIndexOf(" ", miniPartLength)
   //                     let newLineLastIndex = tempMiniPartContent.lastIndexOf("\n", miniPartLength)
   //
   //                     let miniPartAbsoluteEnd = 0
   //                     if(blankLastIndex === -1 && newLineLastIndex === -1){
   //                         miniPartAbsoluteEnd = tempMiniPartAbsoluteEnd
   //                     }else{
   //                         miniPartAbsoluteEnd = blankLastIndex >= newLineLastIndex ?
   //                             miniPartAbsoluteStart + blankLastIndex : miniPartAbsoluteStart +newLineLastIndex
   //                     }
   //                     let miniCorrectionPart = new CorrectionPart(miniPartAbsoluteStart, miniPartAbsoluteEnd)
   //                     dividedPartArray.push(miniCorrectionPart)
   //                     longCorrectionPartLength -= miniCorrectionPart.partLength
   //                 }
   //
   //
   //             }else{
   //                 dividedPartArray.push(new CorrectionPart(dividedPartArray[dividedPartArray.length - 1].end, longCorrectionPartAbsoluteEnd))
   //                 break;
   //             }
   //         }
   //
   //         return dividedPartArray
   //     }
   //
   //
   //      //현재 입력이 이루어진(그냥 한글자던 범위선택후 조작이던간에) correctionPart(하나 또는 둘 이살일수 있다)
   //      let associatedCorrectionParts = findCorrectionPart()
   //      // console.log(associatedCorrectionParts)
   //      // console.log(associatedCorrectionParts)
   //      console.log(associatedCorrectionParts)
   //      if(associatedCorrectionParts.length === 0){
   //
   //          this.correctionPartInfoStorage.push(new CorrectionPart(0, differencePos))
   //      }else if(associatedCorrectionParts.length === 1){ //결부된 part가 하나가 있을때 (단순타이핑)
   //          // console.log("one")
   //          //현재 processing 혹은 partcompleted 이라면 그리고 그것이 현재 마지막 part라면 새로은 part를 만듬
   //          if((associatedCorrectionParts[0].status === CorrectionPart.PROCESSING ||
   //              associatedCorrectionParts[0].status === CorrectionPart.PARTCOMPLETED)&&
   //              associatedCorrectionParts[0] === correctionPartInfoStorage[correctionPartInfoStorage.length -1] &&
   //              associatedCorrectionParts[0].end <= oldPosStart){
   //              // console.log(newPosEnd)
   //
   //              if(differencePos > 0){
   //                  //일단 linux냐 window냐에 따라서 한글이 초성과 중성만 만 쳤을때 caret위치가 어디냐가 차이가 있다 따라서
   //                  //이에 차이를 둠
   //                  this.correctionPartInfoStorage.push(new CorrectionPart(associatedCorrectionParts[0].end , oldPosStart + differencePos))
   //              }else{
   //                  // minus 일때는 새로운 part를 생성하지 않는다
   //                  adjustCorrectionParts()
   //              }
   //
   //          }else{
   //              adjustCorrectionParts()
   //          }
   //
   //      }else{
   //          //결부된 part가 두개 이상일 경우
   //          // console.log다("associatedCorrectionParts")
   //          // part와 part사이 단일 caret이 위치한 상태중 공통부분에서 추가나 삭제있을시 처리
   //          // 즉 드래그 상태가 아님
   //          if (oldPosStart === oldPosEnd){
   //              adjustCorrectionParts()
   //
   //          //특정한 correctionPart의 끝자락(다음 part와의 공통공간) 에 oldPosEnd가 있는상황하에서
   //          //교정버튼을 눌렀을경우 위 어느 조건에도 들어가지 않는다 따라서 해당조건 추가
   //          }else if(oldPosStart === newPosStart && newPosEnd !== newPosStart){
   //              adjustCorrectionParts()
   //
   //          //합치는 알고리즘 splice를 쓰면 될듯. 그리고 adjust시킴
   //          // 드래그로 여러 part가 선택 되었을때 + 특정 키를 눌렀을때 드래그 된게 없어지면서 새로운 내용이 들어가는상황
   //          // oldPosStart: 0, oldPosEnd: 33, newPosStart: 1, newPosEnd: 1,
   //          //두개의 part가 선택되었을때 특정키를 눌른 상태 아래 조건에 들어가야함
   //
   //          //ctrl+a 를 눌렀을때 oldPosStart: 0, oldPosEnd: 33, newPosStart:0 , newPosEnd: 33, 인 상황은
   //          //(selectionChange event keyup keydown 모든 이벤트를 거치기에 여기까지 온다)
   //          //아래 조건에 들어가면 안되기 때문에 newPosEnd === newPosStart 조건을 집어넣
   //          }else if(newPosEnd === newPosStart) {
   //              //우선 여러 파트에 selection이 거쳐져 있으므로 첫번째 part의 start와 마지막 part의 end로
   //              // 새로운 part를 만들고 splice로 예전것들을 제거한뒤 새로운걸 그자리에 집어넣는다
   //              let joinedPartStart = associatedCorrectionParts[0].start
   //              let joinedPartEnd = associatedCorrectionParts[associatedCorrectionParts.length - 1].end
   //              let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
   //
   //              for (let i = 0; i < correctionPartInfoStorage.length; i++) {
   //                  let correctionPart = correctionPartInfoStorage[i];
   //                  if (correctionPart === associatedCorrectionParts[0]) {
   //                      correctionPartInfoStorage.splice(i, associatedCorrectionParts.length, joinedPart)
   //                      break;
   //                  }
   //              }
   //              adjustCorrectionParts()
   //
   //
   //          }
   //      }
   //
   //      //나누어져 있던 part가 띄여쓰기를 제거하고 서로 이어붙이기를 했을때
   //      // correctionPart 둘을 이어붙여 하나로 만든다 backspace든 delete키든 둘 모두
   //      if( (associatedCorrectionParts.length === 1 && oldPosStart === oldPosEnd) ||
   //         (associatedCorrectionParts.length === 2 && oldPosStart === oldPosEnd) ){
   //
   //         let allContent = this.targetAreaElement.textContent
   //         // backspace 키를 눌러서 합쳤을때 + correctionPart가 하나밖에 없을때는 적용하지 않고(beforeJoinedPart를 구할수 없다),
   //         // 역시 마지막 남은 글자를 지웠을때 위에서(adjustCorrectionParts()) correctionPartInfoStorage
   //         // 를 0으로 만들므로 오류가 난다 따라서 이때도 적용안되게 함
   //         if (caretLengthPosInfo.lastKeyCode === 8 &&
   //             !(associatedCorrectionParts[0] === correctionPartInfoStorage[0] &&
   //                 associatedCorrectionParts.length === 1) &&
   //             correctionPartInfoStorage.length !== 0){
   //
   //             let afterJoinedPart = associatedCorrectionParts[associatedCorrectionParts.length - 1]
   //
   //             let indexOfAfterJoinedPart = 0
   //             for(let i = 0; i < correctionPartInfoStorage.length; i++){
   //                 if (afterJoinedPart == correctionPartInfoStorage[i]){
   //                     indexOfAfterJoinedPart = i
   //                     break
   //                 }
   //             }
   //             let beforeJoinedPart = correctionPartInfoStorage[indexOfAfterJoinedPart - 1]
   //
   //             if (allContent.charAt(beforeJoinedPart.end - 1) !== " " &&
   //                 allContent.charAt(beforeJoinedPart.end - 1) !== "\n"){
   //                 if(allContent.charAt(afterJoinedPart.start) !== " " &&
   //                     allContent.charAt(afterJoinedPart.start) !== "\n"){
   //                     let joinedPartStart = beforeJoinedPart.start
   //                     let joinedPartEnd = afterJoinedPart.end
   //                     let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
   //
   //                     correctionPartInfoStorage.splice(indexOfAfterJoinedPart - 1, 2, joinedPart)
   //                 }
   //             }
   //         //del 키를 눌러서 합쳤을때 + correctionPart가 마지막 것 일때는 적용하지 않고 (afterJoinedPart를 구할수 없으므로),
   //         // 역시 마지막글자를 del 키로 지웠을때 위에서(adjustCorrectionParts()) correctionPartInfoStorage
   //         // 를 0으로 만들므로 오류가 난다 따라서 이때도 적용안되게 함
   //         }else if(caretLengthPosInfo.lastKeyCode === 46 &&
   //             !(associatedCorrectionParts[0] === correctionPartInfoStorage[correctionPartInfoStorage.length - 1] &&
   //                 associatedCorrectionParts.length === 1) &&
   //             correctionPartInfoStorage.length !== 0){
   //
   //             let beforeJoinedPart = associatedCorrectionParts[0]
   //
   //             let indexOfBeforeJoinedPart = 0
   //             for(let i = 0; i < correctionPartInfoStorage.length; i++){
   //                 if (beforeJoinedPart == correctionPartInfoStorage[i]){
   //                     indexOfBeforeJoinedPart = i
   //                     break
   //                 }
   //             }
   //             let afterJoinedPart = correctionPartInfoStorage[indexOfBeforeJoinedPart + 1]
   //
   //             if (allContent.charAt(beforeJoinedPart.end - 1) !== " " &&
   //                 allContent.charAt(beforeJoinedPart.end - 1) !== "\n"){
   //                 if(allContent.charAt(afterJoinedPart.start) !== " " &&
   //                     allContent.charAt(afterJoinedPart.start) !== "\n"){
   //                     let joinedPartStart = beforeJoinedPart.start
   //                     let joinedPartEnd = afterJoinedPart.end
   //                     let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
   //
   //                     correctionPartInfoStorage.splice(indexOfBeforeJoinedPart , 2, joinedPart)
   //                 }
   //             }
   //         }
   //      }
   //
   //      //Length 가 100이 넘는 longCorrectionPart 를 50Length 작은 단위의 miniCorrectionPart들로 나눈다
   //      for(let i = 0; i < correctionPartInfoStorage.length; i++){
   //         let correctionPart = correctionPartInfoStorage[i]
   //         if (correctionPart.partLength > 100){
   //             //분리된 part를 받아온뒤 longCorrectionPart를 지우고 거기에 모두 넣는다.
   //             let dividedMiniPartArray = sliceLongCorrectionPart(correctionPart, i)
   //             console.log(dividedMiniPartArray)
   //             //... 은 python의 args나 kargs같은 개념인듯
   //             correctionPartInfoStorage.splice(i, 1, ...dividedMiniPartArray)
   //         }
   //      }
   //
   //      // console.log(correctionPartInfoStorage)
   //     for(let i = 0; i < correctionPartInfoStorage.length; i++){
   //         let correctionPart = correctionPartInfoStorage[i]
   //          console.log(correctionPart)
   //
   //     }
   //     console.log("-------------------")
   //  }


    registeringHandlerCE(){
        let that = this;
        let caretLengthPosInfo = that.lengthPosInfo

        this.targetAreaElement.addEventListener("DOMSubtreeModified",function(evt){
            // console.log(evt.target, evt.currentTarget)
            //TA와는 다르게 textNode만 있는것이 아니므로 tag와 기타 event에서 다중으로 event가 발동된다
            //따라서 manageCorrectionPart() 나 adjustRangeByEvent가 다중으로 발동됨
            if(that.targetAreaElement.textContent.trim().length === 0){
                that.lastRequestedContent = ""
            }
        })

        this.targetAreaElement.addEventListener("keyup", function(evt){
            // console.log(evt.keyCode)
            let target = evt.target;

            if(caretLengthPosInfo.ctrlKey === true && evt.keyCode === 17){
                caretLengthPosInfo.ctrlKey = false
            }
            if(caretLengthPosInfo.altKey === true && evt.keyCode === 18){
                caretLengthPosInfo.altKey = false
            }

            //한/영 전환키를 누르게 되면 keyDown에서 한글 입력과 똑같은 229로 찍힌다 따라서
            //addingPos가 1이 추가가 되어버림 따라서 keyup에서는 원래대로 21로 찍히므로 한/영전환키를
            //눌렀을땐 keyUp 을 따른다.
            //한글 조합중에 backspace(8) 를 누르면 keyDown의 keyCode는 한글입력인 229로 찍힌다
            //문제는 조합의 마지막남은걸 지울때도 한글로 인식하여 addingPos가 1이 되어버린다
            //따라서 마지막 남은 자음을 지워도 한칸 띄워진 상태의 caretLengthPos상태가 되어져 문제가 발생함
            if(evt.keyCode === 21 || evt.keyCode === 8 ){
                caretLengthPosInfo.lastKeyCode = evt.keyCode
            }

            let sel = document.getSelection()
            // console.log(sel.isCollapsed)
            //collapsed를 확인하는 이유는 selection 되어 있는 상황에서 backspace가 나가 버리면 caretLengthPosInfo가
            //selectionChange의 의도대로 바뀌는게 아니라 아래처럼 바뀌어 버리기 때문
            if (sel.isCollapsed){
                //2019 06월 어느시점이후로 caret위치가 linux처럼 조합중에도 글자 뒤에 있는걸로 바뀌었음 따라서
                //우선 addingPos가 필요가 없어졌음
                let addingPos = caretLengthPosInfo.lastKeyCode === 229 && os === "win" ? 0 : 0
                // console.log(addingPos)
                caretLengthPosInfo.oldPosEnd = caretLengthPosInfo.newPosEnd
                caretLengthPosInfo.oldPosStart = caretLengthPosInfo.newPosStart
                caretLengthPosInfo.oldWholeLength = caretLengthPosInfo.newWholeLength

                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement) + addingPos;
                caretLengthPosInfo.newPosStart = position
                caretLengthPosInfo.newPosEnd = position
                caretLengthPosInfo.newWholeLength = that.targetAreaElement.textContent.length

            }
            // console.log(caretLengthPosInfo)
            if(evt.keyCode === 35 || evt.keyCode === 36 ||
                evt.keyCode === 37 || evt.keyCode === 38 || evt.keyCode === 39 || evt.keyCode === 40){
                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement)
                caretLengthPosInfo.newPosStart = position
                caretLengthPosInfo.newPosEnd = position
            }else{
                //실제 내용에 영향을것 미치는 것들만 modified time에 넣는다
                caretLengthPosInfo.lastModifiedTime = new Date().getTime()
                console.log(caretLengthPosInfo)
                that.manageCorrectionPart()
                    //탭, space bar  enter 일경우 typingWorker를 호출
                    if(caretLengthPosInfo.lastKeyCode === 32 ||
                        caretLengthPosInfo.lastKeyCode === 9 ||
                        caretLengthPosInfo.lastKeyCode === 13){
                        that.initCorrectionTypingWorker()
                    }
                that.adjustRangeByEvent()
            }
        })

        this.targetAreaElement.addEventListener("keypress", function(evt){
            //linux 에서
            // try{
            //     console.log(that.rangeInfoStorage[1].rangeObj)
            // }catch(e){
            //
            // }
        })

        this.targetAreaElement.addEventListener("keydown", function(evt){
            //mouse로 붙여넣기 버튼을 누를때나 ctrl +  x , ctrl+ z  를 누를때 인식하기 위해서 필요
            caretLengthPosInfo.ctrlKey = evt.keyCode === 17 ? true : false
            caretLengthPosInfo.altKey = evt.keyCode === 18 ? true : false
            // console.log(caretLengthPosInfo.ctrlKey)
            // console.log(evt.keyCode)
            //한글 입력 여부를 알기위해 keydown에서 229를 체크
            caretLengthPosInfo.lastKeyCode = evt.keyCode
            let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement)

            //collapsed를 확인하는 이유는 selection 되어 있는 상황에서 backspace가 나가 버리면 caretLengthPosInfo가
            //selectionChange의 의도대로 바뀌는게 아니라 아래처럼 바뀌어 버리기 때문
            let sel = document.getSelection()
            // backspace 또는 del키를 가만히 누르고 있으면서 지우면 part 경계 부분을 인식하지 않고 지나가서
            // part를 못 없앤다 따라서 누르면서 지울때는 연속적으로 이를 keyUp 처럼 반영하도록 한다
            if ((evt.keyCode === 8 || evt.keyCode === 46) && sel.isCollapsed ){
                caretLengthPosInfo.oldPosEnd = caretLengthPosInfo.newPosEnd
                caretLengthPosInfo.oldPosStart = caretLengthPosInfo.newPosStart
                caretLengthPosInfo.oldWholeLength = caretLengthPosInfo.newWholeLength

                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement)
                caretLengthPosInfo.newPosStart = position
                caretLengthPosInfo.newPosEnd = position
                caretLengthPosInfo.newWholeLength = that.targetAreaElement.textContent.length
                that.manageCorrectionPart()
            }


            //직접 내용이 변화 되어서 추적할수 있는 내용은 input이 담당하고
            // 내용이 변환되지 않으나 위치를 추적해야 하는 Home end Arrow old 는 여기서 지속적으로 추적해줌
            if(evt.keyCode === 35 || evt.keyCode === 36 || evt.keyCode === 37 ||
                evt.keyCode === 38 || evt.keyCode === 39 || evt.keyCode === 40){
                caretLengthPosInfo.oldPosEnd = position
                caretLengthPosInfo.oldPosStart = position
            }
            //linux에서 '나는 이땅에 신토부울이' 를 치고 대기한뒤 initTime worker에 의해서
            // 교정 밑줄이 그어진뒤 스페이스 바를 누르면 RnageObj의 endOffset이 하나 줄어있다
            // 원인은 못 찾았음.

            // try{
            //     console.log(that.rangeInfoStorage[1].rangeObj)
            // }catch(e){
            //
            // }
        })

        this.targetAreaElement.addEventListener("paste", function(evt){
            // console.log(evt.target)
            console.log(evt.ctrlKey)
            //after paste
            setTimeout(function(){
                caretLengthPosInfo.oldPosEnd = caretLengthPosInfo.newPosEnd
                caretLengthPosInfo.oldPosStart = caretLengthPosInfo.newPosStart
                caretLengthPosInfo.oldWholeLength = caretLengthPosInfo.newWholeLength

                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement);
                // after text synced
                caretLengthPosInfo.newPosStart = position
                caretLengthPosInfo.newPosEnd = position
                caretLengthPosInfo.newWholeLength = that.targetAreaElement.textContent.length
                console.log(caretLengthPosInfo)

                //ctrl + v 로 붙여넣기 했을때 일단은 keyup에서 처리하므로 여기선 할게 없다
               if(caretLengthPosInfo.ctrlKey === true && caretLengthPosInfo.lastKeyCode === 17){

                //마우스 우클릭해서 붙여넣기를 눌렀을때다 keyUp에서 다루지 못하므로 여기서 다룬다
                }else{
                    caretLengthPosInfo.lastModifiedTime = new Date().getTime()
                    that.manageCorrectionPart()
                    that.adjustRangeByEvent()
                }

            })
        })

        //selectionchange는 document를 해야 먹힘
        document.addEventListener("selectionchange", function(evt){
            // console.log(evt.target, evt.currentTarget)
            //click, selection event일때는 증감이 없으므로 old와 new가 동일
            let caretLengthPosInfo = that.lengthPosInfo
            //
            // //onselectEvent가 없으므로 positionUtil.getCurrentCursorPosition 와 length를 이용하여
            // // selection을 판단한다
            let sel = document.getSelection()

            //CE에서는 \n과 \t 를 select에서 counting 하지 않는다 애초에 다textContent로 들어가있지 않는
            let selWithOutTab = sel.toString().replace(/\t/g, '')
            let selWithoutNewLine = selWithOutTab.toString().replace(/\n/g, '')
            // console.log(selWithoutNewLine)
            let selectedLength = selWithoutNewLine.length

            let backwardAdding = 0
            // console.log("selection",sel.toString(), selectedLength)
            if(selectedLength > 0){

                //그런데 역방향으로  selection한것과 정방향으로 selection한것이 다른 결과를 내므로
                //역방향인지 아닌지 여부를 알기위해 다음과 같은 로직을 이용함
                let selPosition = sel.anchorNode.compareDocumentPosition(sel.focusNode),
                    backward = false;
                // selPosition == 0 if nodes are the same
                if (!selPosition && sel.anchorOffset > sel.focusOffset ||
                    selPosition === Node.DOCUMENT_POSITION_PRECEDING)
                    backward = true;
                if (backward) backwardAdding = selectedLength

                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement);
                caretLengthPosInfo.oldPosEnd = position + backwardAdding
                caretLengthPosInfo.oldPosStart = position - selectedLength + backwardAdding
                caretLengthPosInfo.oldWholeLength = caretLengthPosInfo.newWholeLength
                caretLengthPosInfo.newPosEnd = position + backwardAdding
                caretLengthPosInfo.newPosStart = position - selectedLength + backwardAdding
                caretLengthPosInfo.newWholeLength = that.targetAreaElement.textContent.length
                console.log(caretLengthPosInfo)
            }
        })

        this.targetAreaElement.addEventListener("mouseup",function(evt){
            let sel = document.getSelection()
            if(sel.isCollapsed){
                let caretLengthPosInfo = that.lengthPosInfo
                let position = positionUtil.getCurrentCursorPosition(that.targetAreaElement);
                caretLengthPosInfo.oldPosEnd = caretLengthPosInfo.newPosEnd
                caretLengthPosInfo.oldPosStart = caretLengthPosInfo.newPosStart
                caretLengthPosInfo.newPosEnd = position
                caretLengthPosInfo.newPosStart = position
            }
            // console.log(caretLengthPosInfo)
        })

        //iframe 자체에서 scroll을 contenteditable로 지원하는 경우 이렇게 조정해 줘야 한다
        document.addEventListener("scroll", function(evt){
            // if (that.targetAreaElement.tagName.toLowerCase() === "body"){
            //CE가 최외곽에 있는경우에는 scrollTop을 보내주지 않는다
            if(window.top !== window.self){
                for(let j in that.rangeInfoStorage){
                    if(that.rangeInfoStorage.hasOwnProperty(j)){
                        let rangeInfo = that.rangeInfoStorage[j]

                        // rangeInfo.setRangeLocalCoord()
                        rangeInfo.makeGhostErrZone(document.documentElement.scrollTop, document.documentElement.scrollLeft)
                    }
                }
            }
        })

        //errZone이 visibility가 hidden인 것으로 이루어져 있으므로 targetAreaElement의 마우스 좌표를
        //추적하여 해당 공간에 들어서면 발동을 시키는 것으로 함
        this.targetAreaElement.addEventListener("mousemove", function(evt){
            let target = evt.target;

            let mouseCoordX = evt.clientX
            let mouseCoordY = evt.clientY
            let ghostPanel = that.ghostPanel

            let locatedErrZone = null
            let errZonePosX = 0
            let errZonePosY = 0
            let errZoneWidth = 0
            let errZoneHeight = 0

            var targetAreaStyle = window.getComputedStyle(that.targetAreaElement)
            let targetAreaRect = that.targetAreaElement.getBoundingClientRect()
            let marginTop = parseInt(targetAreaStyle.marginTop.replace("px", ""))
            let marginLeft = parseInt(targetAreaStyle.marginLeft.replace("px", ""))
            let currentFrameAbsolutePosition = getCurrentFrameAbsolutePosition()


            for(let rangeInfo in that.rangeInfoStorage){
                if(that.rangeInfoStorage.hasOwnProperty(rangeInfo)){
                    let errZone = that.rangeInfoStorage[rangeInfo].ghostErrWordZone
                    let rangeObj = that.rangeInfoStorage[rangeInfo].rangeObj
                    let rangeLocalCoord = that.rangeInfoStorage[rangeInfo].localCoord

                    //RangeInfoStorage에 저장되어있는 errZone에는 iframe scroll 의 scrollTop이 반영되어 있음
                    let errZoneLeft = parseInt(errZone.style.left.replace("px", ""))
                    let errZoneTop = parseInt(errZone.style.top.replace("px", ""))
                    let zoneLeft = 0
                    let zoneTop = 0
                    if(window.top === window.self){
                        zoneLeft = errZoneLeft+ currentFrameAbsolutePosition.x + targetAreaRect.left
                        zoneTop = errZoneTop+currentFrameAbsolutePosition.y + targetAreaRect.top
                    }else{
                        zoneLeft = errZoneLeft
                        zoneTop = errZoneTop
                    }

                    if(zoneLeft < mouseCoordX  && mouseCoordX < zoneLeft + rangeLocalCoord.width &&
                        mouseCoordY > zoneTop && mouseCoordY < zoneTop + rangeLocalCoord.height){
                        locatedErrZone = errZone
                        errZonePosX = zoneLeft
                        errZonePosY = zoneTop
                        errZoneWidth = rangeLocalCoord.width
                        errZoneHeight = rangeLocalCoord.height
                        break
                    }
                }
            }

            if(locatedErrZone){
                if(!that.pointedErrZone){
                    // console.log("errZone")
                    let errZoneNum = locatedErrZone.id.split("han_gramm_errZone__")[1];
                    locatedErrZone.classList.add("_3GrEs")
                    if (window.top === window.self) {


                    }else {
                        //현재 frame의 절대 위치
                        // console.log(currentFrameAbsolutePosition)
                        //현재 프레임의 절대 위치에서  프레임에서 errZone 까지의 거리를 더하면 전체 x y 가 도출됨
                        errZonePosX = currentFrameAbsolutePosition.x + errZonePosX;
                        errZonePosY = currentFrameAbsolutePosition.y + errZonePosY;
                    }
                    //해당 iframe 내의 위치

                    // console.log(errZonePosX, errZonePosY, errZoneHeight, errZoneWidth, that.candidateContents[errZoneNum]);
                    window.top.postMessage({
                            type: "errZoneMouseIn",
                            data : [
                                errZonePosX,
                                errZonePosY,
                                errZoneWidth,
                                errZoneHeight,
                                that.candidateContents[errZoneNum],
                                errZoneNum,
                                that.targetAreaId]
                        },
                        window.top.location.href)
                    that.pointedErrZone = locatedErrZone
                }


            }else{
                if(that.pointedErrZone){
                    that.pointedErrZone.classList.remove("_3GrEs")
                    window.top.postMessage({
                            type: "errZoneMouseOut",
                            data : ""
                        },
                        window.top.location.href)
                    that.pointedErrZone = null
                }

            }

        })

        //첫줄에 errZone이 있을경우 너무 위에 붙어 있어서 errZone으로 들어갔다가 나올때
        // ghostPanel 내부영역인 errZone의바깥으로 나가 는게 아닌 아예 아싸리 ghostpanel
        // 바깥으로 나가는 경우가 생김. 이때도 errZone 을 빠져나갔다는 신호를 줘야함
        this.ghostPanel.addEventListener("mouseleave", function(evt){
            // console.log(evt.target)
            if(that.pointedErrZone){
                that.pointedErrZone.classList.remove("_3GrEs")
                window.top.postMessage({
                        type: "errZoneMouseOut",
                        data : ""
                    },
                    window.top.location.href)
                that.pointedErrZone = null
            }
        })
    }

    adjustRangeByEvent(differencePos) {
        let caretLengthPosInfo = this.lengthPosInfo
        // differencePos = differencePos || caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength
        differencePos = differencePos !== undefined ? differencePos : caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength
        // console.log(differencePos, caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength)
        // let differencePos = caretLengthPosInfo.newPosEnd - caretLengthPosInfo.oldPosEnd
        // let differencePos = caretLengthPosInfo.newPosEnd - caretLengthPosInfo.oldPosEnd
        // console.log(differencePos)
        let rangeInfoStorage = this.rangeInfoStorage
        // CE는 newLine여부를 모르므로 항상 이렇게 추적을 해야함
        let linePartedContentArray = this.makeLinePartedContentArray()
        // console.log(beforeNewLine)
        // console.log(rangeInfoStorage)
        // console.log(caretLengthPosInfo, differencePos)
        for (let i in rangeInfoStorage) {

            if (rangeInfoStorage.hasOwnProperty(i)) {

                let range = rangeInfoStorage[i].rangeObj
                let clone = range.cloneRange()
                let errWordOnly = rangeInfoStorage[i].errWordOnly
                let absoluteStartPos = rangeInfoStorage[i].absoluteStartPos
                let absoluteEndPos = rangeInfoStorage[i].absoluteEndPos
                let moved = false

                //여기서 && differencePos !== 0 조건이 들어가야 논리적으로 맞지만  newLine 개념이 없는CE에서
                //엔터를 눌러 내용을 아래로 내렸을경우 인식하지 못하는 문제가 있다. 따라서 해당조건을 없애
                if(caretLengthPosInfo.oldPosStart <= absoluteStartPos ){
                    console.log(caretLengthPosInfo.oldPosStart, absoluteStartPos)
                    moved = true
                }
                // console.log(errWordOnly, this.analyzeLinePartedContentArray(linePartedContentArray, absoluteStartPos, absoluteEndPos))
                if(moved){
                    // console.log(caretLengthPosInfo, absoluteStartPos, differencePos)
                    let finalRange = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos + differencePos, absoluteEndPos + differencePos)
                    //앞뒤로 한칸씩 늘린 range
                    let oneWideRangeAfter = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos + differencePos - 1 , absoluteEndPos + differencePos + 1 )
                    let oneWideRangeBeforeString = rangeInfoStorage[i].oneWideRangeString
                    console.log(oneWideRangeAfter.toString(), oneWideRangeBeforeString)
                    if(errWordOnly === finalRange.toString() && oneWideRangeAfter.toString().trim() === oneWideRangeBeforeString.trim()){
                        rangeInfoStorage[i].rangeObj=finalRange
                        rangeInfoStorage[i].absoluteStartPos += differencePos
                        rangeInfoStorage[i].absoluteEndPos += differencePos
                    }else{

                        //양옆붙은것들은 analyzedLineInfo를 거쳐서 결정한다.
                        let analyzedLineInfo = this.analyzeLinePartedContentArray(linePartedContentArray, absoluteStartPos +differencePos, absoluteEndPos +differencePos)
                        // console.log(errWordOnly, analyzedLineInfo,linePartedContentArray)
                        //최우측이나 최좌측(newLine근처) 일경우 확장된 단어가 다르다고 하더라도 없애지 않는다
                        if(analyzedLineInfo.extremeRight === true || analyzedLineInfo.extremeLeft === true){
                            rangeInfoStorage[i].rangeObj=finalRange
                            rangeInfoStorage[i].absoluteStartPos += differencePos
                            rangeInfoStorage[i].absoluteEndPos += differencePos
                        }else{
                            console.log(absoluteStartPos, absoluteEndPos)
                            rangeInfoStorage[i].ghostErrWordZone.remove()
                            delete rangeInfoStorage[i]
                            delete this.candidateContents[i]
                        }
                    }
                }else{
                    let finalRange = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos , absoluteEndPos )
                    //앞뒤로 한칸씩 늘린 range
                    let oneWideRangeAfter = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos - 1 , absoluteEndPos + 1 )
                    let oneWideRangeBeforeString = rangeInfoStorage[i].oneWideRangeString
                    // console.log(finalRange, errWordOnly, finalRange.toString(), oneWideRangeAfter, oneWideRangeAfter.toString(), oneWideRangeBeforeString)
                    // console.log(errWordOnly === finalRange.toString(),oneWideRangeAfter.toString() === oneWideRangeBeforeString)
                    if(errWordOnly === finalRange.toString() && oneWideRangeAfter.toString().trim() === oneWideRangeBeforeString.trim()){
                        continue
                    }else{
                        //원래 errWord와 다르면 무조건 지워준다
                        if(errWordOnly !== finalRange.toString()){
                            rangeInfoStorage[i].ghostErrWordZone.remove()
                            delete rangeInfoStorage[i]
                            delete this.candidateContents[i]
                            continue
                        }

                        let analyzedLineInfo = this.analyzeLinePartedContentArray(linePartedContentArray, absoluteStartPos, absoluteEndPos)
                        // console.log(analyzedLineInfo, linePartedContentArray)
                        //첫단어가 check 된것은 비록 extremeLeft 일 지라도 옆에게 붙으면 삭제 해준다
                        if((analyzedLineInfo.extremeRight === true || analyzedLineInfo.extremeLeft === true) ){
                        // && absoluteStartPos !== 0
                            continue
                        }
                        console.log("here")
                        rangeInfoStorage[i].ghostErrWordZone.remove()
                        delete rangeInfoStorage[i]
                        delete this.candidateContents[i]
                    }
                }
            }
        }
    }



    processSaramInSpellCheckedData(receivedData){
        let that = this
        let originalContent = receivedData.originalContent
        let contentRange = receivedData.contentRange;
        //최초에 맞춤법 검사를 통째로 보낸 범위 CE의 경우 newLine의 정보가 없다
        let startPos = contentRange.startPos;
        let endPos = contentRange.endPos;

        //최초 newLine정보가 없는 범위값으로 CE의 getContentInRange로직에 의해 newLine 정보가 들어간
        //content가 뽑혀 나왔듯이 현재시점의 똑같은 범위값 내용 content를 뽑아냄
        let extractedContent = this.getContentInRange(startPos, endPos)
        // console.log(extractedContent)
        //교정 요청전과 후가 달려져 있다면 취소 시킴
        if (extractedContent !== originalContent){
            return
        }

        //saramin만 이 작업을 시킴
        this.preProcessSaramInSpellCheckedData(receivedData)

        let checkedData = receivedData.checkedData
        //jobkorea 변수명인 mcontent로 맞춤
        let mcontent = receivedData.checkedData.result_text
        // console.log(mcontent)
        mcontent = mcontent.replace(/<br>/g, '')
        mcontent = mcontent.replace(/<br\/>/g, '')
        //saramin 에서는 <br /> 식으로 온다
        mcontent = mcontent.replace(/<br\ \/>/g, '')
        mcontent = mcontent.replace(/\n/g, '')

        let dummyElem = document.createElement("div");
        dummyElem.innerHTML = mcontent;
        //가끔 string화된 tag ex) > 가 띄어쓰기가 있는경우가 있기에  이렇게 일치 시켜 준다
        mcontent = dummyElem.innerHTML
        // 두칸 이상뛴 내용에 nbsp가 섞여서 올 수 있음
        mcontent = mcontent.replace(/\&nbsp\;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp/g, ' ')
        // console.log(mcontent)
        let modifiedContentInfoArray = [];

        for(let i = 0; i < dummyElem.childElementCount; i++){
            //html string 으로 받아지는 mcontent내용을 parsing 하기 위한 작업
            let childrenMcontent = dummyElem.children[i]
            //여기있는 id 값이 spell checked 된 반환내용중 candidate words의 몇번째 내용인지를 결정한다
            // 이건 jobKorea기준이므로 달라질 수 있음
            let childrenMcontentId = parseInt(childrenMcontent.getAttribute("data-value"))

            let childOuterHtml = childrenMcontent.outerHTML;
            let childInnerText = childrenMcontent.innerText
            let childInnerTextLength = childInnerText.length

            let errWordOnly = childInnerText;
            let errWordOnlyLength = childInnerTextLength;
            let errWordWithTag = childOuterHtml
            //attribute  중에서 data-value="0" 가 다 다르기 때문에 동일한 errword를 가지고 있다고 해도 무방
            let errWordStartPos = mcontent.indexOf(errWordWithTag)
            let errWordEndPos = errWordStartPos + errWordOnlyLength

            //errWordOnly로 치환 함으로서 다음 순회에서 위치를 뽑아낼수 있음
            mcontent = mcontent.replace(errWordWithTag, errWordOnly);

            if(this.checkAlphabetOnlyWord(errWordOnly, errWordOnlyLength)){
                continue
            }
            // 앞에서부터 치환된 문장을 기준으로 위치를 기록
            // console.log(mcontent)
            let errWordZoneNumber = this.errWordZoneNumber++
            // console.log(checkedData.WordCandidateList, checkedData.WordCandidateList[i])
            this.candidateContents[errWordZoneNumber] = checkedData.WordCandidateList[i]
            // console.log(this.candidateContents[errWordZoneNumber], this.candidateContents, this)
            // console.log(this.candidateContents[errWordZoneNumber], this.candidateContents, this)
            modifiedContentInfoArray.push({"errWordZoneNumber" : errWordZoneNumber,"errWordOnly": errWordOnly,
            "errWordWithTag" : errWordWithTag, "errWordStartPos": errWordStartPos, "errWordEndPos": errWordEndPos})
        }
        // console.log(this.candidateContents[0])
        // console.log(mcontent)
        // console.log(modifiedContentInfoArray)
        for(let i=0; i< modifiedContentInfoArray.length; i++){
            // console.log(modifiedContentInfoArray)
            let modifiedContentInfo = modifiedContentInfoArray[i]
            let errWordStartPos = modifiedContentInfo.errWordStartPos
            let errWordEndPos = modifiedContentInfo.errWordEndPos;
            //이게 적합한 위치에 있는 errWord와 바꿔치기 된다
            let errWordZoneNumber = modifiedContentInfo.errWordZoneNumber;
            let errWordOnly = modifiedContentInfo.errWordOnly

            let absoluteStartPos = startPos + errWordStartPos
            let absoluteEndPos = startPos + errWordEndPos

            // console.log(startPos, errWordStartPos, errWordEndPos)
            let finalRange = this.createFinalRangeHG(this.targetAreaElement, startPos + errWordStartPos, startPos + errWordEndPos)
            let clone = finalRange.cloneRange();
            let rangeLength = finalRange.toString().length;
            let oneWideRange = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos - 1 , absoluteEndPos + 1 )
            let oneWideRangeString = oneWideRange.toString()

            let duplicated = false
            for(let j in this.rangeInfoStorage){
                if(this.rangeInfoStorage.hasOwnProperty(j)){
                    duplicated = this.rangeInfoStorage[j].checkDuplicated(absoluteStartPos, absoluteEndPos, errWordOnly)
                    if (duplicated) break
                }
            }

            if(duplicated){
                //위에서 candidateContents를 errWordZoneNumber로 집어 넣었으므로 지워줌
                delete this.candidateContents[errWordZoneNumber]
                continue
            }

            this.rangeInfoStorage[errWordZoneNumber] =
                new RangeInfo(this, finalRange, absoluteStartPos, absoluteEndPos, errWordOnly, errWordZoneNumber, oneWideRangeString)

            // console.log(finalRange, finalRange.toString(), rangeLength);
        }
    }

    //잡코리아 위주의 response data 처리를 함
    processJobKoreaSpellCheckedData(receivedData){
        let that = this
        //이거 쓸모없으면 지울것
        let originalContent = receivedData.originalContent
        let contentRange = receivedData.contentRange;
        //최초에 맞춤법 검사를 통째로 보낸 범위 CE의 경우 newLine의 정보가 없다
        let startPos = contentRange.startPos;
        let endPos = contentRange.endPos;

        //최초 newLine정보가 없는 범위값으로 CE의 getContentInRange로직에 의해 newLine 정보가 들어간
        //content가 뽑혀 나왔듯이 현재시점의 똑같은 범위값 내용 content를 뽑아냄
        let extractedContent = this.getContentInRange(startPos, endPos)
        // console.log(extractedContent)
        //교정 요청전과 후가 달려져 있다면 취소 시킴
        if (extractedContent !== originalContent){
            return
        }

        //jobKorea만 이 작업을 시킴
        // console.log(receivedData.checkedData.WordInfo.mcontent);
        let checkedData = this.preProcessJobKoreaSpellCheckedData(receivedData.checkedData);
        //candidateContent처리
        this.preProcessJobKoreaCandidateContents(checkedData.WordCandidateList);

        let mcontent = checkedData.WordInfo.mcontent;
        //checked된 데이터에 <br> 이 string으로 박혀서 올수 있음
        mcontent = mcontent.replace(/<br>/g, '')
        mcontent = mcontent.replace(/<br\/>/g, '')
        // 두칸 이상뛴 내용에 nbsp가 섞여서 올 수 있음
        mcontent = mcontent.replace(/\&nbsp\;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp/g, ' ')

        //jobKorea에서 String으로 받아온 span이 들어간 내용을 div안에 넣는다
        let dummyElem = document.createElement(("div"));
        dummyElem.innerHTML = mcontent;

        //contenteditable의 range로 설정한 값들은 띄어쓰기를 무시한다 따라서 br로 오는건 없애줌
        //but TA는 다름
        dummyElem.innerHTML = dummyElem.innerHTML.replace(/<br>/g, '');
        dummyElem.innerHTML = dummyElem.innerHTML.replace(/<br\/>/g, '');
        // console.log(mcontent)
        // console.log(dummyElem)
        let modifiedContentInfoArray = [];
        // console.log(dummyElem,dummyElem.childElementCount, dummyElem.children, dummyElem.childNodes)
        //jobKorea 형식(태그안에 errWord가 있는 전체문장)으로 오는것을 파싱하여 정보를 갖춘 array로 만든다.
        for (let i = 0; i < dummyElem.childElementCount ; i++){
            //html string으로 받아지는 mcontent내용을 parsing하기 위한 작업
            let childrenMcontent = dummyElem.children[i];
            //여기있는 id 값이 spell checked 된 반환내용중 candidate words의 몇번째 내용인지를 결정한다
            // 이건 jobKorea기준이므로 달라질 수 있음
            let childrenMcontentId = childrenMcontent.id;

            let childOuterHtml =  childrenMcontent.outerHTML;
            let childOuterHtmlLength = childOuterHtml.length;

            let childInnerText = childrenMcontent.innerText;
            let childInnerTextLength = childInnerText.length;


            //errWordWithTag로 위치 파악하고 errWordOnlyLength로 범위를 구한다
            let errWordOnly = childInnerText;
            let errWordOnlyLength = childInnerTextLength;

            let errWordWithTag = childOuterHtml;

            //id 부분이 숫자가 다 다르기 때문에 동일한 errword를 가지고 있다해도 무방함
            let errWordStartPos = mcontent.indexOf(errWordWithTag);
            let errWordEndPos = errWordStartPos + errWordOnlyLength;

            //errWordOnly로 치환 함으로서 다음 순회에서 위치를 뽑아낼수 있음
            mcontent = mcontent.replace(errWordWithTag, errWordOnly);
            // 앞에서부터 치환된 문장을 기준으로 위치를 기록
            if(this.checkAlphabetOnlyWord(errWordOnly, errWordOnlyLength)){
                continue
            }

            let errWordZoneNumber = this.errWordZoneNumber++;
            //만들필요 없음
            // let errWordZoneElem = this.makeErrWordZoneElem(errWordOnly, errWordZoneNumber);
            // console.log(errWordZoneNumber)
            // console.log(checkedData.WordCandidateList)
            // console.log(childrenMcontentId, checkedData.WordCandidateList[childrenMcontentId])
            this.candidateContents[errWordZoneNumber] = checkedData.WordCandidateList[childrenMcontentId]

            // this.candidateWords = {}
            modifiedContentInfoArray.push({"errWordZoneNumber" : errWordZoneNumber, "errWordOnly" : errWordOnly,
                "errWordWithTag" : errWordWithTag, "errWordStartPos" : errWordStartPos,
                "errWordEndPos" : errWordEndPos})
        }
        console.log(mcontent)

        for (let i = 0; i < modifiedContentInfoArray.length; i++){
            // console.log(modifiedContentInfoArray)
            //통째로 보낸 범위 부터 시작한 errWord의 시작위치
            let modifiedContentInfo = modifiedContentInfoArray[i]
            let errWordStartPos = modifiedContentInfo.errWordStartPos;
            let errWordEndPos = modifiedContentInfo.errWordEndPos;
            //이게 적합한 위치에 있는 errWord와 바꿔치기 된다
            let errWordZoneNumber = modifiedContentInfo.errWordZoneNumber;
            let errWordOnly = modifiedContentInfo.errWordOnly

            let absoluteStartPos = startPos + errWordStartPos
            let absoluteEndPos = startPos + errWordEndPos

            //체커가 잡코리아나 사람인 체커를 것을 파싱해서 사용할때 그쪽에서 보내주는 errWord 를 추린것임 테스트용으로사용했음
            let errWordWithTagFromChecker = modifiedContentInfoArray[i].errWordWithTag;

            // console.log(startPos, errWordStartPos, errWordEndPos)
            let finalRange = this.createFinalRangeHG(this.targetAreaElement, startPos + errWordStartPos, startPos + errWordEndPos)
            let clone = finalRange.cloneRange();
            let rangeLength = finalRange.toString().length;
            let oneWideRange = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos - 1 , absoluteEndPos + 1 )
            let oneWideRangeString = oneWideRange.toString()
            // console.log(finalRange, finalRange.toString(), rangeLength);
            // console.log(clone)
            // let endContainer = finalRange.endContainer;
            let duplicated = false
            for(let j in this.rangeInfoStorage){
                if(this.rangeInfoStorage.hasOwnProperty(j)){
                    duplicated = this.rangeInfoStorage[j].checkDuplicated(absoluteStartPos, absoluteEndPos, errWordOnly)
                    if (duplicated) break
                }
            }

            if(duplicated){
                //위에서 candidateContents를 errWordZoneNumber로 집어 넣었으므로 지워줌
                delete this.candidateContents[errWordZoneNumber]
                continue
            }

            this.rangeInfoStorage[errWordZoneNumber] =
                new RangeInfo(this, finalRange, absoluteStartPos, absoluteEndPos, errWordOnly, errWordZoneNumber, oneWideRangeString)

            // console.log(finalRange, finalRange.toString(), rangeLength);
        }

        //세밀한 correctionPart를 폐기함 으로서 일단 이건 필요 없는걸로 한다.
        //무사히 part가 성공적으로 되면 status 를 바꿔준다
        let correctionPartUuid = receivedData.correctionPartUuid
        for (let part = 0 ;part < this.correctionPartInfoStorage.length; part++){
            let correctionPart = this.correctionPartInfoStorage[part]
            // console.log(correctionPartUuid, correctionPart.uuid)
            if (correctionPart.uuid === correctionPartUuid){
                correctionPart.status = CorrectionPart.PARTCOMPLETED
                correctionPart.processingStartTime = null
                console.log(this.correctionPartInfoStorage)
                break
            }
        }
    }

    analyzeLinePartedContentArray(partedContentArray, absoluteStartPos, absoluteEndPos){
        let value = {extremeLeft : false, extremeRight : false}

        absoluteStartPos = absoluteStartPos
        for(let i = 0; i < partedContentArray.length; i++){
            let content = partedContentArray[i]
            let locChar = content.charAt(absoluteStartPos)
            let partialContentlength = content.length

            // console.log("start",absoluteStartPos, partialContentlength, locChar)
            if(locChar ===""){
                absoluteStartPos -= partialContentlength
            }else{
                if(absoluteStartPos === 0){
                    value.extremeLeft = true
                }
                break
            }
        }

        absoluteEndPos = absoluteEndPos -1
        for(let i = 0; i < partedContentArray.length; i++){
            let content = partedContentArray[i]
            let locChar = content.charAt(absoluteEndPos)
            let partialContentlength = content.length

            // console.log("end", absoluteEndPos , partialContentlength - 1)
            if(locChar ===""){
                absoluteEndPos -= partialContentlength
            }else{
                if(absoluteEndPos === partialContentlength - 1){
                    value.extremeRight = true
                }
                break
            }
        }
        return value
    };

    //CE버전의 getContentIn Range에서 착안하여 띄여쓰기를 기분으로 array를 만듬
    makeLinePartedContentArray(){
        let linePartedContentArray = [];
        for (let childNum = 0; childNum < this.targetAreaElement.childNodes.length; childNum++){
            let childNode = this.targetAreaElement.childNodes[childNum];

            if (childNode.nodeType === Node.TEXT_NODE){
                if(linePartedContentArray.length === 0){
                    linePartedContentArray.push(childNode.nodeValue)
                    continue
                }
                linePartedContentArray[linePartedContentArray.length - 1] += childNode.nodeValue;

            }else{
                if (childNode.tagName.toLowerCase() === "p" || childNode.tagName.toLowerCase() === "div"){
                    linePartedContentArray.push(childNode.innerText)
                }else if (childNode.tagName.toLowerCase() === "br" || childNode.tagName.toLowerCase() === "br/"){
                    linePartedContentArray.push("\n")
                }else{
                    linePartedContentArray[linePartedContentArray.length - 1] += childNode.nodeValue
                }
            }
        }
        // console.log(linePartedContentArray)

        //backspace 키를 눌러서 내려가 있는 문장을 위로 올리면 linePartedContent에 \n 이 뒤에 붙어 버림 그걸 지워줌
        for (let i =0; i < linePartedContentArray.length; i++){
            if (linePartedContentArray[i].length > 1 &&
                linePartedContentArray[i].lastIndexOf("\n") === linePartedContentArray[i].length - 1 ){
                // console.log("found")
                linePartedContentArray[i] = linePartedContentArray[i].substring(0, linePartedContentArray[i].length - 1)
            }
        }

        // facebook이나 유튜브의 경우 \n 이 하나의 array 값 안에 들어가는 경우가 있다 따라서 마지막에 있는\n을
        // 제외하고 모두 나눔 처리를 해줌

        let temp = []
        for (let i =0; i < linePartedContentArray.length; i++){
            let content = linePartedContentArray[i]
            if(content !== "\n"){
                let splited = content.split("\n")
                // console.log(splited)
                temp = temp.concat(splited)
            }else{
                temp.concat(["\n"])
            }
        }

        linePartedContentArray = temp

        //array의 처음 시작은 문장을 몇칸 아래로 내리고 쓰더라도 linePartedContentArray의 시작은 문장이 와야됨 \n 이
        // 오면 안됨
        while(true){
            // console.log(linePartedContentArray[0], linePartedContentArray[0] === "\n")
            if(linePartedContentArray[0] === "\n"){
                linePartedContentArray.splice(0,1)
            }else{
                break
            }
        }
        // console.log(linePartedContentArray)

        return linePartedContentArray
    }

    getContentInRange (start, end){
        // caret 위치까지의 모든 String을 수집 만약 시작위치와 종료위치가 있다면
        // setStart와 setEnd를 적적히 사용할 것 createRange를 사용해야 할듯
        // for contentedit field
        this.targetAreaElement.focus();
        let range = this.createFinalRangeHG(this.targetAreaElement,start, end)
        let targetAreaContent = range.toString();
        // console.log(targetAreaContent) //여기 까지는 \n 이 반영되지 않는다.

        //mirror를 통해서 해결해 보려고 했으나 안됨
        //일단 해당 범위 안에 있는 content의 newline여부를 알아야 하기 때문에 documentFragment를
        //호출하여 첫번째 textnode를 추적
        // let rangeForFragment = this.createFinalRangeHG(this.targetAreaElement,start, this.targetAreaElement.innerText.length)
        // var documentFragment = range.cloneContents();
        // console.log(documentFragment)
        // console.log(documentFragment,documentFagment.childNodes.length);
        //gmail같은경우 contenteditable 하부에 무조건 div나 p 태그가 생기는게 아니라 첫줄은 외부 태그없이 바로 text가 들어감
        //따라서함
        //fragment를 하면 띄여쓰기 여부를 알수가 없기에 일단 다 뽑아내고 범위를 추린다
        //실제 newLine 보다 더많은 line이 생길수 있느데 어차피 spell Check 된 후에 어차피 모든 newLine을 없애므로
        //상관없
        let contentConsideringLine = "";

        contentConsideringLine = this.targetAreaElement.innerText
        // console.log(contentConsideringLine)

        let countStartWithNewLine = 0
        let countStartWithoutNewLine = 0
        for(let i = 0;i < contentConsideringLine.length; i++){
            if (start === 0 ) break
            countStartWithNewLine++
            if(contentConsideringLine[i] !== "\n"){
                countStartWithoutNewLine++;
            }
            if(countStartWithoutNewLine === start){
                break
            }
        }


        let countEndWithNewLine = 0
        let countEndWithoutNewLine = 0
        for(let i = 0;i < contentConsideringLine.length; i++){
            if(end === 0) break
            countEndWithNewLine++
            if(contentConsideringLine[i] !== "\n"){
                countEndWithoutNewLine++;
            }
            if(countEndWithoutNewLine === end){
                break
            }
        }
        // console.log(start, end, countStartWithoutNewLine, countStartWithNewLine, countEndWithoutNewLine, countEndWithNewLine)
        //보내는 content는 newline을 고려해서 보내주고 받아온뒤에 newline 을모두 제거한뒤  newline 없는 기준의
        //start와 end 로 따져서 rangeObj를 만들어 낸다
        contentConsideringLine = contentConsideringLine.substring(countStartWithNewLine, countEndWithNewLine)
        //c2b7 middle dot 주시하고 있을것 이것때문에ㅐ
        // nbsp 처리를 안하면 검사기 엔진이 제대로 작동을 안하는 듯 보임
        let contentConsideringLineWithoutNbsp = contentConsideringLine.replace(/\u00a0/g, " ");
        // middle Dot 처리를 안하면 검사기 엔진이 제대로 작동을 안하는 듯 보임
        let contentConsideringLineWithout8203 = contentConsideringLineWithoutNbsp.replace(/\u200B/g, "");

        // return contentWithoutNbsp;
        return contentConsideringLineWithout8203;
    }

    makeHanGrammGhost(){
        let that = this;
        let hanGrammarGhost = document.createElement("hanGrammar-ghost")
        hanGrammarGhost.style.position = "relative"
        hanGrammarGhost.style.top = "0px";
        hanGrammarGhost.style.left = "0px";
        hanGrammarGhost.style.pointerEvents = "none";
        hanGrammarGhost.style.zIndex = "5000"

        let hanGrammarGhostPart = document.createElement("div")
        hanGrammarGhostPart.setAttribute("hanGrammar-part","highlights")
        hanGrammarGhostPart.style.position = "absolute"
        hanGrammarGhostPart.style.top = "0px";
        hanGrammarGhostPart.style.left = "0px";

        //크기 sync가 필요함 이건 무조건 this.targetArea 위치에 있어야
        let hanGrammarGhostMirror = document.createElement("div")
        let ghostMirrorStyle = hanGrammarGhostMirror.style
        ghostMirrorStyle.position = "relative"
        ghostMirrorStyle.boxSizing = "content-box"
        ghostMirrorStyle.pointerEvents = "none"
        ghostMirrorStyle.overflow = "hidden"
        ghostMirrorStyle.border = "0px"
        ghostMirrorStyle.borderRadius = "0px"
        ghostMirrorStyle.padding = "0px"
        ghostMirrorStyle.margin = "0px"

        //CE는 iframe 바로 안에 있는 targetAreaElement면 targetArea 자체보다는 바로 선조격인 iframe이
        //content가 들어갈 영역을 잘 반영하고 있음
       let standardElem = this.CEclosestIframe || this.targetAreaElement
        // console.log(standardElem)
        //textarea 크기를 sync 해줌
        // let computed = window.getComputedStyle(standardElem)
        // // + computed["padding-right"] + computed["padding-left"]
        // // + computed["padding-top"] +computed["padding-bottom"]
        //
        // let computedWidth =  parseInt(computed["width"])  + parseInt(computed["padding-left"]) + parseInt(computed["padding-right"])
        // let computedHeight = parseInt(computed["height"]) + parseInt(computed["padding-top"]) + parseInt(computed["padding-bottom"])
        // // console.log(computedWidth, computedHeight)
        // ghostMirrorStyle.width =  computedWidth + "px"
        // ghostMirrorStyle.height = computedHeight + "px"

        //보이는 영역 기준인 그리고 transform 까지 계산되어진 rect를 사용하는게 맞는듯 하
        var rect = standardElem.getBoundingClientRect()
        let computedWidth = rect.width
        let computedHeight = rect.height
        // console.log(computedWidth, computedHeight)
        ghostMirrorStyle.width =  computedWidth + "px"
        ghostMirrorStyle.height = computedHeight + "px"

        let hanGrammarGhostButton = document.createElement("div")
        hanGrammarGhostButton.setAttribute("hanGrammar-part","button")
        hanGrammarGhostButton.style.position = "absolute"
        hanGrammarGhostButton.style.right = "5px";
        hanGrammarGhostButton.style.bottom = "5px";

        let ghostbuttonLoader = document.createElement("div")
        ghostbuttonLoader.innerText = new String(0)[0]

        hanGrammarGhostButton.appendChild(ghostbuttonLoader)

        let observer = new MutationObserver(function(mutation) {
            var rect = standardElem.getBoundingClientRect()

            let targetWidth = rect.width
            let targetHeight = rect.height

            ghostMirrorStyle.width =targetWidth + "px"
            ghostMirrorStyle.height = targetHeight + "px"

        })
        observer.observe(standardElem,{attributes : true, attributeFilter : ['style']})

        // #mutationObserver 로는 안되는 경우가 있어서 이걸로 계속 sync
        setTimeout(function syncSize(){
            var rect = standardElem.getBoundingClientRect()

            let targetWidth = rect.width
            let targetHeight = rect.height

            ghostMirrorStyle.width =targetWidth + "px"
            ghostMirrorStyle.height = targetHeight + "px"
            //네이버 같은경우 메일창을 나가면 메일 쓰는 CE가 0px가 된다 따라서 모두 지워
            if(targetWidth === "0px" || targetHeight === "0px"){
                for (let i in that.rangeInfoStorage) {
                    if (that.rangeInfoStorage.hasOwnProperty(i)) {
                        that.rangeInfoStorage[i].ghostErrWordZone.remove()
                        delete that.rangeInfoStorage[i]
                        delete that.candidateContents[i]
                    }
                }
            }
            // console.log("synced")
            setTimeout(syncSize, 100)
        },100)

        let hanGrammarGhostPanelSkin = document.createElement("div")
        hanGrammarGhostPanelSkin.style.position = "absolute"
        hanGrammarGhostPanelSkin.style.top = "0px";
        hanGrammarGhostPanelSkin.style.left = "0px";

        //todo 코드로 맞추는 잡업이 필요함  크기가 알아서 커지게
        let hanGrammarGhostPanel = document.createElement("div")
        //임시로 만든것
        hanGrammarGhostPanel.style.width = "2553px"
        hanGrammarGhostPanel.style.height = "1004px"

        //panel이 하나가 더있는데 용도를 모르겠음  hanGrammarGhostPanel과 heigh width 가 동일

        hanGrammarGhostPanelSkin.appendChild(hanGrammarGhostPanel)
        hanGrammarGhostMirror.appendChild(hanGrammarGhostPanelSkin)
        hanGrammarGhostMirror.appendChild(hanGrammarGhostButton)
        hanGrammarGhostPart.appendChild(hanGrammarGhostMirror)
        hanGrammarGhost.appendChild(hanGrammarGhostPart)

        let left,top

        // window.top.document.getElementsByTagName("html")[0].appendChild(hanGrammarGhost)
        if(window.top === window.self){
            this.targetAreaElement.parentElement.parentElement.prepend(hanGrammarGhost)

            let hanGrammarGhostPartRect = hanGrammarGhostPart.getBoundingClientRect()
            let targetAreaRect = this.targetAreaElement.getBoundingClientRect()
            left = targetAreaRect.left - hanGrammarGhostPartRect.left
            top = targetAreaRect.top -hanGrammarGhostPartRect.top

        }else {

            //정확한 좌표를 잡으려면 ghost의 위치를 최외곽 frame의 상단에 넣어야 한다 즉 topFrame안의 element로
            //(iframe  안에 또 iframe이 있고 그곳에 targetAreaElement 가 있으면 이러한 문제가 발생)
            //따라서 최외곽 frame을 찾는 작업임
            let currentWindow = window
            let outerFrame;
            while(currentWindow.frameElement !== null){
                outerFrame = currentWindow.frameElement
                currentWindow = currentWindow.parent
            }
            // console.log(outerFrame)
            let iframeElement = outerFrame

            iframeElement.parentElement.parentElement.prepend(hanGrammarGhost)

            let currentFrameAbsolutePosition = getCurrentFrameAbsolutePosition()
            let hanGrammarGhostPartRect = hanGrammarGhostPart.getBoundingClientRect()
            let targetAreaRect = this.targetAreaElement.getBoundingClientRect()
            // console.log(currentFrameAbsolutePosition, hanGrammarGhostPartRect, targetAreaRect)
            var targetAreaStyle = window.getComputedStyle(this.targetAreaElement)
            let marginTop = parseInt(targetAreaStyle.marginTop.replace("px", ""))
            let marginLeft = parseInt(targetAreaStyle.marginLeft.replace("px", ""))

            left = (currentFrameAbsolutePosition.x + targetAreaRect.left - marginLeft) - hanGrammarGhostPartRect.left
            top = (currentFrameAbsolutePosition.y + targetAreaRect.top - marginTop) -hanGrammarGhostPartRect.top
        }

        // console.log(top, left)
        ghostMirrorStyle.top =top + "px"
        ghostMirrorStyle.left =left+ "px"

        return hanGrammarGhostPanel
    }
    // 나는 이땅에 신토부울이 나느 너에게 너는 나에게 바래왔던 모든것
    //
    //
    //
    // 속을 들키면 게임오버 여기 까지가
    processCorrectionBySelectedWord(errWord, selectedWord, errZoneNum){

        let absoluteStartPos = this.rangeInfoStorage[errZoneNum].absoluteStartPos
        let absoluteEndPos = this.rangeInfoStorage[errZoneNum].absoluteEndPos

        //원인은 찾지 못했지만 CE 에서는 저장된 rangeObj가 도중에 바뀌어버리는 때가 존재하게 됨 따라서 startPos와 EndPos로
        //rangeObj를 교정시마다 새로 만들어서 활용하는 방안을 선택하였음
        // let foundedErrZoneRange = this.rangeInfoStorage[errZoneNum].rangeObj
        let foundedErrZoneRange = this.createFinalRangeHG(this.targetAreaElement, absoluteStartPos, absoluteEndPos)

        // let clone = foundedErrZoneRange.cloneRange()
        console.log(foundedErrZoneRange)
        // console.log(clone, foundedErrZoneRange)
        foundedErrZoneRange.deleteContents()
        // console.log(clone, foundedErrZoneRange)
        foundedErrZoneRange.insertNode(document.createTextNode(selectedWord))


        let rangeInfoStorage = this.rangeInfoStorage
        rangeInfoStorage[errZoneNum].ghostErrWordZone.remove()
        delete rangeInfoStorage[errZoneNum]
        delete this.candidateContents[errZoneNum]

        let differencePos = selectedWord.length - errWord.length

        let caretLengthPosInfo = this.lengthPosInfo
        caretLengthPosInfo.oldPosStart = absoluteEndPos
        caretLengthPosInfo.oldPosEnd = absoluteEndPos
        caretLengthPosInfo.newWholeLength = this.targetAreaElement.textContent.length

        caretLengthPosInfo.newPosStart = absoluteEndPos + (selectedWord.length - errWord.length)
        caretLengthPosInfo.newPosEnd = absoluteEndPos + (selectedWord.length - errWord.length)
        caretLengthPosInfo.newWholeLength = this.targetAreaElement.textContent.length

        //교정이 되면서 글 길이가 변하므로 part 들도 조정해 줘야 한다.
        this.manageCorrectionPart(differencePos)
        //교정 되면서 글 길이가 변하므로 range 들도 조정해 줘야 한다.
        console.log(selectedWord, errWord, differencePos)
        this.adjustRangeByEvent(differencePos)
        //caret의 위치를 교정단어 끝으로 이동
        positionUtil.setCurrentCursorPosition(this.targetAreaElement, caretLengthPosInfo.newPosEnd)
    }

    //IGNORE를 클릭했을때 1.겉보기에 errWordZone인것을 없애고 2. 다음 spell Check가 왔을때는
    //errZone인것처럼 아무 영향이 없어야 한다
    processIgnoreSelection(errZoneNum){
        let rangeInfo = this.rangeInfoStorage[errZoneNum]
        rangeInfo.ignored = true
        delete this.candidateContents[errZoneNum]
        this.rangeInfoStorage[errZoneNum].ghostErrWordZone.remove()
    }
}

class HanGrammarAreaTA extends HanGrammarArea{

    constructor(targetAreaId){
        super(targetAreaId)
        this.mirrorDiv = this.makeHanGrammMirror()
        //registeringHandlerTA 보다 먼져 와야
        this.ghostPanel = this.makeHanGrammGhost()
        this.hanGrammarButton = this.ghostPanel.closest("hangrammar-ghost").querySelector("[hangrammar-part=button]").firstElementChild
        this.registeringHandlerTA()
        this.initButtonWorker()
        this.lastRequestedContent = this.targetAreaElement.value

    }

    initButtonWorker(){
        let that = this
        let button = this.hanGrammarButton
        //CE든 TA 든 실제 영향을 미치는 것만 lastModifiedTime에 들어가 있다
        let lastModifiedTime = this.lengthPosInfo.lastModifiedTime;
        // button.classList.add("han-gramm-loader")

        setTimeout(function doButtonWork(){
            var count = Object.keys(that.candidateContents).length
            if (that.targetAreaElement.value.length ===0 ){
                if (button.classList.contains("han_gramm_loader")){
                    button.innerText = new String(count)
                    button.classList.remove("han_gramm_loader")
                }
            }else{
                //이건 무조건 체크해서 반영한다
                button.innerText = new String(count)

                let currentTime = new Date().getTime()
                if(currentTime - that.lengthPosInfo.lastModifiedTime > 2000){
                    if (button.classList.contains("han_gramm_loader")) {
                        button.classList.remove("han_gramm_loader")

                    }

                }else{
                    if(!button.classList.contains("han_gramm_loader")){
                        button.classList.add("han_gramm_loader")
                    }
                }
            }
            setTimeout(doButtonWork, 1000)
        }, 1000 )
    }

    initCorrectionTimeWorker(){
        let that = this;
        let caretLengthPos = this.lengthPosInfo

        setTimeout(function timeCorrector(){
            let currentTime = new Date().getTime()
            if((currentTime - caretLengthPos.lastModifiedTime > 3000 &&
                Math.abs(that.targetAreaElement.value.trim().length - that.lastRequestedContent.trim().length) > 5)){
                //제일 끝에서 쭉 지워서 남아있는 내용이 마지막 request의 내용중에 있으면 request를 보내지 않는다
                // if(that.targetAreaElement.value.trim().length - that.lastRequestedContent.trim().length < 0 &&
                //     that.lastRequestedContent.trim().indexOf(that.targetAreaElement.value.trim() !== -1)){
                //     console.log("return!")
                //     return
                // }
                let correctionPart = that.manageCorrectionPart()
                that.observeArea(correctionPart)
                that.lastRequestedContent = that.targetAreaElement.value
            }
            setTimeout(timeCorrector, 1000)
        },  1000)
    }

    initCorrectionTypingWorker(){
        let that = this;
        let caretLengthPos = this.lengthPosInfo

        let delayingVal = 0

        if(Math.abs(that.targetAreaElement.value.trim().length > 5 &&
            that.targetAreaElement.value.trim().length - that.lastRequestedContent.trim().length) > 10){
            //제일 끝에서 쭉 지워서 남아있는 내용이 마지막 request의 내용중에 있으면 request를 보내지 않는다
            // if(that.targetAreaElement.value.trim().length - that.lastRequestedContent.trim().length < 0 &&
            //     that.lastRequestedContent.trim().indexOf(that.targetAreaElement.value.trim() !== -1)){
            //     console.log("return!")
            //     return
            // }
            let correctionPart = that.manageCorrectionPart()
            that.observeArea(correctionPart)
            that.lastRequestedContent = that.targetAreaElement.value
        }
    }


    //correctionPart를 세밀하게 나누는것을 포기한 이후로 주석처리함
    // //시간에 따라서 교정을 시작하는 로직 javascript worker랑 관련없음
    // initCorrectionTimeWorker(){
    //     let correctionPartInfoStorage = this.correctionPartInfoStorage
    //     let that = this;
    //     let caretLengthPos = this.lengthPosInfo
    //
    //
    //     setTimeout(function timeCorrector(){
    //         let delayingVal = 0
    //         for (let part = 0; part < correctionPartInfoStorage.length; part++){
    //             let correctionPart = correctionPartInfoStorage[part]
    //             let currentTime = new Date().getTime()
    //             if(correctionPart.status === CorrectionPart.MAKING || correctionPart.status === CorrectionPart.MODIFYING ){
    //                 //partlength 가 10 이상인 상황에서 3초 이상입력이 없으면 processing 진행
    //                 // console.log(currentTime - caretLengthPos.lastModifiedTime)
    //                 if((currentTime - caretLengthPos.lastModifiedTime > 3000 && correctionPart.partLength > 10)){
    //                     //너무많은 요청이 있을때 각 요청을 1초씩 차이를 둔다
    //                     setTimeout(function(){
    //                         that.observeArea(correctionPart)
    //                     }, 1000 * delayingVal)
    //                     delayingVal++
    //                 }
    //             }
    //             //아주 잠깐의 시간동안 MODIFYING으로 바뀐뒤에 다음 setTimeout call 때 곧바로 다시 요청이 들어간다
    //             if(correctionPart.status === CorrectionPart.PROCESSING &&
    //                 correctionPart.processingStartTime !== null &&
    //                 currentTime - correctionPart.processingStartTime > 9000){
    //                 correctionPart.status = CorrectionPart.MODIFYING
    //                 correctionPart.processingStartTime = null
    //             }
    //         }
    //         setTimeout(timeCorrector, 1000)
    //     }, 1000)
    // }
    //
    // //spacebar나 엔터 탭키 등으로 교정을 시작하는 로직
    // initCorrectionTypingWorker(){
    //     let correctionPartInfoStorage = this.correctionPartInfoStorage
    //     let that = this;
    //     let caretLengthPos = this.lengthPosInfo
    //
    //     let delayingVal = 0
    //     for (let part = 0; part < correctionPartInfoStorage.length; part++){
    //         let correctionPart = correctionPartInfoStorage[part]
    //         if(correctionPart.status === CorrectionPart.MAKING || correctionPart.status === CorrectionPart.MODIFYING ){
    //             console.log(correctionPart, correctionPart.partLength)
    //             if(correctionPart.partLength > 15){
    //                 setTimeout(function(){
    //                     that.observeArea(correctionPart)
    //                 }, 500 * delayingVal)
    //                 delayingVal++
    //             }
    //         }
    //     }
    // }

    killAll(){
        //rangeInfoStorage에서 있을지 모를 ghostErrZone과 candidate rangeObj들을 모두 삭제한다
        for (let i in this.rangeInfoStorage) {
            if (this.rangeInfoStorage.hasOwnProperty(i)) {
                this.rangeInfoStorage[i].ghostErrWordZone.remove()
                delete this.rangeInfoStorage[i]
                delete this.candidateContents[i]
            }
        }
        // button을 모두 삭제해 준다
        this.hanGrammarButton.style.display= "none";
        // this.hanGrammarButton.remove()

        //다시 해당 도메인을 on 했을때를 대비해서 mirror와 ghost는 그대로 둔다
    }

    reviveAll(){
        this.hanGrammarButton.style.display = ""
    }


    manageCorrectionPart(){
        //newLine이 고려되지 않은 length임
        return new CorrectionPart(0, this.targetAreaElement.value.length)
    }

    //correctionPart를 세밀하게 나누는 것을 포기한 이후로 주석처리함
    //manageCorrection은 TA에선 DomeNodeChange(?) 에서 이루어지는 변화를 반영하여 correctionPart를 만들고
    //조작한다
    //교정을 했을때는 변화의 정도를 추적하는 differencePos 를 인자로 넣어준다
    // manageCorrectionPart(differencePos){
    //     let that = this
    //     let caretLengthPosInfo =this.lengthPosInfo
    //     let correctionPartInfoStorage = this.correctionPartInfoStorage
    //
    //     let oldPosStart = caretLengthPosInfo.oldPosStart
    //     let oldPosEnd = caretLengthPosInfo.oldPosEnd
    //     let newPosStart = caretLengthPosInfo.newPosStart
    //     let newPosEnd = caretLengthPosInfo.newPosEnd
    //     let oldWholeLength = caretLengthPosInfo.oldWholeLength
    //     let newWholeLength = caretLengthPosInfo.newWholeLength
    //     // console.log(caretLengthPosInfo)
    //     differencePos = differencePos !== undefined ? differencePos : caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength
    //
    //     //현재 typing이나 드래그 등이 어느 part에 결부되어있는지 찾아낸다 하나 또는 그 이상(드래그, 중간)일수 있음
    //     function findCorrectionPart(){
    //         let correctionParts = []
    //         for (let part= 0 ;part < correctionPartInfoStorage.length ; part++){
    //             let correctionPart = correctionPartInfoStorage[part]
    //
    //             if(correctionParts.length === 0 && correctionPart.status  === CorrectionPart.MAKING &&
    //                 correctionPartInfoStorage.length > 1){
    //                 correctionParts.push(correctionPart)
    //             }else{
    //                 //개별 파트를 oldPosStart와 oldPosEnd로 측량해서  범위안에(붙여넣기 일때)
    //                 let upper = correctionPart.isUpperPartThanOldPosStart(oldPosStart)
    //                 // console.log(upper)
    //                 let lower = correctionPart.isLowerPartThanOldPosEnd(oldPosEnd)
    //                 // console.log(lower)
    //                 if(upper === lower){
    //                     correctionParts.push(upper)
    //                 }
    //             }
    //         }
    //         return correctionParts
    //     }
    //
    //     function adjustCorrectionParts(){
    //         differencePos = newWholeLength - oldWholeLength
    //         // console.log(caretLengthPosInfo)
    //         for(let part= 0 ; part < correctionPartInfoStorage.length ; part++){
    //             let correctionPart = correctionPartInfoStorage[part]
    //             // console.log(correctionPart,caretLengthPosInfo ,differencePos)
    //             //part 내부에서 일어난 일
    //             if (correctionPart.start <= oldPosStart && oldPosEnd <= correctionPart.end){
    //                 //part 사이의 공통공간 조작시 오른쪽은 start도 영향을 받아야함 첫번째 part시작은 제외
    //                 if(correctionPart.start === oldPosEnd && correctionPart.start !== 0){
    //                     correctionPart.start += differencePos
    //                 }
    //                 //이 조건은 사실 필요없을것 같은데 확실해지면 지울것
    //                 if(correctionPart.status === CorrectionPart.PROCESSING){
    //                     continue
    //                 }else if(correctionPart.status === CorrectionPart.PARTCOMPLETED){
    //                     correctionPart.status = CorrectionPart.MODIFYING
    //                     correctionPart.end += differencePos
    //                 }else{
    //                     //making 상황일때는 오직 PROCESSING을 거쳐 PARTCOMPLETED로 갈 뿐이다
    //                     correctionPart.end += differencePos
    //                 }
    //             //oldPosEnd가 임의의 part보다 앞에 있을때는 part 전체 통째로 움직인다
    //             }else if(oldPosEnd < correctionPart.start) {
    //                 correctionPart.start += differencePos
    //                 correctionPart.end += differencePos
    //
    //             //part내부에서 일어난 일과 정반대의 상황으로 3개 이상의 part가 결부되어 있을경우
    //             //가운데에 낀 part상황임 혹은 절묘하게 part의 start와 end만 select 된후 일어나는 상황
    //             }
    //             //위의 과정을 거친후 해당 part의 partLength가 0이되는 순간 지워준다
    //             if(correctionPart.partLength === 0 ){
    //                 // console.log("partLength = 0")
    //                 //만약 맨끝 part를 지우는게 아니라 처음 또는 중간을 지워버리면 다음part를 건너뛰어버림
    //                 //그것 조
    //                 let needPartNumAdjust =false
    //                 if(part !== correctionPartInfoStorage.length - 1) needPartNumAdjust = true
    //                 correctionPartInfoStorage.splice(part,1)
    //
    //                 if (needPartNumAdjust) part--
    //
    //                 continue
    //             }
    //             // console.log(correctionPart,caretLengthPosInfo ,differencePos)
    //         }
    //     }
    //
    //     function sliceLongCorrectionPart(correctionPart, partNum){
    //         let wholeContent = that.targetAreaElement.value
    //         let miniPartLength = 50
    //         let longCorrectionPartAbsoluteStart = correctionPart.start
    //         let longCorrectionPartAbsoluteEnd = correctionPart.end
    //
    //         let longCorrectionPartLength = correctionPart.partLength
    //
    //         let dividedPartLength = Math.ceil(longCorrectionPartLength / miniPartLength)
    //
    //         let dividedPartArray = []
    //         while(true){
    //             //애초에 length 100 이상인 것들만 넘어오므로 최초에는 무조건 50이 넘는다
    //             if(longCorrectionPartLength >= miniPartLength){
    //                 if(dividedPartArray.length ===0){
    //                     //최초 miniPart의 absoluteStart는 longCorrectionPartAbsoluteStart의 start와 같
    //                     let miniPartAbsoluteStart = longCorrectionPartAbsoluteStart
    //                     let tempMiniPartAbsoluteEnd = longCorrectionPartAbsoluteStart + miniPartLength
    //                     //일단 50까지 잘라낸것
    //                     let tempMiniPartContent = wholeContent.substring(miniPartAbsoluteStart, tempMiniPartAbsoluteEnd)
    //                     //50까지 끊은것에서 뒤로 계속 가서 " " 이나 \n 이 나오는 lastindex를 구함
    //                     let blankLastIndex = tempMiniPartContent.lastIndexOf(" ", miniPartLength)
    //                     let newLineLastIndex = tempMiniPartContent.lastIndexOf("\n", miniPartLength)
    //
    //                     let miniPartAbsoluteEnd = 0
    //                     if(blankLastIndex === -1 && newLineLastIndex === -1){
    //                         miniPartAbsoluteEnd = tempMiniPartAbsoluteEnd
    //                     }else{
    //                         miniPartAbsoluteEnd = blankLastIndex >= newLineLastIndex ?
    //                             miniPartAbsoluteStart + blankLastIndex : miniPartAbsoluteStart +newLineLastIndex
    //                     }
    //                     let miniCorrectionPart = new CorrectionPart(miniPartAbsoluteStart, miniPartAbsoluteEnd)
    //                     dividedPartArray.push(miniCorrectionPart)
    //                     longCorrectionPartLength -= miniCorrectionPart.partLength
    //
    //                 }else{
    //                     //두번째 miniPart의 start는 이전 miniPart의 end와 같다.
    //                     let miniPartAbsoluteStart = dividedPartArray[dividedPartArray.length - 1].end
    //                     let tempMiniPartAbsoluteEnd = miniPartAbsoluteStart + miniPartLength
    //                     //일단 이전 end 부터 50까지 끊는다.
    //                     let tempMiniPartContent = wholeContent.substring(miniPartAbsoluteStart, tempMiniPartAbsoluteEnd)
    //                     //50까지 끊은것에서 뒤로 계속 가서 " " 이나 \n 이 나오는 lastindex를 구함
    //                     let blankLastIndex = tempMiniPartContent.lastIndexOf(" ", miniPartLength)
    //                     let newLineLastIndex = tempMiniPartContent.lastIndexOf("\n", miniPartLength)
    //
    //                     let miniPartAbsoluteEnd = 0
    //                     if(blankLastIndex === -1 && newLineLastIndex === -1){
    //                         miniPartAbsoluteEnd = tempMiniPartAbsoluteEnd
    //                     }else{
    //                         miniPartAbsoluteEnd = blankLastIndex >= newLineLastIndex ?
    //                             miniPartAbsoluteStart + blankLastIndex : miniPartAbsoluteStart +newLineLastIndex
    //                     }
    //                     let miniCorrectionPart = new CorrectionPart(miniPartAbsoluteStart, miniPartAbsoluteEnd)
    //                     dividedPartArray.push(miniCorrectionPart)
    //                     longCorrectionPartLength -= miniCorrectionPart.partLength
    //                 }
    //
    //
    //             }else{
    //                 dividedPartArray.push(new CorrectionPart(dividedPartArray[dividedPartArray.length - 1].end, longCorrectionPartAbsoluteEnd))
    //                 break;
    //             }
    //         }
    //
    //         return dividedPartArray
    //     }
    //
    //     //현재 입력이 이루어진(그냥 한글자던 범위선택후 조작이던간에) correctionPart(하나 또는 둘 이살일수 있다)
    //     let associatedCorrectionParts = findCorrectionPart()
    //     // console.log(associatedCorrectionParts)
    //     if(associatedCorrectionParts.length === 0){
    //         console.log(caretLengthPosInfo)
    //         this.correctionPartInfoStorage.push(new CorrectionPart(0, differencePos))
    //     }else if(associatedCorrectionParts.length === 1){ //결부된 part가 하나가 있을때 (단순타이핑)
    //         // console.log("one")
    //         //현재 processing 혹은 partcompleted 이라면 그리고 그것이 현재 마지막 part라면 새로은 part를 만듬
    //         if((associatedCorrectionParts[0].status === CorrectionPart.PROCESSING ||
    //             associatedCorrectionParts[0].status === CorrectionPart.PARTCOMPLETED) &&
    //             associatedCorrectionParts[0] === correctionPartInfoStorage[correctionPartInfoStorage.length -1] &&
    //             associatedCorrectionParts[0].end <= oldPosStart){
    //             // console.log(newPosEnd)
    //
    //             if(differencePos > 0){
    //                 //일단 linux냐 window냐에 따라서 한글이 초성과 중성만 만 쳤을때 caret위치가 어디냐가 차이가 있다 따라서
    //                 //이에 차이를 둠
    //                 // let correctionPartEnd = os === "linux" ? oldPosStart + differencePos : newPosEnd
    //                 this.correctionPartInfoStorage.push(new CorrectionPart(associatedCorrectionParts[0].end , oldPosStart + differencePos))
    //             }else{
    //                 // minus 일때는 새로운 part를 생성하지 않는다
    //                 adjustCorrectionParts()
    //             }
    //
    //         }else{
    //             adjustCorrectionParts()
    //         }
    //
    //     }else{
    //         //결부된 part가 두개 이상일 경우
    //         // console.log("associatedCorrectionParts")
    //         // part와 part사이 단일 caret이 위치한 상태중 공통부분에서 추가나 삭제있을시 처리
    //         // 즉 드래그 상태가 아님
    //         if (oldPosStart === oldPosEnd){
    //             adjustCorrectionParts()
    //             // 드래그로 여러 part가 선택 되었을때 처리 +
    //
    //         //특정한 correctionPart의 끝자락(다음 part와의 공통공간) 에 oldPosEnd가 있는상황하에서
    //         //교정버튼을 눌렀을경우  어느 조건에도 들어가지 않는다 따라서 해당조건 추가
    //         }else if(oldPosStart === newPosStart && newPosEnd !== newPosStart){
    //             adjustCorrectionParts()
    //
    //         }else{
    //             //합치는 알고리즘 splice를 쓰면 될듯. 그리고 adjust시킴
    //             //우선 여러 파트에 selection이 거쳐져 있으므로 첫번째 part의 start와 마지막 part의 end로
    //             // 새로운 part를 만들고 splice로 예전것들을 제거한뒤 새로운걸 그자리에 집어넣는다
    //             let joinedPartStart = associatedCorrectionParts[0].start
    //             let joinedPartEnd = associatedCorrectionParts[associatedCorrectionParts.length - 1].end
    //             let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
    //
    //             for(let i = 0; i < correctionPartInfoStorage.length; i++){
    //                 let correctionPart = correctionPartInfoStorage[i];
    //                 if (correctionPart === associatedCorrectionParts[0]){
    //                     correctionPartInfoStorage.splice(i, associatedCorrectionParts.length, joinedPart)
    //                     break;
    //                 }
    //             }
    //             adjustCorrectionParts()
    //         }
    //
    //     }
    //
    //     //나누어져 있던 part가 띄여쓰기를 제거하고 서로 이어붙이기를 했을때
    //     // correctionPart 둘을 이어붙여 하나로 만든다 backspace든 delete키든 둘 모두
    //     if( (associatedCorrectionParts.length === 1 && oldPosStart === oldPosEnd) ||
    //         (associatedCorrectionParts.length === 2 && oldPosStart === oldPosEnd) ){
    //
    //         let allContent = this.targetAreaElement.value
    //         // backspace 키를 눌러서 합쳤을때 + correctionPart가 하나밖에 없을때는 적용하지 않고(beforeJoinedPart를 구할수 없다),
    //         // 역시 마지막 남은 글자를 지웠을때 위에서(adjustCorrectionParts()) correctionPartInfoStorage
    //         // 를 0으로 만들므로 오류가 난다 따라서 이때도 적용안되게 함
    //         if (caretLengthPosInfo.lastKeyCode === 8 &&
    //             !(associatedCorrectionParts[0] === correctionPartInfoStorage[0] &&
    //                 associatedCorrectionParts.length === 1) &&
    //             correctionPartInfoStorage.length !== 0){
    //
    //             let afterJoinedPart = associatedCorrectionParts[associatedCorrectionParts.length - 1]
    //
    //             let indexOfAfterJoinedPart = 0
    //             for(let i = 0; i < correctionPartInfoStorage.length; i++){
    //                 if (afterJoinedPart == correctionPartInfoStorage[i]){
    //                     indexOfAfterJoinedPart = i
    //                     break
    //                 }
    //             }
    //             let beforeJoinedPart = correctionPartInfoStorage[indexOfAfterJoinedPart - 1]
    //
    //             //붙어있는 두 글자가 띄여쓰기나 newLine이 없으면 붙은걸로 간주함
    //             if (allContent.charAt(beforeJoinedPart.end - 1) !== " " &&
    //                 allContent.charAt(beforeJoinedPart.end - 1) !== "\n"){
    //                 if(allContent.charAt(afterJoinedPart.start) !== " " &&
    //                     allContent.charAt(afterJoinedPart.start) !== "\n"){
    //                     let joinedPartStart = beforeJoinedPart.start
    //                     let joinedPartEnd = afterJoinedPart.end
    //                     let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
    //
    //                     correctionPartInfoStorage.splice(indexOfAfterJoinedPart - 1, 2, joinedPart)
    //                 }
    //             }
    //         //del 키를 눌러서 합쳤을때 + correctionPart가 마지막 것 일때는 적용하지 않고 (afterJoinedPart를 구할수 없으므로),
    //         // 역시 마지막글자를 del 키로 지웠을때 위에서(adjustCorrectionParts()) correctionPartInfoStorage
    //         // 를 0으로 만들므로 오류가 난다 따라서 이때도 적용안되게 함
    //         }else if(caretLengthPosInfo.lastKeyCode === 46 &&
    //             !(associatedCorrectionParts[0] === correctionPartInfoStorage[correctionPartInfoStorage.length - 1] &&
    //                 associatedCorrectionParts.length === 1) &&
    //             correctionPartInfoStorage.length !== 0){
    //             let beforeJoinedPart = associatedCorrectionParts[0]
    //
    //             let indexOfBeforeJoinedPart = 0
    //             for(let i = 0; i < correctionPartInfoStorage.length; i++){
    //                 if (beforeJoinedPart == correctionPartInfoStorage[i]){
    //                     indexOfBeforeJoinedPart = i
    //                     break
    //                 }
    //             }
    //             let afterJoinedPart = correctionPartInfoStorage[indexOfBeforeJoinedPart + 1]
    //
    //             //붙어있는 두 글자가 띄여쓰기나 newLine이 없으면 붙은걸로 간주함
    //             if (allContent.charAt(beforeJoinedPart.end - 1) !== " " &&
    //                 allContent.charAt(beforeJoinedPart.end - 1) !== "\n"){
    //                 if(allContent.charAt(afterJoinedPart.start) !== " " &&
    //                     allContent.charAt(afterJoinedPart.start) !== "\n"){
    //                     let joinedPartStart = beforeJoinedPart.start
    //                     let joinedPartEnd = afterJoinedPart.end
    //                     let joinedPart = new CorrectionPart(joinedPartStart, joinedPartEnd)
    //
    //                     correctionPartInfoStorage.splice(indexOfBeforeJoinedPart , 2, joinedPart)
    //                 }
    //             }
    //         }
    //     }
    //
    //     //Length 가 100이 넘는 longCorrectionPart 를 50Length 작은 단위의 miniCorrectionPart들로 나눈다
    //     for(let i = 0; i< correctionPartInfoStorage.length; i++){
    //         let correctionPart = correctionPartInfoStorage[i]
    //         if (correctionPart.partLength > 100){
    //             //분리된 part를 받아온뒤 longCorrectionPart를 지우고 거기에 모두 넣는다.
    //             let dividedMiniPartArray = sliceLongCorrectionPart(correctionPart, i)
    //             console.log(dividedMiniPartArray)
    //             //... 은 python의 args나 kargs같은 개념인듯
    //             correctionPartInfoStorage.splice(i, 1, ...dividedMiniPartArray)
    //         }
    //     }
    //     console.log(correctionPartInfoStorage)
    // }


    registeringHandlerTA(){
        let that = this;
        let caretLengthPosInfo = that.lengthPosInfo
        //어차피 주인공은 mirrorDiv이므로 이것의 변화를 추적하여 range를 갱신시킨다 화살표와 Home End키는
        //각각 old 와 new 로 나누어서 keyDown과 keyup 에서 관리함
        this.mirrorDiv.addEventListener("DOMSubtreeModified",function(evt){
            // console.log(new Date().getTime())
            if(that.targetAreaElement.value.trim().length === 0){
                that.lastRequestedContent = ""
            }

            let modifying = caretLengthPosInfo.modifying
            //win32api상 한글 조합시 caret이 왼쪽에 위치하는데 크롬은 이를 그대로 이용하여 한글입력시
            //caret이 왼쪽에 위치된다 따라서  한글 입력시 pos 는 1을 더 하여야 한다
            //2019 06월 어느시점이후로 caret위치가 linux처럼 조합중에도 글자 뒤에 있는걸로 바뀌었음 따라서
            //우선 addingPos가 필요가 없어졌음
            let addingPos = caretLengthPosInfo.lastKeyCode === 229 && os === "win" ? 0 : 0

            console.log(addingPos)
            if (!modifying){
                // console.log(caretLengthPosInfo)
                caretLengthPosInfo.oldPosEnd = caretLengthPosInfo.newPosEnd
                caretLengthPosInfo.oldPosStart = caretLengthPosInfo.newPosStart
                caretLengthPosInfo.oldWholeLength = caretLengthPosInfo.newWholeLength

                // after text synced
                caretLengthPosInfo.newPosStart = that.targetAreaElement.selectionStart + addingPos
                caretLengthPosInfo.newPosEnd = that.targetAreaElement.selectionEnd + addingPos
                caretLengthPosInfo.newWholeLength = that.mirrorDiv.textContent.length
                caretLengthPosInfo.lastModifiedTime = new Date().getTime()
                console.log(caretLengthPosInfo)
                that.manageCorrectionPart()
                if(caretLengthPosInfo.lastKeyCode === 32 ||
                    caretLengthPosInfo.lastKeyCode=== 9 ||
                    caretLengthPosInfo.lastKeyCode === 13){
                    that.initCorrectionTypingWorker()
                }
                that.adjustRangeByEvent()
            }
        })

        this.targetAreaElement.addEventListener("select",function(evt){
            //selection event일때는 증감이 없으므로 old와 new가 동일
            // console.log("select")
            caretLengthPosInfo.oldPosEnd = that.targetAreaElement.selectionEnd
            caretLengthPosInfo.oldPosStart = that.targetAreaElement.selectionStart
            caretLengthPosInfo.newPosEnd = that.targetAreaElement.selectionEnd
            caretLengthPosInfo.newPosStart = that.targetAreaElement.selectionStart
            console.log(caretLengthPosInfo)
        })

        this.targetAreaElement.addEventListener("mouseup",function(evt){
            //selection event일때는 증감이 없으므로 old와 new가 동일
            caretLengthPosInfo.oldPosEnd = that.targetAreaElement.selectionEnd
            caretLengthPosInfo.oldPosStart = that.targetAreaElement.selectionStart
            caretLengthPosInfo.newPosEnd = that.targetAreaElement.selectionEnd
            caretLengthPosInfo.newPosStart = that.targetAreaElement.selectionStart
            // console.log(caretLengthPosInfo)
        })

        this.targetAreaElement.addEventListener("paste", function(evt){
            //이시점에선 아직 붙여넣기가 이루어지지않은 상황이다
            let pastedData = evt.clipboardData.getData("text/plain")
            // console.log(that.targetAreaElement.selectionEnd)
            // console.log(that.targetAreaElement.value.length)

            caretLengthPosInfo.oldPosEnd = that.targetAreaElement.selectionEnd
            caretLengthPosInfo.oldPosStart = that.targetAreaElement.selectionStart
            caretLengthPosInfo.oldWholeLength = that.targetAreaElement.value.length

            setTimeout(function(){
                let target = evt.target;
                let position = target.selectionEnd;

                that.mirrorDiv.textContent = that.targetAreaElement.value;
            })
            //after paste 상황
            setTimeout(function() {
                caretLengthPosInfo.newPosEnd = that.targetAreaElement.selectionEnd
                caretLengthPosInfo.newPosStart = that.targetAreaElement.selectionStart
                caretLengthPosInfo.newWholeLength = that.targetAreaElement.value.length
                // 그냥 붙여넣기를 하던, 일정 범위선택후 붙여넣기를 하던 이게 증감이 된다
                // that.adjustRangeByEvent()
                // DOMSubtreeModified 에서 중복관리 함.
                // that.observeArea(that.targetAreaElement);
            })
        })

        this.targetAreaElement.addEventListener("keydown", function(evt){
            // console.log("keydown", evt.keyCode)
            //한글 자모는keydown시에 229 로 찍힌다
            caretLengthPosInfo.lastKeyCode = evt.keyCode
            // console.log("down", evt.keyCode)
            //mouse로 붙여넣기 버튼을 누를때나 ctrl +  x , ctrl+ z  를 누를때 인식하기 위해서 필요
            caretLengthPosInfo.ctrlKey = evt.keyCode === 17 ? true : false
            caretLengthPosInfo.altKey = evt.keyCode === 18 ? true : false

            //직접 내용이 mirroring 되어서 변화를 추적할수 있는 내용은 DOMSubtreeModified 가 담당하고
            // 내용이 변환되지 않으나 위치를 추적해야 하는 Home end Arrow old 는 여기서 지속적으로 추적해줌
            if(evt.keyCode === 35 || evt.keyCode === 36 || evt.keyCode === 37 ||
                evt.keyCode === 38 || evt.keyCode === 39 || evt.keyCode === 40 ){
                caretLengthPosInfo.oldPosEnd = that.targetAreaElement.selectionEnd
                caretLengthPosInfo.oldPosStart = that.targetAreaElement.selectionStart
            }
            // backspace 또는 del키를 가만히 누르고 있으면서 지우면 part 경계 부분을 인식하지 않고 지나가서
            //part를 못 없앤다 따라서 누르면서 지울때는 연속적으로 이를 mirrorDiv에 반영하도록 한다
            if (evt.keyCode === 8 || evt.keyCode === 46){
                that.mirrorDiv.textContent = that.targetAreaElement.value;
            }
        })

        //mirror에 내용을 일치시키는 작업을 keyup에서 해야 좀더 안정화가 있어 보임
        this.targetAreaElement.addEventListener("keyup", function(evt){
            // console.log(evt.keyCode, "keyup")
            let target = evt.target;
            let position = target.selectionEnd;

            if(caretLengthPosInfo.ctrlKey === true && evt.keyCode === 17){
                caretLengthPosInfo.ctrlKey = false
            }
            if(caretLengthPosInfo.altKey === true && evt.keyCode === 18){
                caretLengthPosInfo.altKey = false
            }

            //한글 조합중에 조합을 backspace 를 눌러러단계별로 해체할경우 koydown의 backspace는
            //한글 누를때와 똑같은 229로 찍힌다 따라서 addingPos가 1이되어버림 이를 방지하기 위해
            //backspace 일때는 keyUp의 keyCode를 따른다
           if(evt.keyCode === 8 ){
                caretLengthPosInfo.lastKeyCode = evt.keyCode
            }

            that.mirrorDiv.textContent = that.targetAreaElement.value;

            //직접 내용이 mirroring 되어서 변화를 추적할수 있는 내용은 DOMSubtreeModified 가 담당하고
            // 내용이 변환되지 않으나 위치를 추적해야 하는 Home end Arrow new 는 여기서 지속적으로 추적해줌
            if(evt.keyCode === 35 || evt.keyCode === 36 || evt.keyCode === 37 ||
                evt.keyCode === 38 || evt.keyCode === 39 || evt.keyCode === 40){
                caretLengthPosInfo.newPosStart = that.targetAreaElement.selectionStart
                caretLengthPosInfo.newPosEnd = that.targetAreaElement.selectionEnd
            }
        })

        //textArea를 정확히 따라하는 hanGramm Mirror 은 textArea인 targetArea가 없어지면
        //따라서 없어져야 한다 근데 대댓글 같은경우 대댓글을 닫으면 targetArea가 없어지는게 아니라
        //조상 element가 사라지는경우가 많음 따라서 다음과 같은 작업을 함
        document.addEventListener("DOMNodeRemoved", function(evt){
            let target = evt.target
            // console.log(evt.target)
            //개별 페이지의 dom의 변화가 워낙 각양 각색이라 try문에 둠
            try{
                let allDescendantElements = target.getElementsByTagName("*")
                let exist = false
                for (let i = 0; i < allDescendantElements.length; i++){
                    let elem = allDescendantElements[i]
                    if (elem === that.targetAreaElement){
                        exist = true
                    }
                }
                if (exist === true) that.mirrorDiv.parentNode.remove()
            }catch (e){

            }
        })

        //TA가 scroll이 생길경우  errZone글을 따라 이동해야 하는데 이에 대한 처리가 필요하다
        this.targetAreaElement.addEventListener("scroll", function(evt){
            let target = evt.target

            for(let j in that.rangeInfoStorage){
                if(that.rangeInfoStorage.hasOwnProperty(j)){
                    let rangeInfo = that.rangeInfoStorage[j]

                    // rangeInfo.setRangeLocalCoord()
                    rangeInfo.makeGhostErrZone(target.scrollTop, target.scrollLeft)
                }
            }
        })

        //errZone이 visibility가 hidden인 것으로 이루어져 있으므로 targetAreaElement의 마우스 좌표를
        //추적하여 해당 공간에 들어서면 발동을 시키는 것으로 함
       this.targetAreaElement.addEventListener("mousemove", function(evt){
            let target = evt.target;

            let mouseCoordX = evt.clientX
            let mouseCoordY = evt.clientY
            let ghostPanel = that.ghostPanel

            let locatedErrZone = null
            let errZonePosX = 0
            let errZonePosY = 0
            let errZoneWidth = 0
            let errZoneHeight = 0
            for(let i =0 ; i < ghostPanel.childElementCount; i++){
                let ghostErrZoneElem = ghostPanel.children[i]

                let rect = ghostErrZoneElem.getBoundingClientRect()
                let zoneLeft = rect.left
                let zoneTop = rect.top

                if(zoneLeft < mouseCoordX  && mouseCoordX < zoneLeft + rect.width &&
                    mouseCoordY > zoneTop && mouseCoordY < zoneTop + rect.height){
                    locatedErrZone = ghostErrZoneElem
                    errZonePosX = zoneLeft
                    errZonePosY = zoneTop
                    errZoneWidth = rect.width
                    errZoneHeight = rect.height
                    break
                }
            }

            if(locatedErrZone){
                if(!that.pointedErrZone){
                    let errZoneNum = locatedErrZone.id.split("han_gramm_errZone__")[1];
                    locatedErrZone.classList.add("_3GrEs")
                    if (window.top === window.self) {

                        //hanGrammarScript가
                    }else {
                        //현재 frame의 절대 위치
                        let currentFrameAbsolutePosition = getCurrentFrameAbsolutePosition();
                        // console.log(currentFrameAbsolutePosition)
                        //현재 프레임의 절대 위치에서  프레임에서 errZone 까지의 거리를 더하면 전체 x y 가 도출됨
                        errZonePosX = currentFrameAbsolutePosition.x + errZonePosX;
                        errZonePosY = currentFrameAbsolutePosition.y + errZonePosY;
                    }
                    //해당 iframe 내의 위치

                    // console.log(errZonePosX, errZonePosY, errZoneHeight, errZoneWidth, that.candidateContents[errZoneNum]);
                    window.top.postMessage({
                            type: "errZoneMouseIn",
                            data : [
                                errZonePosX,
                                errZonePosY,
                                errZoneWidth,
                                errZoneHeight,
                                that.candidateContents[errZoneNum],
                                errZoneNum,
                                that.targetAreaId]
                        },
                        window.top.location.href)

                    that.pointedErrZone = locatedErrZone
                }


            }else{
                if(that.pointedErrZone){
                    that.pointedErrZone.classList.remove("_3GrEs")
                    window.top.postMessage({
                            type: "errZoneMouseOut",
                            data : ""
                        },
                        window.top.location.href)
                    that.pointedErrZone = null
                }
            }
        })

        // 이건 mouseout을 원하는 주체가 targetAreaElement가
        // 아니고 children 이기 때문에 mouseleave(최외곽을 빠져나왔을때 적용되기를 원할때)가
        // 아닌 mouseOut 을 사용
        this.ghostPanel.addEventListener("mouseleave", function(){
            if(that.pointedErrZone){
                that.pointedErrZone.classList.remove("_3GrEs")
                window.top.postMessage({
                        type: "errZoneMouseOut",
                        data : ""
                    },
                    window.top.location.href)
                that.pointedErrZone = null
            }
        })
    }

    //교정일때는 differencePos가 온다
    adjustRangeByEvent(differencePos) {
        let caretLengthPosInfo = this.lengthPosInfo
        differencePos = differencePos !== undefined ? differencePos : caretLengthPosInfo.newWholeLength - caretLengthPosInfo.oldWholeLength

        // console.log(differencePos)
        // let differencePos = caretLengthPosInfo.newPosEnd - caretLengthPosInfo.oldPosEnd
        // let differencePos = caretLengthPosInfo.newPosEnd - caretLengthPosInfo.oldPosEnd
        // console.log(differencePos)
        let rangeInfoStorage = this.rangeInfoStorage
        // console.log(rangeInfoStorage)
        for (let i in rangeInfoStorage) {
            if (rangeInfoStorage.hasOwnProperty(i)) {
                let range = rangeInfoStorage[i].rangeObj
                let clone = range.cloneRange()
                let errWordOnly = rangeInfoStorage[i].errWordOnly
                let absoluteStartPos = rangeInfoStorage[i].absoluteStartPos
                let absoluteEndPos = rangeInfoStorage[i].absoluteEndPos
                let moved = false

                // console.log(caretLengthPosInfo, absoluteStartPos, differencePos)
                if(caretLengthPosInfo.oldPosStart <= absoluteStartPos && differencePos !== 0){
                    moved = true
                }

                if(moved){
                    console.log(caretLengthPosInfo, absoluteStartPos, differencePos)
                    let finalRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos + differencePos, absoluteEndPos + differencePos)
                    //앞뒤로 한칸씩 늘린 range
                    let oneWideRangeAfter = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos + differencePos - 1 , absoluteEndPos + differencePos + 1 )
                    let oneWideRangeBeforeString = rangeInfoStorage[i].oneWideRangeString
                    if(errWordOnly === finalRange.toString() && oneWideRangeAfter.toString().trim() === oneWideRangeBeforeString.trim()){
                        rangeInfoStorage[i].rangeObj=finalRange
                        rangeInfoStorage[i].absoluteStartPos += differencePos
                        rangeInfoStorage[i].absoluteEndPos += differencePos
                    }else{
                        rangeInfoStorage[i].ghostErrWordZone.remove()
                        delete rangeInfoStorage[i]
                        delete this.candidateContents[i]
                    }
                }else{
                    let finalRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos , absoluteEndPos )
                    //앞뒤로 한칸씩 늘린 range
                    let oneWideRangeAfter = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos - 1 , absoluteEndPos + 1 )
                    let oneWideRangeBeforeString = rangeInfoStorage[i].oneWideRangeString
                    if(errWordOnly === finalRange.toString() && oneWideRangeAfter.toString().trim() === oneWideRangeBeforeString.trim()){
                        continue
                    }else{
                        console.log(finalRange,finalRange.toString(), oneWideRangeAfter, oneWideRangeAfter.toString())
                        rangeInfoStorage[i].ghostErrWordZone.remove()
                        delete rangeInfoStorage[i]
                        delete this.candidateContents[i]
                    }
                }

            }
        }
    }



    //이건 위치가 textArea에 거의 일치 해야하거나 적어도 left가 비슷해야함
    makeHanGrammGhost(){
        let that = this
        let hanGrammarGhost = document.createElement("hanGrammar-ghost")
        hanGrammarGhost.style.position = "absolute"
        hanGrammarGhost.style.top = "0px";
        hanGrammarGhost.style.left = "0px";
        hanGrammarGhost.style.pointerEvents = "none";


        let hanGrammarGhostPart = document.createElement("div")
        hanGrammarGhostPart.setAttribute("hanGrammar-part","highlights")
        hanGrammarGhostPart.style.position = "absolute"
        hanGrammarGhostPart.style.top = "0px";
        hanGrammarGhostPart.style.left = "0px";

        //크기 sync가 필요함 이건 무조건 this.targetArea 위치에 있어야함
        // mirrorDiv는 textArea의 내용을 Div 안에 집어넣어서 rangeObj를 만들기 위한 공간이고
        //이건 ghost panel 위에 존재하는 targetArea와 정확히 똑같은 크기(mirror)의 공간이다 둘의 차이명심
        let hanGrammarGhostMirror = document.createElement("div")
        let ghostMirrorStyle = hanGrammarGhostMirror.style
        ghostMirrorStyle.position = "relative"
        ghostMirrorStyle.boxSizing = "content-box"
        ghostMirrorStyle.pointerEvents = "none"
        ghostMirrorStyle.overflow = "hidden"

        //textarea 크기를 sync 해줌
        let computed = window.getComputedStyle(this.targetAreaElement)
            // + computed["padding-right"] + computed["padding-left"]
            // + computed["padding-top"] +computed["padding-bottom"]
        // let computedWidth =  parseInt(computed["width"])  + parseInt(computed["padding-left"]) + parseInt(computed["padding-right"])
        // let computedHeight = parseInt(computed["height"]) + parseInt(computed["padding-top"]) + parseInt(computed["padding-bottom"])

        //보이는 영역 기준인 그리고 transform 까지 계산되어진 rect를 사용하는게 맞는듯 하
        var rect = this.targetAreaElement.getBoundingClientRect()
        let computedWidth = rect.width
        let computedHeight = rect.height
        // console.log(computedWidth, computedHeight)
        ghostMirrorStyle.width =  computedWidth + "px"
        ghostMirrorStyle.height = computedHeight + "px"

        let hanGrammarGhostButton = document.createElement("div")
        hanGrammarGhostButton.setAttribute("hanGrammar-part","button")
        hanGrammarGhostButton.style.position = "absolute"
        hanGrammarGhostButton.style.right = "5px";
        hanGrammarGhostButton.style.bottom = "5px";

        let ghostbuttonLoader = document.createElement("div")
        ghostbuttonLoader.innerText = new String(0)[0]

        hanGrammarGhostButton.appendChild(ghostbuttonLoader)

        let observer = new MutationObserver(function(mutation) {

            var rect = mutation[0].target.getBoundingClientRect()

            let targetWidth = rect.width
            let targetHeight = rect.height

            ghostMirrorStyle.width =targetWidth + "px"
            ghostMirrorStyle.height = targetHeight + "px"

        })
        observer.observe(this.targetAreaElement,{attributes : true, attributeFilter : ['style']})

        // #mutationObserver 로는 안되는 경우가 있어서 이걸로 계속 sync
        setTimeout(function syncSize(){
            //확실하게 렌더링 되서 보이는 공간의 크기라면 getBoundingClientRect가 맞다
            var rect = that.targetAreaElement.getBoundingClientRect()

            let targetWidth = rect.width
            let targetHeight = rect.height

            ghostMirrorStyle.width =targetWidth + "px"
            ghostMirrorStyle.height = targetHeight + "px"
            // for(let j in that.rangeInfoStorage){
            //     if(that.rangeInfoStorage.hasOwnProperty(j)){
            //         let rangeInfo = that.rangeInfoStorage[j]
            //
            //         rangeInfo.setRangeLocalCoord()
            //         rangeInfo.makeGhostErrZone(0)
            //     }
            // }

            // console.log(targetWidth, targetHeight)
            //네이버 같은경우 메일창을 나가면 메일 쓰는 CE가 0px가 된다 따라서 모두 지워
            if(targetWidth === "0px" || targetHeight === "0px"){
                for (let i in that.rangeInfoStorage) {
                    if (that.rangeInfoStorage.hasOwnProperty(i)) {
                        that.rangeInfoStorage[i].ghostErrWordZone.remove()
                        delete that.rangeInfoStorage[i]
                        delete that.candidateContents[i]
                    }
                }
            }
            // console.log("synced")
            setTimeout(syncSize, 100)
        },100)

        let hanGrammarGhostPanelSkin = document.createElement("div")
        hanGrammarGhostPanelSkin.style.position = "absolute"
        hanGrammarGhostPanelSkin.style.top = "0px";
        hanGrammarGhostPanelSkin.style.left = "0px";

        //todo 코드로 맞추는 작업이 필요함  크기가 알아서 커지게
        let hanGrammarGhostPanel = document.createElement("div")
        //임시로 만든것
        hanGrammarGhostPanel.style.width = "2553px"
        hanGrammarGhostPanel.style.height = "1004px"


        //panel이 하나가 더있는데 용도를 모르겠음  hanGrammarGhostPanel과 heigh width 가 동일


        hanGrammarGhostPanelSkin.appendChild(hanGrammarGhostPanel)
        hanGrammarGhostMirror.appendChild(hanGrammarGhostPanelSkin)
        hanGrammarGhostMirror.appendChild(hanGrammarGhostButton)
        hanGrammarGhostPart.appendChild(hanGrammarGhostMirror)
        hanGrammarGhost.appendChild(hanGrammarGhostPart)


        this.targetAreaElement.parentElement.prepend(hanGrammarGhost)

        let hanGrammarGhostPartRect = hanGrammarGhostPart.getBoundingClientRect()
        let targetAreaRect = this.targetAreaElement.getBoundingClientRect()
        // console.log(hanGrammarGhostPartRect, targetAreaRect)
        let left = targetAreaRect.left - hanGrammarGhostPartRect.left
        let top = targetAreaRect.top -hanGrammarGhostPartRect.top

        ghostMirrorStyle.top =top + "px"
        ghostMirrorStyle.left =left+ "px"

        return hanGrammarGhostPanel

    }

    makeHanGrammMirror(){
        let that = this
        var properties = [
            'boxSizing',
            'width',  // on Chrome and IE, exclude the scrollbar, so the mirror div wraps exactly as the textarea does
            'height',
            'overflowX',
            'overflowY',  // copy the scrollbar for IE

            'borderTopWidth',
            'borderRightWidth',
            'borderBottomWidth',
            'borderLeftWidth',

            'paddingTop',
            'paddingRight',
            'paddingBottom',
            'paddingLeft',

            // https://developer.mozilla.org/en-US/docs/Web/CSS/font
            'fontStyle',
            'fontVariant',
            'fontWeight',
            'fontStretch',
            'fontSize',
            'lineHeight',
            'fontFamily',

            'textAlign',
            'textTransform',
            'textIndent',
            'textDecoration',  // might not make a difference, but better be safe

            'letterSpacing',
            'wordSpacing'
        ];

        let hanGrammMirror = document.createElement("hanGramm-mirror")
        let mirrorDiv = document.createElement("div")
        hanGrammMirror.appendChild(mirrorDiv)
        document.getElementsByTagName("html")[0].append(hanGrammMirror)

        let mirrorDivStyle = mirrorDiv.style;
        let computed = window.getComputedStyle(this.targetAreaElement)

        //copy all style to hanGramm mirror
        // mirrorDivStyle.cssText = document.defaultView.getComputedStyle(this.targetAreaElement, "").cssText

        mirrorDivStyle.whiteSpace = 'pre-wrap';
        mirrorDivStyle.wordWrap = 'break-word';

        // position off-screen
        mirrorDivStyle.position = 'fixed' //required to return coordinates properly
        mirrorDivStyle.top = "0px"
        mirrorDivStyle.left = "0px"

        mirrorDivStyle.visibility = 'hidden';

        mirrorDivStyle.overflow = 'hidden'

        properties.forEach(function(prop){
            mirrorDivStyle[prop] = computed[prop]
        })


        //textarea 창 크기를 sync 해줌
       let observer = new MutationObserver(function(mutation) {
            console.log(mutation)
            var targetWidth = mutation[0].target.style.width
            var targetHeight = mutation[0].target.style.height

            console.log(targetWidth, targetHeight)
           //px 붙일 필요없음음
           mirrorDivStyle.width =targetWidth
            mirrorDivStyle.height = targetHeight

        })
        observer.observe(this.targetAreaElement,{attributes : true, attributeFilter : ['style']})

        //targetArea가 예를 들어 width 100% 식으로 되어버리면 mirror가 정신을 못차리므로
        //computed로 px 를 뽑아서 지속적으로 갱신해 준다
        setTimeout(function syncSize(){
            let computed = window.getComputedStyle(that.targetAreaElement)


            mirrorDivStyle.width =computed.width
            mirrorDivStyle.height = computed.height

            // console.log("synced")
            setTimeout(syncSize, 100)
        },100)

        return mirrorDiv
    }


    getContentInRange(start, end) {
        if(start === undefined || start === null){
            start = 0
        }

        if(end === undefined || end === null){
            end = this.targetAreaElement.selectionEnd
        }

        var sel = this.targetAreaElement.value.substring(start,  end)
        // console.log(sel, sel.length)
        return sel
    }

    //saramin response data 처리를 하지만  jobkorea와 유사하
    processSaramInSpellCheckedData(receivedData){
        let that = this
        let originalContentRightTrimed = receivedData.originalContent.trimRight();
        // console.log(receivedData)
        let contentRange = receivedData.contentRange;
        let originalContent = receivedData.originalContent
        let startPos = receivedData.contentRange.startPos;
        let endPos = receivedData.contentRange.endPos;

        let extractedContent = this.getContentInRange(startPos, endPos)
        // console.log(extractedContent)
        //spellCheck를 받아온 현재 content내용이 바뀌어 있을 경우 진행하지 않고/ MODIFYING으로 해놓는다
        if (extractedContent !== originalContent){
            return
        }
        //saramin만 이 작업을 시킴
        this.preProcessSaramInSpellCheckedData(receivedData)
        // console.log(receivedData.checkedData.WordInfo.mcontent)
        //jobKorea만 이 작업을 시킴
        // console.log(receivedData.checkedData.WordInfo.mcontent);
        let checkedData = receivedData.checkedData

        let mcontent = receivedData.checkedData.result_text
        // console.log(mcontent, typeof mcontent)
        //checked된 데이터에 <br> 이 string으로 밖혀서 올수 있음
        //contentEditable 과는 다르게 mirrorDiv는 textArea를 정확히 반영하기에
        // newline 을 신토불이↵이땅에↵" 식으로 newline이 하나의 length를 차지하게 된다
        //따라서 CE처럼 받아오는 <br>을 없애는게 아니라 \n 으로 바꿔 넣어줌
        mcontent = mcontent.replace(/<br>/g, '\n')
        mcontent = mcontent.replace(/<br\/>/g, '\n')
        //saramin 에서는 <br /> 바로 옆에 \n 이 오기 때문에 그냥 <br /> 을 없애만 준다.
        mcontent = mcontent.replace(/<br\ \/>/g, '')
        // 두칸 이상뛴 내용에 nbsp가 섞여서 올 수 있음
        mcontent = mcontent.replace(/\&nbsp\;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp/g, ' ')

        //jobKorea에서 String으로 받아오
        let dummyElem = document.createElement(("div"));
        dummyElem.innerHTML = mcontent;
        // mcontent를 다시 바꿔줘야 함
        mcontent = dummyElem.innerHTML

        // console.log(mcontent)
        // console.log(dummyElem)


        let modifiedContentInfoArray = [];
        // console.log(dummyElem,dummyElem.childElementCount, dummyElem.children, dummyElem.childNodes)
        //jobKorea 형식(태그안에 errWord가 있는 전체문장)으로 오는것을 파싱하여 정보를 갖춘 array로 만든다.
        for (let i = 0; i < dummyElem.childElementCount ; i++){
            //html string으로 받아지는 mcontent내용을 parsing하기 위한 작업
            let childrenMcontent = dummyElem.children[i];
            let childrenMcontentId = childrenMcontent.getAttribute("data-value")
            // console.log(childrenMcontentId)
            //여기있는 id 값이 spell checked 된 반환내용중 candidate words의 몇번째 내용인지를 결정한다
            // 이건 jobKorea기준이므로 달라질 수 있음

            let childOuterHtml =  childrenMcontent.outerHTML;
            let childOuterHtmlLength = childOuterHtml.length;

            let childInnerText = childrenMcontent.innerText;
            let childInnerTextLength = childInnerText.length;


            //errWordWithTag로 위치 파악하고 errWordOnlyLength로 범위를 구한다
            let errWordOnly = childInnerText;
            let errWordOnlyLength = childInnerTextLength;

            let errWordWithTag = childOuterHtml;

            //id 부분이 숫자가 다 다르기 때문에 동일한 errword를 가지고 있다해도 무방함
            let errWordStartPos = mcontent.indexOf(errWordWithTag);
            let errWordEndPos = errWordStartPos + errWordOnlyLength;

            //errWordOnly로 치환 함으로서 다음 순회에서 위치를 뽑아낼수 있음
            mcontent = mcontent.replace(errWordWithTag, errWordOnly);
            // 앞에서부터 치환된 문장을 기준으로 위치를 기록
            if(this.checkAlphabetOnlyWord(errWordOnly, errWordOnlyLength)){
                continue
            }
            // TA에서는 errZone이 아니지만 의미는 비슷하기에 그냥 errWordZoneNumber라고 하고
            let errWordZoneNumber = this.errWordZoneNumber++;
            // console.log(errWordZoneNumber)

            this.candidateContents[errWordZoneNumber] = checkedData.WordCandidateList[childrenMcontentId]
            // this.candidateWords = {}
            modifiedContentInfoArray.push({"errWordOnly" : errWordOnly,
                "errWordWithTag" : errWordWithTag, "errWordStartPos" : errWordStartPos,
                "errWordEndPos" : errWordEndPos, "errWordZoneNumber" : errWordZoneNumber})
        }
        console.log(mcontent, mcontent.length)
        //최초에 맞춤법 검사를 통째로 보낸 범위

        for (let i = 0; i < modifiedContentInfoArray.length; i++){
            console.log(modifiedContentInfoArray)
            //통째로 보낸 범위 부터 시작한 errWord의 시작위치
            let modifiedContentInfo = modifiedContentInfoArray[i]
            let errWordStartPos = modifiedContentInfo.errWordStartPos;
            let errWordEndPos = modifiedContentInfo.errWordEndPos;
            //이게 적합한 위치에 있는 errWord와 바꿔치기 된다
            let errWordOnly = modifiedContentInfo.errWordOnly
            let errWordZoneNumber = modifiedContentInfo.errWordZoneNumber
            // console.log(startPos, errWordStartPos, errWordEndPos)
            let absoluteStartPos = startPos + errWordStartPos
            let absoluteEndPos = startPos + errWordEndPos
            console.log(absoluteStartPos, absoluteEndPos)

            let finalRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos, absoluteEndPos)
            let clone = finalRange.cloneRange();
            let rangeLength = finalRange.toString().length;
            //나중에 옆으로 붙은것이 있을때나 단어가 변경되었을때를 확인하기 위해 한칸씩 넓힌 range를 넣는다
            let oneWideRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos - 1 , absoluteEndPos + 1 )
            let oneWideRangeString = oneWideRange.toString()
            // console.log(oneWideRange, oneWideRange.toString())
            let duplicated = false
            for(let j in this.rangeInfoStorage){
                if(this.rangeInfoStorage.hasOwnProperty(j)){
                    duplicated = this.rangeInfoStorage[j].checkDuplicated(absoluteStartPos, absoluteEndPos, errWordOnly)
                    if (duplicated) break
                }
            }

            if(duplicated){
                //위에서 candidateContents를 errWordZoneNumber로 집어 넣었으므로 지워줌
                delete this.candidateContents[errWordZoneNumber]
                continue
            }

            this.rangeInfoStorage[errWordZoneNumber] =
                new RangeInfo(this, clone, absoluteStartPos, absoluteEndPos, errWordOnly, errWordZoneNumber, oneWideRangeString)
            // console.log(finalRange, finalRange.toString(), rangeLength);
        }

    }

    //잡코리아 위주의 response data 처리를 하지만 되도록 common이 될 수 있도록
    processJobKoreaSpellCheckedData(receivedData){
        let that = this
        let originalContentRightTrimed = receivedData.originalContent.trimRight();
        // console.log(receivedData)
        let contentRange = receivedData.contentRange;
        let originalContent = receivedData.originalContent
        let startPos = receivedData.contentRange.startPos;
        let endPos = receivedData.contentRange.endPos;

        let extractedContent = this.getContentInRange(startPos, endPos)
        // console.log(extractedContent)
        //spellCheck를 받아온 현재 content내용이 바뀌어 있을 경우 진행하지 않고/ MODIFYING으로 해놓는다
        if (extractedContent !== originalContent){
            let correctionPartUuid = receivedData.correctionPartUuid
            for (let part = 0 ;part < this.correctionPartInfoStorage.length; part++){
                let correctionPart = this.correctionPartInfoStorage[part]
                if (correctionPart.uuid === correctionPartUuid){
                    correctionPart.status = CorrectionPart.MODIFYING
                    correctionPart.processingStartTime = null
                    break
                }
            }
            return
        }

        // console.log(receivedData.checkedData.WordInfo.mcontent)
        //jobKorea만 이 작업을 시킴
        // console.log(receivedData.checkedData.WordInfo.mcontent);
        let checkedData = this.preProcessJobKoreaSpellCheckedData(receivedData.checkedData);
        //candidateContent처리
        this.preProcessJobKoreaCandidateContents(checkedData.WordCandidateList);

        let mcontent = checkedData.WordInfo.mcontent;
        //checked된 데이터에 <br> 이 string으로 밖혀서 올수 있음
        //contentEditable 과는 다르게 mirrorDiv는 textArea를 정확히 반영하기에
        // newline 을 신토불이↵이땅에↵" 식으로 newline이 하나의 length를 차지하게 된다
        //따라서 CE처럼 받아오는 <br>을 없애는게 아니라 \n 으로 바꿔 넣어줌
        mcontent = mcontent.replace(/<br>/g, '\n')
        mcontent = mcontent.replace(/<br\/>/g, '\n')
        mcontent = mcontent.replace(/<br\ \/>/g, '\n')
        // 두칸 이상뛴 내용에 nbsp가 섞여서 올 수 있음
        mcontent = mcontent.replace(/\&nbsp\;/g, ' ')
        mcontent = mcontent.replace(/\&nbsp/g, ' ')

        //jobKorea에서 String으로 받아오
        let dummyElem = document.createElement(("div"));
        dummyElem.innerHTML = mcontent;


        // console.log(mcontent)
        // console.log(dummyElem)


        let modifiedContentInfoArray = [];
        // console.log(dummyElem,dummyElem.childElementCount, dummyElem.children, dummyElem.childNodes)
        //jobKorea 형식(태그안에 errWord가 있는 전체문장)으로 오는것을 파싱하여 정보를 갖춘 array로 만든다.
        for (let i = 0; i < dummyElem.childElementCount ; i++){
            //html string으로 받아지는 mcontent내용을 parsing하기 위한 작업
            let childrenMcontent = dummyElem.children[i];
            //여기있는 id 값이 spell checked 된 반환내용중 candidate words의 몇번째 내용인지를 결정한다
            // 이건 jobKorea기준이므로 달라질 수 있음
            let childrenMcontentId = childrenMcontent.id;

            let childOuterHtml =  childrenMcontent.outerHTML;
            let childOuterHtmlLength = childOuterHtml.length;

            let childInnerText = childrenMcontent.innerText;
            let childInnerTextLength = childInnerText.length;

            //errWordWithTag로 위치 파악하고 errWordOnlyLength로 범위를 구한다
            let errWordOnly = childInnerText;
            let errWordOnlyLength = childInnerTextLength;

            let errWordWithTag = childOuterHtml;

            //id 부분이 숫자가 다 다르기 때문에 동일한 errword를 가지고 있다해도 무방함
            let errWordStartPos = mcontent.indexOf(errWordWithTag);
            let errWordEndPos = errWordStartPos + errWordOnlyLength;

            //errWordOnly로 치환 함으로서 다음 순회에서 위치를 뽑아낼수 있음
            mcontent = mcontent.replace(errWordWithTag, errWordOnly);
            // 앞에서부터 치환된 문장을 기준으로 위치를 기록
            if(this.checkAlphabetOnlyWord(errWordOnly, errWordOnlyLength)){
                continue
            }
            // TA에서는 errZone이 아니지만 의미는 비슷하기에 그냥 errWordZoneNumber라고 하고
            let errWordZoneNumber = this.errWordZoneNumber++;
            // console.log(errWordZoneNumber)

            this.candidateContents[errWordZoneNumber] = checkedData.WordCandidateList[childrenMcontentId]

            // this.candidateWords = {}
            modifiedContentInfoArray.push({"errWordOnly" : errWordOnly,
                "errWordWithTag" : errWordWithTag, "errWordStartPos" : errWordStartPos,
                "errWordEndPos" : errWordEndPos, "errWordZoneNumber" : errWordZoneNumber})
        }
        console.log(mcontent, mcontent.length)
        //최초에 맞춤법 검사를 통째로 보낸 범위

        for (let i = 0; i < modifiedContentInfoArray.length; i++){
            // console.log(modifiedContentInfoArray)
            //통째로 보낸 범위 부터 시작한 errWord의 시작위치
            let modifiedContentInfo = modifiedContentInfoArray[i]
            let errWordStartPos = modifiedContentInfo.errWordStartPos;
            let errWordEndPos = modifiedContentInfo.errWordEndPos;
            //이게 적합한 위치에 있는 errWord와 바꿔치기 된다
            let errWordOnly = modifiedContentInfo.errWordOnly
            let errWordZoneNumber = modifiedContentInfo.errWordZoneNumber
            // console.log(startPos, errWordStartPos, errWordEndPos)
            let absoluteStartPos = startPos + errWordStartPos
            let absoluteEndPos = startPos + errWordEndPos
            // console.log(absoluteStartPos, absoluteEndPos)

            let finalRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos, absoluteEndPos)
            let clone = finalRange.cloneRange();
            let rangeLength = finalRange.toString().length;
            //나중에 옆으로 붙은것이 있을때나 단어가 변경되었을때를 확인하기 위해 한칸씩 넓힌 range를 넣는다
            let oneWideRange = this.createFinalRangeHG(this.mirrorDiv, absoluteStartPos - 1 , absoluteEndPos + 1 )
            let oneWideRangeString = oneWideRange.toString()
            // console.log(oneWideRange, oneWideRange.toString())
            let duplicated = false
            for(let j in this.rangeInfoStorage){
                if(this.rangeInfoStorage.hasOwnProperty(j)){
                    duplicated = this.rangeInfoStorage[j].checkDuplicated(absoluteStartPos, absoluteEndPos, errWordOnly)
                    if (duplicated) break
                }
            }

            if(duplicated){
                //위에서 candidateContents를 errWordZoneNumber로 집어 넣었으므로 지워줌
                delete this.candidateContents[errWordZoneNumber]
                continue
            }

            this.rangeInfoStorage[errWordZoneNumber] =
                new RangeInfo(this, clone, absoluteStartPos, absoluteEndPos, errWordOnly, errWordZoneNumber, oneWideRangeString)
            // console.log(finalRange, finalRange.toString(), rangeLength);
        }

    }

    //대체어를 쿨릭했을때 postMessage로 받아서 수정
    processCorrectionBySelectedWord(errWord, selectedWord, errZoneNum) {
        let rangeInfo = this.rangeInfoStorage[errZoneNum]

        let absoluteStartPos = rangeInfo.absoluteStartPos
        let absoluteEndPos = rangeInfo.absoluteEndPos
        // console.log(absoluteStartPos, absoluteEndPos)
        let targetAreaElement = this.targetAreaElement

        let caretLengthPosInfo = this.lengthPosInfo

        caretLengthPosInfo.oldWholeLength = this.mirrorDiv.textContent.length

        targetAreaElement.value = targetAreaElement.value.substring(0, absoluteStartPos) +
            selectedWord + targetAreaElement.value.substring(absoluteEndPos)
        rangeInfo.ghostErrWordZone.remove()
        delete this.candidateContents[errZoneNum]
        delete this.rangeInfoStorage[errZoneNum]
        //correctionPart와 rangeInfo에 영향을 미치는 곳이 어느곳이냐? 냐는 질문에 대해 대답을 하는것이기 때문에
        // 교정되야될 단어 마지막 위치를 oldPosStart와 oldPostEnd로 둔다.
        caretLengthPosInfo.oldPosStart = absoluteEndPos
        caretLengthPosInfo.oldPosEnd = absoluteEndPos
        //DomNodeModified 와 중복이 되어 버리므로 수정할때는 해당 이벤트를 막아두고 여기서 모든걸 해결한
        caretLengthPosInfo.modifying = true
        this.mirrorDiv.textContent = this.targetAreaElement.value;

        caretLengthPosInfo.newPosStart = absoluteEndPos + (selectedWord.length - errWord.length)
        caretLengthPosInfo.newPosEnd = absoluteEndPos + (selectedWord.length - errWord.length)
        caretLengthPosInfo.newWholeLength = this.mirrorDiv.textContent.length

        let differencePos = selectedWord.length - errWord.length
        //교정이 되면서 글 길이가 변하므로 part 들도 조정해 줘야 한다.
        this.manageCorrectionPart(differencePos)
        //교정 되면서 글 길이가 변하므로 range 들도 조정해 줘야 한다.
        this.adjustRangeByEvent(differencePos)
        caretLengthPosInfo.modifying = false

        //이거 집어넣으면 문장이 길경우 교정시 아래로 계속 이동함
        //this.targetAreaElement.focus()

        //caret의 위치를 교정단어 끝으로 이동
        this.targetAreaElement.selectionEnd = caretLengthPosInfo.newPosEnd
    }

    //IGNORE를 클릭했을때 1.겉보기에 errWordZone인것을 없애고 2. 다음 spell Check가 왔을때는
    //errZone인것처럼 아무 영향이 없어야 한다
    processIgnoreSelection(errZoneNum){
        let rangeInfo = this.rangeInfoStorage[errZoneNum]
        rangeInfo.ignored = true
        delete this.candidateContents[errZoneNum]
        this.rangeInfoStorage[errZoneNum].ghostErrWordZone.remove()
    }
}

// ---------------------------------------------------------------------------------------
framePort.onMessage.addListener(function(message ){
    switch (message.type){
        case "spellChecked":
            let correctionServer = message.checkedServer
            let targetAreaId = message.targetAreaId;
            if(correctionServer === JOBKOREA){
                areaObjs[targetAreaId].processJobKoreaSpellCheckedData(message)
            }else if(correctionServer === SARAMIN){
                areaObjs[targetAreaId].processSaramInSpellCheckedData(message)
            }
            break;

        case "killAll":
            for(let areaObj in areaObjs){
                if(areaObjs.hasOwnProperty(areaObj)){
                    // console.log(areaObjs[areaObj])
                    //이미 areaObj가 있는 것들은 candidate와 errZone은 없애되 spellCheck 는 막는다
                    // kill process작동
                    areaObjs[areaObj].killAll()
                }
            }
            break;
        //그냥 숨겨진 button만 되살리는 기능만 하는걸로..
        case "reviveAll":
            for(let areaObj in areaObjs){
                if(areaObjs.hasOwnProperty(areaObj)){

                    areaObjs[areaObj].reviveAll()
                }
            }
            break;
    }
});

//topFrame에 injecting된 targetAreaSeeker 와 통신하기 위함임  물론 top과 top 끼리 이를 이용해 소통할때도 있
window.addEventListener("message", function(msg){
    let type = msg.data.type;
    let data = msg.data.data;
    switch(type){

        case "candidateWordSelected":
            let errWord = data[0];
            let selectedWord = data[1];
            let errZoneNum = data[2];
            let areaObjId = data[3];
            areaObjs[areaObjId].processCorrectionBySelectedWord(errWord, selectedWord, errZoneNum)
            break;
        case "ignoreSelected":
            let errZoneNumber = data[0];
            let areaObjectId = data[1];
            areaObjs[areaObjectId].processIgnoreSelection(errZoneNumber)
            break;
    }
})
//
// setInterval(function(){
//     console.log(areaObjs[Object.keys(areaObjs)[0]].rangeInfoStorage[1].rangeObj)
// }, 1000)