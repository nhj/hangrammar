'use strict'

var background = chrome.extension.getBackgroundPage();
console.log(background)

let bgPort = background.bgPort;



$(document).ready(function() {

    if(background.activation === false){
        console.log(background.activation)
        $("#activationSwitch")[0].checked = false;
        // background.bgMainMethods.deactivateProcess(false)
    }


    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        var pageURL = tabs[0].url

        var sliced = pageURL.split("/")


        sliced = sliced[0] + "//" + sliced[2]
        // console.log(sliced)
        ppMainMethods.displayOnPopup("#processedUrl", sliced)

        //default로 deactivated된 도메인은 무조건 어느 기기 에서건 해당 도메인은 off 됨
        chrome.storage.sync.get(['defaultDeactivated'], function(result){
            for(let i=0 ; i < result.defaultDeactivated.length; i++){
                console.log(sliced, result.defaultDeactivated[i], sliced.indexOf(result.defaultDeactivated[i]))
                if(sliced.indexOf(result.defaultDeactivated[i]) > -1){
                    // console.log("imIn")
                    $("#urlActivationCheck")[0].checked = false;
                    background.bgMainMethods.deactivateProcess(true)
                }
            }
        });

        //temporary 측면임 일단 하나의 도메인에서 off 를 시키면 페이지가 다시 전환 되더라도 계속 off
        let temporaryDeactivatedUrlArray = background.temporaryDeactivatedUrlArray;
        console.log(background, background.temporaryDeactivatedUrlArray);
        for(let i = 0; i < temporaryDeactivatedUrlArray.length; i++){
            let siteName = temporaryDeactivatedUrlArray[i];
            console.log(siteName,sliced.indexOf(siteName));
            if(sliced.indexOf(siteName) > -1){
                $("#urlActivationCheck")[0].checked = false;
                background.bgMainMethods.deactivateProcess(true)
            }
        }
    });
    $("#activationSwitch").on("change",function(evt){
        let checked = evt.target.checked;
        console.log(checked)
        background.activation = checked;

        let urlActivationChecked = $("#urlActivationCheck")[0].checked
        // console.log(urlActivationChecked, typeof urlActivationChecked)

        if(checked){
            //해당 url 이 금지가 아닐 경우에만 activate 시킨다
            //어차피 background.activation은 true가 되었으므로 다른 url에서는 정상작동
            if (urlActivationChecked){
                background.bgMainMethods.activateProcess(false)
            }
        }else{
            background.bgMainMethods.deactivateProcess(false)
        }
    })

    $("#urlActivationCheck").on("change",function(evt){
        let checked = evt.target.checked;

        if(checked){
            if (background.activation === true){
                background.bgMainMethods.activateProcess(true)
            }
        }else{
            background.bgMainMethods.deactivateProcess(true)
        }
    })
});

var ppMainMethods = {
    displayOnPopup: function (where, text) {
        if (where == "body") { //전체를 뒤집어쓰는 내용은 body에 그냥 넣어 버린다
            $(where).html("<h2>" + text + "</h2>");
        } else {
            $(where).append(text);
        }
    },

};

