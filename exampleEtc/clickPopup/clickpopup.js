/*
** scripts for populating classTitle, , classDescrip, instructor,
** instructorBio, and instructorImg elements from js array classesp[]
*/;

////  main routine  ////
////  ============  ////

// declare global variables
var errMsg = '';
var nodeObject;		// the node list returned by document.getElementsByClassName()
var popup;			// the <div> that will popup on click on either
// class title or instructor name in #FHC-Schedule table
var existsPopup = false;	// no popup currently exists
var classes = [];		// the array of objects, 1 per ea. time/classroom slot in
// #FHC-Schedule table, containing the info for ea. class

classes = [
    { timeslot:'930', classroom:'A', classTitle:'Searching on Ancestry.com 2016 (Basic Search Ideas For The Future)', instructor:'Mindy McLane', gender: 'female',
        classDescripInnerHTML:
            '<p>Searching on Ancestry has changed drastically in 2016. Learn advanced syntax and searching strategies.</p>\
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur</p>',
        instructorBioInnerHTML:
            '<p>Mindy McLane has taught courses at each of our conferences. She is particularly adept at all things Ancestry.</p>\
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>',
        instructorImgInnerHTML: '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/54b4806ae4b043943fa5a471/1463443824178/McLane_Mindy.jpg?format=300w">'
    },

    { timeslot:'930', classroom:'C', classTitle:'Land and Title Records', instructor:'Nancy Feroe', gender: 'female',
        classDescripInnerHTML:
            '<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem</p>',
        instructorBioInnerHTML:
            '<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem</p>',
        instructorImgInnerHTML: ''
    },

    { timeslot:'930', classroom:'D', classTitle:'Open Computer Lab', instructor: 'varying instructors', gender: '',
        classDescripInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>\
            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui</p>',
        instructorBioInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorImgInnerHTML: ''
    },

    { timeslot:'930', classroom:'E', classTitle:'"Hey Dad! What did you do in the War?', instructor:'Jim Johnson', gender: 'male',
        classDescripInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorBioInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorImgInnerHTML: '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/550b8741e4b06facd79df789/1463444097808/Johnson_Jim.png?format=500w">'
    },

    { timeslot:'930', classroom:'F', classTitle:'Thinking through your DNA results and figuring out what to do next.', instructor:'Dianne Gianninni', gender: 'female',
        classDescripInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorBioInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorImgInnerHTML: '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/5491d32be4b00e36dbf33c17/1418842987561/Giannini_Diane.jpg?format=300w">'
    },

    { timeslot:'930', classroom:'I', classTitle:'Come Find Out What’s Available at the Family History Center in Springdale', instructor:'Charlie Fowler', gender: 'male',
        classDescripInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorBioInnerHTML:
            '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt</p>\
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>',
        instructorImgInnerHTML: '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/56df3afe37013b7f3882d97c/1457470305450/?format=300w">'
    }
];	// end classes[]

// initialize / populate #FHC-Schedule table from the data in classes[]
populateScheduleTable();

//  add listeners then wait for something to happen
nodeObject = document.getElementsByClassName("classTitle");
addListeners(nodeObject);
nodeObject = document.getElementsByClassName("instructor");
addListeners(nodeObject);

/// function definitions ////
//// ==================== ////
// populate #FHC-Schedule table
function populateScheduleTable(){
    var tdId, selectorStrPrefix, selectorStr, el;

    // for ea. classes[i], get node list of elements
    //  matching selector; set elements' innerHTML to
    //  matching data from classes[i]

    for(i = 0, max = classes.length; i < max; i++){
        tdId = classes[i].timeslot + classes[i].classroom;
        selectorStrPrefix = '#x' + tdId + ' > ';

        selectorStr = selectorStrPrefix  + '.classTitle';
        el = document.querySelector( selectorStr );
        if(el === null){ logErrMsg( i, selectorStr );}
        else {
            el.innerHTML = classes[i].classTitle;}
        // end if

        selectorStr = selectorStrPrefix + '.instructor';
        el = document.querySelector( selectorStr );
        if(el === null ){ logErrMsg( i, selectorStr );}
        else {
            el.innerHTML = classes[i].instructor;}
        // end if

        selectorStr = selectorStrPrefix + '.gender';
        el = document.querySelector( selectorStr );
        if(el === null ){ logErrMsg( i, selectorStr );}
        else {
            el.innerHTML = classes[i].gender;}
        // end if

        selectorStr = selectorStrPrefix + '.classDescrip';
        el = document.querySelector( selectorStr );
        if(el === null ){ logErrMsg( i, selectorStr );}
        else {
            el.innerHTML = classes[i].classDescripInnerHTML;}
        // end if

        selectorStr = selectorStrPrefix + '.instructorBio';
        el = document.querySelector( selectorStr )
        if( el === null){logErrMsg( i, selectorStr )}
        else {
            el.innerHTML = classes[i].instructorBioInnerHTML;}
        // end if

        selectorStr = selectorStrPrefix + '.instructorImg';
        el = document.querySelector( selectorStr )
        if( el === null){logErrMsg( i, selectorStr )}
        else {
            el.innerHTML = classes[i].instructorImgInnerHTML;}
        // end if
    }	// end for
    if( errMsg !== "" ) { console.log( errMsg ); }
}	// end fn populateScheduleTable

// log error messages; called in populateScheduleTable()
function logErrMsg( i, selectorStr ){
    errMsg += 'querySelector (' + i + ' : ' + selectorStr +  ') === null; \r\r' ;
}	// end fn logErrMsg

//// functions for main routine ////
//// ========================== ////
//  addListeners() -- called in main routine to add listeners to
// client-title and instructor elements in table
function addListeners(nodeObject) {
    var i;
    for (i = 0; i < nodeObject.length; i++) {
        nodeObject[i].addEventListener("click", eventOnElement);
    }	// end for
}	// end fn addListeners

// called when there is a click event
function eventOnElement( event ) {
    var parentNode, targetNode;
    if ( !existsPopup ){	// if a popup already exists, do nothing on this click
        createPopup();
        populatePopup( event );
        parentNode = document.getElementsByTagName("body")[0];
        targetNode = parentNode.children[0];
        insertPopup( parentNode, targetNode );
        positionPopupOnPage( event );

        setPopupStyles();
    }	// end if
}	// end fn eventOnElement

// called when popup dismiss button clicked
function removePopup(){
    popup.remove();
    existsPopup = false;
}	// end fn removePopup

// called to put data from markup into popup
function populatePopup( event ) {
    var popupHTML, imgHTML, klassName, instructorBioHTML, classDescripHTML;
    var btnHTML = '<div class="popup-btn-wrapper"><button class="dismiss-popup-btn" onclick="removePopup()">X</button></div>';

    klassName = event.target.className;
    if( klassName.indexOf('instructor') !== -1 )
    // this is a click on instructor name event
    { imgHTML = setInstructorImg( event );
        instructorBioHTML = event.target.nextElementSibling.nextElementSibling.innerHTML;
        // set popup's innerHTML
        popup.innerHTML = '<div class="popup">' + btnHTML + '<div class="popup-content">' + imgHTML + instructorBioHTML +'</div></div>';
    }
    else // this is a click on classTitle event
    { classDescripHTML = event.target.nextElementSibling.innerHTML;
        popup.innerHTML = '<div class="popup">' + btnHTML + '<div class="popup-content">' + classDescripHTML + '</div></div>';
    }
}	// end fn populatePopup

function setInstructorImg( event ) {
    // this is a clickOnInstructor event
    var imgHTML, gender;
    // get the <img> HTML and gender from the markup
    imgHTML = event.target.nextElementSibling.nextElementSibling.nextElementSibling;
    gender = event.target.nextElementSibling.innerHTML;
    // if there is no <img> HTML in the markup, set one by default
    if( (imgHTML.innerHTML === '') || (imgHTML.innerHTML === null) )
    {	if( gender === "male" )
    { imgHTML = '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/5738ed8345bf2122c02ecfa1/1463348615348/vintage_man_1.jpg" >';
    }
    else { if( gender === "female" )
    { imgHTML = '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/57280dc8e707ebe0b422235e/1463347003660/Grandma.unframed.png" >';
    }
    else { imgHTML = '<img class="popup-img" src="https://static1.squarespace.com/static/546d0ba7e4b03f89760fd462/t/573a079a4c2f85368aed480a/1463420829016/American_Gothic_-_Grant_Wood.150x150.jpg" >';
    }
    }
    }
    else { imgHTML = imgHTML.innerHTML; }	// there is <img> HTML in markup; use it
    return imgHTML;
}	// end fn setInstructorImg

// to create popup
function createPopup() {
    popup = document.createElement( 'div' );
}	// end fn createPopup

// to insert popup into DOM from
// memory and set its class
function insertPopup( parentNode, targetNode ) {
    //최초 element 직전에 삽입
    parentNode.insertBefore(popup, targetNode);
    popup.classList.add( 'popup');
    popup.style.visibility = 'hidden';
    existsPopup = true;
}	// end fn insertPopup

// positon popup on page relative to cursor
// position at time of click event
// positon popup on page relative to cursor
// position at time of click event
function positionPopupOnPage( evt ) {

    var vpWH = [];
    var vpW, vpH;
    var intCoordX = evt.clientX;
    var intCoordY = evt.clientY;

    var intXOffset = intCoordX;
    var intYOffset = intCoordY;

    //화면 전체의 너비 높이 픽셀값\
    vpWH = getViewPortWidthHeight();
    vpW = vpWH[0];
    vpH = vpWH[1];
    popup.style.position = 'fixed';
    // if not display: block, .offsetWidth & .offsetHeight === 0
    popup.style.display = 'block';
    popup.style.zIndex = '10100';

    //popupclick 부분의 위치가 전체 width의 절반보다 크다면 popup의 너비만큼 offset에서 빼줌
    // 결국 원래 offset보다 좌측에 위치하게 된다
    if ( intCoordX > vpW/2 ) { intXOffset -= popup.offsetWidth; }
    if ( intCoordY > vpH/2 ) { intYOffset -= popup.offsetHeight; }

    if ( vpW <= 500 ) { intXOffset = ( vpW - popup.offsetWidth ) / 2;}
    if ( vpH <= 500 ) { intYOffset = (vpH - popup.offsetHeight ) / 2;}

    popup.style.top = intYOffset + 'px';
    popup.style.left = intXOffset + 'px';
    popup.style.visibility = 'visible';

}	// end fn positionPopupOnPage

function getViewPortWidthHeight() {

    var viewPortWidth;
    var viewPortHeight;

    // the more standards compliant browsers (mozilla/netscape/opera/IE7)
    // use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined')
    {
        viewPortWidth = window.innerWidth;
        viewPortHeight = window.innerHeight;
    }

    // IE6 in standards compliant mode (i.e. with a valid doctype as the
    // first line in the document)
    else if (typeof document.documentElement != 'undefined'
        && typeof document.documentElement.clientWidth !=
        'undefined' && document.documentElement.clientWidth != 0)
    {
        viewPortWidth = document.documentElement.clientWidth;
        viewPortHeight = document.documentElement.clientHeight;
    }

    // older versions of IE
    else {
        viewPortWidth = document.getElementsByTagName('body')[0].clientWidth;
        viewPortHeight = document.getElementsByTagName('body')[0].clientHeight;
    }
    return [viewPortWidth, viewPortHeight];
}	// end fn getViewPortWidthHeight

//  using JavaScript, dynamically set CSS styles as needed
function setPopupStyles(){
};  //end fn setPopupStyles

