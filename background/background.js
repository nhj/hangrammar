'use strict'
const SARAMIN ="SARAMIN"
const JOBKOREA = "JOBKOREA"
const PUSAN = "PUSAN"

const CHECKER_URL_PUSAN = "http://speller.cs.pusan.ac.kr/results";
const PUSAN_POST_KEY = "text1";
const CHECKER_URL_SARAMIN = "http://www.saramin.co.kr/zf_user/tools/spell-check";
const SARAMIN_POST_KEY = "content";
const CHECKER_URL_JOBKOREA = "http://www.jobkorea.co.kr/Service/User/Tool/SpellCheckExecute";
const JOBKOREA_POST_KEY = "tBox";


var service = "";

var currentTabID = -1;

var tabControlObjs = {};

var bgPort = {};

let requestCounter = 2;

var activation = true

let availableCorrectionServer = []

let correctionServer = ""

//여기에 리스트로 등록해놓으면   sync되면서 모든 기기에서 기본적으로 동작하지 않음
var defaultDeactivatedUrlArray = ["facebook.com"];
chrome.storage.sync.set({"defaultDeactivated" : defaultDeactivatedUrlArray}, function(){
})
//deactivated가 한번이라도 된 것들은 여기에 집어 넣어서 페이지가 동일 도메인에서 저장 되어도 계속
//deactivated가 되게 한
var temporaryDeactivatedUrlArray = []

chrome.browserAction.setPopup({popup : "/browserAction/popup.html"});

//탭추적 meta data picker에서 가져온 것 임
(function () {

//chrome 탭 제어 onUpdated~onFocusChanged 까지
//페이지 내에서 이동되거나 리로드 될때 extension event
    chrome.tabs.onUpdated.addListener(function (tabId, props, tab) {

        if (props.status == "loading" && tabId == currentTabID) {
            bgMainMethods.pageHandleSum("updated", currentTabID, tab);
        }
    });

    //For performance reasons Chrome can spawn a separate, invisible tab, and swap an existing tab with this pre-rendered tab.
    // If that happens, a chrome.tabs.onReplaced event is dispatched, providing old and new tabIds
    // 주소창 내에서 새로운 주소 입력시 invisible tab이 열리고 그다음 그걸 현제탭과 바꿔치기 할때가 있음 performance를 위해
    chrome.tabs.onReplaced.addListener(function (addedTab, removedTab) {
        //console.log("removedTab : " + removedTab + ", addedTab" + addedTab);
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            currentTabID = tabs[0].id;
            console.log(currentTabID);
            bgMainMethods.pageHandleSum("replaced", currentTabID, tabs[0]);
        });
    });

    //onActivate도 감지함 onActivate는 주소창에 입력해서 페이지 전환도 감지하나 onHighlighted는 탭 전환만 감지
    chrome.tabs.onHighlighted.addListener(function (highlightInfo) {
        chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
            //alert("asdf")
            currentTabID = tabs[0].id;
            bgMainMethods.pageHandleSum("highlighted", currentTabID, tabs[0]);
        })
    });
    /*
     chrome 새 창을 처음 열었을 경우에는 tabs.onActivated 로 진행시킬수 있지만
     열려져 있는 복수의 창을 왔다갔다 할 경우(탭전환없이  복수의 크롬 창만 왔다갔다) 진행이 안됨
     따라서 windows.onFocusChanged로 이벤트 리스너를 넣음
     */
    chrome.windows.onFocusChanged.addListener(function (windowId) {
        if (windowId > -1) {  //panel 은 탭창 추적에서 제외함
            chrome.tabs.query({active: true, windowId: windowId}, function (tabs) {
                //alert("asdf");
                currentTabID = tabs[0].id;
                bgMainMethods.pageHandleSum("focusChanged", currentTabID, tabs[0]);
            });
        }
    });

    // 탭하나 지웠을때 처리 + window를 통째로 닫을때도 개별 tab들이 호출된다
    chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
        bgMainMethods.pageHandleSum("removed", tabId);
    });


})();

var bgMainMethods = {

    isCorrectionServerAvailable : function(url){
        if(url === CHECKER_URL_SARAMIN){
            availableCorrectionServer.push(SARAMIN)
        }else if(url === CHECKER_URL_JOBKOREA) {
            availableCorrectionServer.push(JOBKOREA)
        }
    },

    //재사용될일 있으면 사용할것,  url을 location.hostName 형식으로 변환 checkHost에서밖에 안쓰면 삭제할것
    simplifyUrl: function (fullUrl) {
        return fullUrl.split("//")[1].split("/")[0]
    },

    pageHandleSum : function pageHandleSum(tabEvent, tabId, tabObj){
        //처음 보는 tab을 등록시
        console.log(tabEvent,tabId, tabObj)
        // tabEvent == "updated" || 이건 gmail에서 수시로 일어나기에 일단 뺐음
        if ( tabEvent == "replaced" || tabEvent == "removed"){
            console.log(String(tabId) + "was deleted in tabControlObjs")
            delete tabControlObjs[tabId]
        }

        if (tabEvent !== "removed"){
            // console.log(currentTabID)
            if (!(tabId in tabControlObjs )){
                tabControlObjs[tabId] = new TabController(tabId, tabObj)
            }else{
                //tabObj가 바뀌었을경우 갱신시켜줌
                tabControlObjs[tabId].tabObj = tabObj
            }
        }
        console.log(activation)

        if(tabEvent !== "removed"){
            if(activation){
                //전반적으로 모든 작동이 되는지 여부를 결정 시킨
                let isDeactivatedUrl = this.queryDeactivatedUrl()
                if(!isDeactivatedUrl){
                    this.activateProcess(false)
                }
            }else{
                this.deactivateProcess(false)
            }
            this.deactivateDefaultThings()
        }
    },

    preProcessContentForCheck : function(content, postKey){
        let processedContent = postKey + "=" + encodeURI(content);
        // console.log(processedContent);
        return processedContent;
    },


    processXHR: function (url, startData, content, contentRange, correctionPartUuid, tabContObj,frameId, areaId, ajax, checkedServer) {
        console.log(url);
        let that = this
        var method = arguments[1] ? "post" : "get";
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                console.log(xhr.status);
                if (xhr.status >= 200 && 300 > xhr.status || xhr.status == 300) {
                    //올때는 json 이라도 String으로 온다 {} 로 묶여있을뿐 암튼 json은 parsing이 필요
                    console.log(xhr)
                    var data = xhr.response.search(/^{/) !== -1 ? JSON.parse(xhr.response) : xhr.responseText;
                    console.log(data);
                    //getJSONEvent.fire({type:"json-completed", data :data});
                    // that.afterXHR(data,url);
                    if (content === undefined){
                        that.isCorrectionServerAvailable(url)
                        return
                    }
                    let properPort = tabContObj.portsForFrames[frameId];

                    properPort.postMessage(
                        {type:"spellChecked", targetAreaId : areaId, originalContent: content, contentRange:contentRange,correctionPartUuid : correctionPartUuid, checkedData : data, checkedServer : checkedServer }
                        );
                }
            }
        };
        xhr.onerror = function () {
            console.log(xhr.status + "  " + xhr.response);
        };

        xhr.open(method, url, true);
        if (method === "post") {
            ajax === true ? xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8")  : xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
            if(typeof startData == "object" || Array.isArray(startData)) {
                //헤더 셋팅 안하면 error 415  unsupported DATA type 뜸
                // xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
                startData = JSON.stringify(startData);
                // console.log(startData)
            }

            if(ajax === false || ajax==null || ajax === undefined){

            }
            xhr.send(startData);
        } else {
            xhr.send();
        }
    },

    //hangrammar 에서 안씀  혹시몰라서 형태만 남겨둔 것
    afterXHR: function afterXHR(data, urlKeyString) {
        //alert(event.data);
        if (typeof data == "object" || typeof urlKeyString === "string") {
            //alert(event.data.source);
            switch (urlKeyString) {
                //autoFill DB형식의 이력항목 subject name  description과 ,  subItem이 있는 항목들의 value description을 가져옴
                //
                case "URL_GET_META_PICKER_BASE_FROM_WAS":
                    console.log(urlKeyString + " =history");
                    if (data === undefined || data === null || data === "") {
                        bgMainMethods.processErr("dataEmpty");
                    } else {

                        statusVal.pickerBaseDataFromWAS = data;
                    }
                    break;

                case "URL_GET_REQUESTED_TABLE_INFO_ALL":
                    console.log(urlKeyString + " =requestedTable");
                    if (data === undefined || data === null || data === "") {
                        bgMainMethods.processErr("dataEmpty");
                    } else {
                        var manageTab = chrome.extension.getViews({type : "tab"});
                        //manage 외에 다른 창을 만드는 것이 있다면 manageMain의location.href을 뽑아내서
                        //구분할것
                        var manageMainContext = manageTab[0];
                        manageMainContext.manageTableMeta.initDataTables(data);
                        // array를 넣던 String을 넣던 table을 만들도록 할것
                    }
                    break;

            }
        } else {
            bgMainMethods.processErr("서버에서 최신 데이터를 가져올수 없음");
        }
    },
    //POPUP에서 호출해서 사용
    activateProcess : function(urlOnly){
        let tabId = currentTabID;
        // console.log(tabId)
        let tabContObj = tabControlObjs[tabId]
        if(urlOnly){
            let url = tabContObj.tabObj.url
            let domain = url.split("/")[2]

            let index = temporaryDeactivatedUrlArray.indexOf(domain)
            console.log(temporaryDeactivatedUrlArray, domain, index)
            if (index > -1) temporaryDeactivatedUrlArray.pop(index)
        }

        //일단 전반적으로 시작할때 area 를 인식하게  만들고
        chrome.tabs.sendMessage(tabId, {type:"processActivated"})

        //현재 성립된 area를 소생시킨다 correction시도 + button 되살리기
        for(let frame in tabContObj.portsForFrames){
            if(tabContObj.portsForFrames.hasOwnProperty(frame)){
                let port = tabContObj.portsForFrames[frame]
                port.postMessage(
                    {type:"reviveAll"}
                );
            }
        }

    },
    //POPUP에서 호출해서 사용
    deactivateProcess : function(urlOnly){
        let tabId = currentTabID;
        console.log(tabId)
        let tabContObj = tabControlObjs[tabId];
        if(urlOnly){
            let url = tabContObj.tabObj.url
            console.log(url)
            let domain = url.split("/")[2]
            let index = temporaryDeactivatedUrlArray.indexOf(domain)
            if (index === -1) temporaryDeactivatedUrlArray.push(domain)
        }
        //일단 전반적으로 시작할때 area 를 인식하지 못하게 만들고
        chrome.tabs.sendMessage(tabId, {type:"processDeactivated"})

        //현재 만들어진 모든 관련 객체 정보를 삭제한다
        for(let frame in tabContObj.portsForFrames){
            if(tabContObj.portsForFrames.hasOwnProperty(frame)){
                // console.log(tabContObj.portsForFrames[frame])
                //개별 frame마다 port가 있으므로 이에대해서 중지 명령을 보낸다
                let port = tabContObj.portsForFrames[frame]
                port.postMessage(
                    {type:"killAll"}
                );
            }
        }
    },

    //pageHandleSum 이 이루어 질때마다 default 로 막아둔 url을 체크함
    deactivateDefaultThings : function(){
        let tabId = currentTabID;
        console.log(tabId)
       //해당 tab이 삭제됐줌는데 진행될수도 있다 따라서 tabId가 있는지 체크해
        if(Object.keys(tabControlObjs).indexOf(String(tabId)) > -1){
            let tabContObj = tabControlObjs[tabId];
            console.log(tabContObj)
            let url = tabContObj.tabObj.url

            chrome.storage.sync.get(['defaultDeactivated'], function(result){
                for(let i=0 ; i < result.defaultDeactivated.length; i++){
                    // console.log(sliced, result.defaultDeactivated[i], sliced.indexOf(result.defaultDeactivated[i]))
                    if(url.indexOf(result.defaultDeactivated[i]) > -1){
                        chrome.tabs.sendMessage(tabId, {type:"processDeactivated"})
                    }
                }
            });
        }
    },

    queryDeactivatedUrl : function(){

        let isDeActivatedUrl = false

        let tabId = currentTabID;
        let tabContObj = tabControlObjs[tabId];

        let url = tabContObj.tabObj.url
        let domain = url.split("/")[2]
        let index = temporaryDeactivatedUrlArray.indexOf(domain)

        if(index > -1) {
            isDeActivatedUrl = true
        }
        // defaultDeactivated에 관련되어 있는지 질의

        chrome.storage.sync.get(['defaultDeactivated'], function(result){
            for(let i=0 ; i < result.defaultDeactivated.length; i++){
                // console.log(sliced, result.defaultDeactivated[i], sliced.indexOf(result.defaultDeactivated[i]))
                if(url.indexOf(result.defaultDeactivated[i]) > -1){
                    isDeActivatedUrl = true

                }
            }
        });
        // console.log(isDeActivatedUrl, url)
        return isDeActivatedUrl

    }
};

//가능한 교정서버를 체크
bgMainMethods.processXHR(CHECKER_URL_SARAMIN, bgMainMethods.preProcessContentForCheck("신토부울이", SARAMIN_POST_KEY))
bgMainMethods.processXHR(CHECKER_URL_JOBKOREA, bgMainMethods.preProcessContentForCheck("신토부울이", JOBKOREA_POST_KEY))


setTimeout(function(){
    if (Math.random() > 0.5){
        correctionServer = SARAMIN
    }else{
        correctionServer = JOBKOREA
    }

    console.log(correctionServer)

    if (correctionServer == "" && availableCorrectionServer.length > 0){
        correctionServer = availableCorrectionServer[0]
    }else if(correctionServer === "" && availableCorrectionServer === 0){
        alert("there is no available correction server!")
    }
}, 2000)


function TabController(tabId, tabObj ){
    // console.log(tabId, tabObj)
    this.tabId = tabId;
    this.tabObj = tabObj;
    this.portsForFrames = {};
    this.injectedFrames = {};
    this.isLodingCompleted = false;
    this.status = null;
    this.frameInfo = {};
    // this.askLoadingCompleted();
}

TabController.prototype = {
    constructor : TabController,

    //TopFrame에 targetAreaSeeker 삽입시키는건 manifest에서 담당함 -> 아래 chrome.webNavigation.onDOMContentLoaded 을 이용해서
    // 각 frame이 load 되는 순서로 frameId에 따라 tarAreaSeeker를 삽입시킴
    // ->(어느frame인지모르지만) 편집장이 클릭등이 되면서 properFrameFounded 메세지를 통해 해당 frame에만 hanGrammarScript가 삽입됨
    injectSeekerScript : function(frameId){
        let that = this;

        if (frameId == 0 || frameId == null || frameId == undefined) {
            // chrome.tabs.executeScript(this.tabId,
            //     {file: "programmaticScript/targetAreaSeeker.js", allFrames: false, "matchAboutBlank": true},function(){
            //         chrome.runtime.getPlatformInfo(function(info) {
            //             // Display host OS in the console
            //             // console.log(info)
            //             chrome.tabs.sendMessage(that.tabId, {type:"osInfo", data:info.os})
            //
            //         });
            //     })
        }else{
            chrome.tabs.executeScript(this.tabId,
                {file: "programmaticScript/targetAreaSeeker.js", allFrames: false,frameId : frameId ,"matchAboutBlank": true},function(){
                    chrome.runtime.getPlatformInfo(function(info) {
                        // Display host OS in the console
                        // console.log(info)
                        chrome.tabs.sendMessage(that.tabId, {type:"osInfo", data:info.os})
                        //페이지를 리로드한 이후에 CE나 TA를 인식할때 결정을 내려줘야 하기 때문에 이 과정이 필요
                        if(activation){
                            let isDeactivatedUrl = bgMainMethods.queryDeactivatedUrl()
                            if(!isDeactivatedUrl){
                                chrome.tabs.sendMessage(that.tabId, {type:"processActivated"})
                            }
                        }else{
                            chrome.tabs.sendMessage(that.tabId, {type:"processDeactivated"})
                        }

                    });
                })
        }
        chrome.tabs.insertCSS(this.tabId,
            {file : "programmaticScript/hanGrammar.css", frameId : frameId, "matchAboutBlank" : true})
    },

    injectHanGrammarScript : function(frameId){
        let that = this;
        // frameId 가 도착함 으로서 this.injectedFrames 안에 frameId가 key로 들어가고 value로 _han_gram_id 가 list의 일부로 들어간다.
        // 하나의 frame 안에 복수의 area가 있을수 있으므로 해당 area는 _han_gram_id 로 관리한다.(port)를 넣는 것이 맞을것으로 보임
        // console.log(frameId);
        chrome.tabs.executeScript(this.tabId,
            {file :"programmaticScript/hanGrammarScript.js", frameId : frameId, "matchAboutBlank" : true}, function(){
                chrome.tabs.sendMessage(that.tabId, {type:"correctionServerInfo", data : correctionServer}, {frameId : frameId})
            })
    }
};


// 디씨 개념글의 경우 모든 프레임이 로딩이 안될경우 끝끝내 injecting이 불가능 해질때가 있다
//
// 그렇다고 completed 되기 전에 seeker를 mainFrame 에만 인젝팅 하면  다른 프레임은 injecting이 안된다.
//
// 그렇다면 mainFrame은 무조건 loding 전에 injecting 시키고 나머지 프레임은 webNavigation으로
//
// mainframe이 injecting 됐는지 여부를 조사하여 특정 상황에서 injecting 시키는 것도 괜찮을 듯 하다.

chrome.webNavigation.onDOMContentLoaded.addListener(function(details){
    // console.log(details)
    var frameId = details.frameId;
    var tabId = details.tabId;

    if (tabId in tabControlObjs){
        tabControlObjs[tabId].injectSeekerScript(frameId);
    }
});
/*--------------------------------inpageScript와 background간의 데이터 송수신 port -----------------------------*/
chrome.runtime.onConnect.addListener(function(port){
    let sender = port.sender;
    if (port.name === "properFrameFounded"){
        let tabIdFromSeeker = sender.tab.id;
        console.log(tabIdFromSeeker);
        let frameIdFromSeeker = sender.frameId;
        console.log(frameIdFromSeeker)
        //해당 frame에만 HanGrammarJs injecting
        tabControlObjs[tabIdFromSeeker].injectHanGrammarScript(frameIdFromSeeker);
        //각 frame과 연동된 port를 여기에다가 집어넣어 사용한다.
        tabControlObjs[tabIdFromSeeker].portsForFrames[frameIdFromSeeker] = port;
        //injectedFrames 안에 frameId 별로된 object가 있고 또 그 안에 targetArea  별로 공간이 있음
        tabControlObjs[tabIdFromSeeker].injectedFrames[frameIdFromSeeker] = {};

        // port.onDisconnect.addListener(function(result){
        //     console.log(result)
        // })

        console.log(port.onDisconnect)

        port.onMessage.addListener(function(message, senderFromMessage){
            //api document와는 다르게 sender.sender로 해야됨
            senderFromMessage = senderFromMessage.sender;
            let frameId = senderFromMessage.frameId;
            let tabId = senderFromMessage.tab.id;
            switch(message.type){
                case "areaContent":
                    // console.log("requestCounter", requestCounter)
                    //초당 3개를 회복하는 requestCounter가 있을때만 요청을 한다
                    if (requestCounter >0){
                        requestCounter--;
                        let targetAreaId = message.targetAreaId;
                        let content = message.content;
                        let contentRange= message.contentRange;
                        let correctionPartUuid = message.correctionPartUuid

                        tabControlObjs[tabId].injectedFrames[frameId][targetAreaId] = content;

                        //2분의 1확률로 서버를 선택했었음
                        if (correctionServer === SARAMIN){
                            let processedPostContent = bgMainMethods.preProcessContentForCheck(content, SARAMIN_POST_KEY)
                            //content와 contentRange
                            bgMainMethods.processXHR(CHECKER_URL_SARAMIN, processedPostContent, content, contentRange ,correctionPartUuid, tabControlObjs[tabId] ,frameId, targetAreaId,false, SARAMIN);
                        }else if (correctionServer === JOBKOREA){
                            // let processedPostContent = bgMainMethods.preProcessContentForCheck(content, JOBKOREA_POST_KEY);
                            let processedPostContent = bgMainMethods.preProcessContentForCheck(content, JOBKOREA_POST_KEY)
                            //content와 contentRange
                            bgMainMethods.processXHR(CHECKER_URL_JOBKOREA, processedPostContent, content, contentRange ,correctionPartUuid, tabControlObjs[tabId] ,frameId, targetAreaId,false, JOBKOREA);
                        }else if(correctionServer === PUSAN){
                            let processedPostContent = bgMainMethods.preProcessContentForCheck(content, PUSAN_POST_KEY)
                            bgMainMethods.processXHR(CHECKER_URL_PUSAN, processedPostContent, content, contentRange, correctionPartUuid, tabControlObjs[tabId], frameId, targetAreaId, false, PUSAN)
                        }

                    }else{
                        return
                    }
                    break;
            }
        });
    }
});

//1초에 한번 requestCounter를 회복시킨다
setTimeout(function restoreRequestCounter(){
    if(requestCounter < 2){
        requestCounter++;
    }

    setTimeout(restoreRequestCounter, 500)

}, 500)




/*--------------------------------inpageScript와 background간의 데이터 송수신 port -----------------------------*/




/*------------------------------------댓글쓰기를 했을때 requst와 response를 추적 -----------------------------*/
// addListener(callBack , filter,  extra-infoSpec) 로 되어 있음
//extra-infoSpec은 중 blocking은 permission 필요, blocking 쓰면 header나 body를 조작한뒤 보낼 수 있음
//filter에 types 조건은 or 조건임

//네이버 블로그에서 commentFrame의 댓글쓰기 버튼을 누르면  문장검사 등은 ajax로 한뒤 그냥 post로 페이지 전환이 일어남
//따라서 extra-infosec의 xmlHttpRequest 항목이 없어도 보임
//댓글쓰기 버튼을 누르면 commentWrite.nhn  과 commentList.nhn/~~~~  가 둘다 request 되는데   둘다 일반 request이며
//commentList.nhn 이 나중이기 때문에 oncomplete 는  commentList.nhn 만 보임(물론 commentWrite는 처리 성공되었을것임)


//댓글창에서 일어나는  commentWrite는 request만 있고 response는 없다 따라서 activatedObj 가 존재하며 sequence가 0
//이 아니라면 cmmentWrite 변수를 True 로 바꿔준다 나중에 webNavigation에서 이를 false로 다시 바꿔준다
// chrome.webRequest.onSendHeaders.addListener(
//     function(details) {
//         if(activatedObj && activatedObj.getSequence() > 0 && details.tabId == activatedObj.tabId){
//             console.log(details);
//             //이걸 해 놓아야 매크로에서 작동시킨 댓글쓰기 라는걸 알 수 있음
//             activatedObj.requestStatus = "sended";
//         }
//     },
//     {urls: ["*://blog.naver.com/CommentWrite.nhn"],types:["sub_frame"]},
//     [ "requestHeaders"]);
//
// //댓글이 입력 완료되고 페이지 전환이 이루어진 뒤 다음 댓글을 쓰는게 ready가 되는 기준은 아무래도 reponse보단
// //페이지가 준비가 되는것이 기준이 되야함 따라서 webNavigation.onCompleted를 씀
// // chrome.webRequest.onCompleted.addListener(
// //     function(details) {
// //         console.log("onCompleted", details)
// //         // return ;
// //     },
// //     {urls: ["<all_urls>"],tabId:currentTabID,types:["sub_frame"]},
// //     [ "responseHeaders"]);
//
//
// chrome.webNavigation.onCompleted.addListener(function(details){
//     console.log(details)
//     var frameId = details.frameId;
//     var tabId = details.tabId;
//     var url = details.url
//     if(url.match("CommentList")){
//         //bgPort의 sender tabId 와 비교하여 같을 경우만 진행시킴  매크로 작동중 다른 탭에서 사용자가 직접 댓글 작성을 할 수도 있으므로
//         if(activatedObj && activatedObj.requestStatus == "sended" && tabId == activatedObj.tabId){
//             activatedObj.requestStatus = "completed";
//             activatedObj.completeSequence(frameId,tabId);
//         }
//     }
//
// });